#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
  echo 
  echo "####################################################################################"
  echo
  echo "Wrong number of parameters"
  echo 
  echo "Usage: $0 <IOT CATALYST AUTOMATION SERVICE IMAGE TAG>"
  echo 
  echo "####################################################################################"
  echo
  exit 1
else
  IMAGE_TAG="$1"
  docker build -t fwdigital/iotcatalyst_automation_service_linux_x86_64:"$IMAGE_TAG" .
fi
