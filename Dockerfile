FROM amd64/python:3.8-slim-buster

RUN apt update
RUN apt install -y redis-server procps

WORKDIR /home/catalyst/IoTCatalystOS

RUN python -m venv .venv
COPY requirements.txt .
RUN .venv/bin/python -m pip install --no-cache-dir -r requirements.txt

COPY docker-entrypoint.sh .
COPY start.sh .
RUN chmod +x *.sh
COPY docs/endpoints-collection.json ./docs/endpoints-collection.json
COPY docs/help.pdf ./docs/help.pdf
COPY IoTCAutomationService IoTCAutomationService

ENTRYPOINT [ "/home/catalyst/IoTCatalystOS/docker-entrypoint.sh" ]
