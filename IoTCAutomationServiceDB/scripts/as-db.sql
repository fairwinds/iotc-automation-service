USE IoTCatalystAutomationService;


-- Application Management Tables


CREATE TABLE shell_command
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_original_shell_command   INTEGER                                             COMMENT 'ID of the deprecated command that has been substituted by this one',
  name                        VARCHAR(80)         NOT NULL                        COMMENT 'Readable name of the shell command',                   
  preview                     VARCHAR(4096)       NOT NULL                        COMMENT 'Listing of the shell command',
  interpreter                 VARCHAR(80)         NOT NULL                        COMMENT 'The type of shell interpreter (eg. BASH, PYTHON)',
  is_metric                   BOOL                NOT NULL                        COMMENT 'Whether the shell command is usable as an Automation Service METRIC command',
  deprecation_date            DATETIME                                            COMMENT 'When the command has been invalidated due to the creation of a new version',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_original_shell_command) REFERENCES shell_command (id)
) COMMENT 'Used for shell commands library. Not related to any specific campaign';

CREATE TABLE shell_command_architecture
(
  id_shell_command            INTEGER             NOT NULL,
  architecture                VARCHAR(80)         NOT NULL                        COMMENT 'Consists of target CPU and OS',

  PRIMARY KEY (id_shell_command, architecture),
  FOREIGN KEY (id_shell_command)  REFERENCES shell_command (id) ON DELETE CASCADE
) COMMENT 'Relationship to determine the available architectures for a given shell command';

CREATE TABLE command_type
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  name                        VARCHAR(80)         UNIQUE NOT NULL                 COMMENT 'Command type',
  description                 VARCHAR(256)        NOT NULL DEFAULT '',
  entity_table                VARCHAR(80)         NOT NULL UNIQUE                 COMMENT 'Name of the specialized command table',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY (id)
) COMMENT 'Polymorphic table, used to link base command invocations to the right specialized commands';


-- Campaign Management Tables


CREATE TABLE campaign
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_last_schedule            INTEGER                                             COMMENT 'Contains the id of the last schedule. NULL if the campaign has never been run yet.',
  id_component_type           INTEGER             NOT NULL                        COMMENT 'Type of the IoTCatalyst entities involved in the campaign (eg. Hypervisor)',
  name                        VARCHAR(80)         NOT NULL                        COMMENT 'Campaign readable name',
  description                 VARCHAR(256)        NOT NULL DEFAULT '',
  targets                     LONGTEXT            NOT NULL                        COMMENT 'JSON array of campaign target ids',
  scheduled                   INTEGER             NOT NULL                        COMMENT 'Scheduled, not scheduled, or unscheduled',
  suspendable                 BOOL                NOT NULL                        COMMENT 'Whether the campaign can be paused from the outside',
  abortable                   BOOL                NOT NULL                        COMMENT 'Whether the campaign can be aborted',
  schedule                    VARCHAR(256)                                        COMMENT 'Information about the different types of schedule (repeated, HH:MM, cron, delayed)',
  n_subjobs                   INTEGER             NOT NULL                        COMMENT 'The number of concurrent subjobs used to parallelize the campaign execution',
  definition                  LONGTEXT            NOT NULL                        COMMENT 'Campaign JSON definition',
  on_error                    VARCHAR(80)         NOT NULL                        COMMENT 'What the Automation Service should do if an error occurs during the campaign execution',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id)
) COMMENT 'Information about a campaign sent to the Automation Service';

CREATE TABLE campaign_architecture
(
  id_campaign                 INTEGER             NOT NULL,
  architecture                VARCHAR(80)         NOT NULL                        COMMENT 'Consists of target CPU and OS',

  PRIMARY KEY (id_campaign, architecture),
  FOREIGN KEY (id_campaign)  REFERENCES campaign (id) ON DELETE CASCADE
) COMMENT 'Relationship between a campaign and its architectures';

CREATE TABLE schedule
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_campaign                 INTEGER             NOT NULL                        COMMENT 'ID of the campaign this schedule refers to',
  progress                    DOUBLE              NOT NULL DEFAULT 0              COMMENT 'Indicates the schedule execution progress',
  status                      VARCHAR(80)         NOT NULL                        COMMENT 'Indicates the schedule execution status',
  start_time                  DATETIME                                            COMMENT 'Date in which the schedule started',
  end_time                    DATETIME                                            COMMENT 'Date in which the schedule finished',
  campaign_semaphore          INTEGER             NOT NULL                        COMMENT 'Used to pause/resume the execution of a campaign schedule',
  n_exited_subjobs            INTEGER             NOT NULL DEFAULT 0              COMMENT 'Number of completed parallel subjobs',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_campaign) REFERENCES campaign (id) ON DELETE CASCADE
) COMMENT 'Information about a specific campaign execution. Scheduled campaigns may have more than a single execution';

CREATE TABLE target_campaign_status
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_campaign                 INTEGER             NOT NULL,
  id_schedule                 INTEGER             NOT NULL,
  id_target                   INTEGER             NOT NULL,
  id_subjob                   INTEGER             NOT NULL,
  name                        VARCHAR(80)         NOT NULL                        COMMENT 'Name of the target',
  architecture                VARCHAR(80)         NOT NULL                        COMMENT 'Architecture of the target',
  progress                    DOUBLE              NOT NULL DEFAULT 0              COMMENT 'Indicates the campaign execution progress on the target',
  status                      VARCHAR(80)         NOT NULL                        COMMENT 'Indicates the campaign execution status on the target',
  start_time                  DATETIME                                            COMMENT 'Date in which the campaign started on the target',
  end_time                    DATETIME                                            COMMENT 'Date in which the campaign finished on the target',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_campaign)  REFERENCES campaign (id) ON DELETE CASCADE,
  FOREIGN KEY (id_schedule)  REFERENCES schedule (id) ON DELETE CASCADE
) COMMENT 'Information about a campaign execution on a specific target';

CREATE TABLE task_invocation_registry
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_campaign                 INTEGER             NOT NULL,
  name                        VARCHAR(80)         NOT NULL                        COMMENT 'Task readable name',
  description                 VARCHAR(256)        NOT NULL DEFAULT ''             COMMENT 'Task optional description',
  conditions                  LONGTEXT            NOT NULL                        COMMENT 'JSON definition of the task conditions',
  on_error_policy             VARCHAR(80)                                         COMMENT 'What an IoTCatalyst Agent should do if an error occurs during the task execution. Used only for tasks that contain commands for an IoTCatalyst Agent',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_campaign) REFERENCES campaign (id) ON DELETE CASCADE
) COMMENT 'Information about a campaign task sent to the Automation Service';

CREATE TABLE task_invocation_result
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_task                     INTEGER             NOT NULL,
  id_campaign                 INTEGER             NOT NULL,
  id_schedule                 INTEGER             NOT NULL                        COMMENT 'ID of the campaign schedule the task result belongs to',
  id_target                   INTEGER             NOT NULL                        COMMENT 'ID of the target that produced the task result',    
  id_subjob                   INTEGER             NOT NULL                        COMMENT 'ID of the subjob that executed the task',
  progress                    DOUBLE              NOT NULL DEFAULT 0              COMMENT 'Indicates the task execution progress',
  status                      VARCHAR(80)         NOT NULL                        COMMENT 'Indicates the task execution status',
  start_time                  DATETIME                                            COMMENT 'Date in which the task started',
  end_time                    DATETIME                                            COMMENT 'Date in which the task finished',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_task) REFERENCES task_invocation_registry (id) ON DELETE CASCADE,
  FOREIGN KEY (id_campaign)  REFERENCES campaign (id) ON DELETE CASCADE,
  FOREIGN KEY (id_schedule)  REFERENCES schedule (id) ON DELETE CASCADE
) COMMENT 'Result of a campaign task on a specific target';

CREATE TABLE base_command_invocation_registry
(
  id                          INTEGER            NOT NULL AUTO_INCREMENT,
  id_specialized_command      INTEGER            NOT NULL                         COMMENT 'ID of the specialized command invocation',
  id_command_type             INTEGER            NOT NULL                         COMMENT 'Router to lead to the right specialized command invocation table',
  id_campaign                 INTEGER            NOT NULL,
  id_task                     INTEGER            NOT NULL                         COMMENT 'The task definition that contains the command',
  definition                  VARCHAR(4096)      NOT NULL                         COMMENT 'JSON definition of the invoked command',
  timeout                     DOUBLE             NOT NULL                         COMMENT 'Command execution timeout (in seconds)',
  retries                     INTEGER            DEFAULT 0                        COMMENT 'How many times the command should be retried if an error occurs',
  creation_date               DATETIME           DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_command_type) REFERENCES command_type (id) ON DELETE CASCADE,
  FOREIGN KEY (id_campaign) REFERENCES campaign (id) ON DELETE CASCADE,
  FOREIGN KEY (id_task) REFERENCES task_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Information about a campaign command sent to the Automation Service. Base table for further specializations.';

CREATE TABLE api_command_invocation_registry
(
  id                          INTEGER            NOT NULL AUTO_INCREMENT,
  id_base_command             INTEGER            ,
  method                      VARCHAR(4096)      NOT NULL                         COMMENT 'IoTCatalyst API method name',
  params                      VARCHAR(4096)      NOT NULL                         COMMENT 'IoTCatalyst API method parameters',
  creation_date               DATETIME           DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_base_command) REFERENCES base_command_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Command specialization';

CREATE TABLE metric_command_invocation_registry
(
  id                          INTEGER            NOT NULL AUTO_INCREMENT,
  id_shell_command            INTEGER            NOT NULL,
  id_base_command             INTEGER            ,
  data_type                   VARCHAR(80)        NOT NULL                         COMMENT 'Metric datatype. It may be numeric or string or date',
  date_format                 VARCHAR(80)                                         COMMENT 'Used to specify the date format if metric datatype is date. It may be unix_timestamp or YYYY-MM-DD HH:MI:SS',
  refresh_interval            DOUBLE            NOT NULL                         COMMENT 'Used to specify the update frequency (in seconds) for the metric value by an IoTCatalyst Agent',
  creation_date               DATETIME           DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_shell_command) REFERENCES shell_command (id) ON DELETE CASCADE,
  FOREIGN KEY (id_base_command) REFERENCES base_command_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Command specialization';

CREATE TABLE shell_command_invocation_registry
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_shell_command            INTEGER             NOT NULL,
  id_base_command             INTEGER             ,
  causes_reboot               BOOL                NOT NULL                        COMMENT 'Whether the command causes a system reboot on the IoTCatalyst Agent',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY (id),
  FOREIGN KEY (id_shell_command) REFERENCES shell_command (id) ON DELETE CASCADE,
  FOREIGN KEY (id_base_command) REFERENCES base_command_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Command specialization';

CREATE TABLE hypervisor_command_invocation_registry
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_base_command             INTEGER             ,
  type                        VARCHAR(80)         NOT NULL                        COMMENT 'Type of hypervisor command (start, stop, update)',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_base_command) REFERENCES base_command_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Command specialization';

CREATE TABLE command_invocation_result
(
  id                          INTEGER             NOT NULL AUTO_INCREMENT,
  id_campaign                 INTEGER             NOT NULL,
  id_schedule                 INTEGER             NOT NULL,
  id_task                     INTEGER             NOT NULL                        COMMENT 'The task definition row ID',
  id_task_result              INTEGER             NOT NULL                        COMMENT 'The task result row ID',
  id_command                  INTEGER             NOT NULL,
  id_target                   INTEGER             NOT NULL,
  id_subjob                   INTEGER             NOT NULL,
  delivery_status             INTEGER             NOT NULL                        COMMENT 'Whether the command has been sent/received by its actual executor. Used to restore campaign status should the Automation Service be turned off',
  raw                         LONGTEXT                                            COMMENT 'Command raw JSON result, as received by the command executor',
  result                      LONGTEXT                                            COMMENT 'String output of the command execution. For SHELL/METRIC commands, it is the stdout', -- grande dimensione, forse conviene mettere in una tabella ausiliaria
  status_code                 INTEGER                                             COMMENT 'Command execution status code',
  execution_date              DATETIME                                            COMMENT 'When the command has been completed',
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id),
  FOREIGN KEY (id_campaign) REFERENCES campaign (id) ON DELETE CASCADE,
  FOREIGN KEY (id_schedule) REFERENCES schedule (id) ON DELETE CASCADE,
  FOREIGN KEY (id_task) REFERENCES task_invocation_registry (id) ON DELETE CASCADE,
  FOREIGN KEY (id_task_result) REFERENCES task_invocation_result (id) ON DELETE CASCADE,
  FOREIGN KEY (id_command) REFERENCES base_command_invocation_registry (id) ON DELETE CASCADE
) COMMENT 'Result of a campaign command on a specific target';


-- Campaign Views


-- For each campaign, the total number of commands.
CREATE VIEW v_total_campaign_commands AS
SELECT id_campaign, COUNT(*) AS n_total_commands
FROM base_command_invocation_registry
GROUP BY id_campaign;

-- For each task result id, the total number of commands.
CREATE VIEW v_total_task_result_commands AS
SELECT id_task_result, COUNT(*) AS n_total_commands
FROM command_invocation_result
GROUP BY id_task_result;

-- For each task registry id, the total number of commands.
CREATE VIEW v_total_task_registry_commands AS
SELECT id_task, COUNT(*) AS n_total_commands
FROM base_command_invocation_registry
GROUP BY id_task;

-- All API command definitions in the database.
CREATE VIEW v_api_command AS
  SELECT command_registry.id AS id_base_command,
    'API' AS type,
    api_registry.method AS command,
    command_registry.timeout AS timeout,
    command_registry.retries AS retries,
    command_registry.definition AS raw_definition,
    NULL AS name,
    api_registry.params AS params,
    NULL AS causes_reboot,
    NULL AS data_type,
    NULL AS date_format,
    NULL AS refresh_interval
  FROM api_command_invocation_registry api_registry,
    (base_command_invocation_registry command_registry
      JOIN command_type
        ON command_registry.id_command_type=command_type.id)
  WHERE command_type.name='API'
    AND command_registry.id_specialized_command=api_registry.id;

-- All METRIC command definitions in the database.
CREATE VIEW v_metric_command AS
  SELECT command_registry.id AS id_base_command,
    'METRIC' AS type,
    shell_command.preview AS command,
    command_registry.timeout AS timeout,
    command_registry.retries AS retries,
    command_registry.definition AS raw_definition,
    shell_command.name AS name,
    NULL AS params,
    NULL AS causes_reboot,
    metric_registry.data_type AS data_type,
    metric_registry.date_format AS date_format,
    metric_registry.refresh_interval AS refresh_interval
  FROM 
    (metric_command_invocation_registry metric_registry
      JOIN shell_command
        ON metric_registry.id_shell_command=shell_command.id),
    (base_command_invocation_registry command_registry
      JOIN command_type
        ON command_registry.id_command_type=command_type.id)
  WHERE command_type.name='METRIC'
    AND command_registry.id_specialized_command=metric_registry.id;

-- All SHELL command definitions in the database.
CREATE VIEW v_shell_command AS
  SELECT command_registry.id AS id_base_command,
    'SHELL' AS type,
    shell_command.preview AS command,
    command_registry.timeout AS timeout,
    command_registry.retries AS retries,
    command_registry.definition AS raw_definition,
    shell_command.name AS name,
    NULL AS params,
    shell_registry.causes_reboot AS causes_reboot,
    NULL AS data_type,
    NULL AS date_format,
    NULL AS refresh_interval
  FROM 
    (shell_command_invocation_registry shell_registry
      JOIN shell_command
        ON shell_registry.id_shell_command=shell_command.id),
    (base_command_invocation_registry command_registry
      JOIN command_type
        ON command_registry.id_command_type=command_type.id)
  WHERE command_type.name='SHELL'
    AND command_registry.id_specialized_command=shell_registry.id;

-- All PRESET command definitions in the database.
CREATE VIEW v_hypervisor_command AS
  SELECT command_registry.id AS id_base_command,
    'PRESET' AS type,
    hv_registry.type AS command,
    command_registry.timeout AS timeout,
    command_registry.retries AS retries,
    command_registry.definition AS raw_definition,
    NULL AS name,
    NULL AS params,
    NULL AS causes_reboot,
    NULL AS data_type,
    NULL AS date_format,
    NULL AS refresh_interval
  FROM hypervisor_command_invocation_registry hv_registry,
    (base_command_invocation_registry command_registry
      JOIN command_type
        ON command_registry.id_command_type=command_type.id)
  WHERE command_type.name='PRESET'
    AND command_registry.id_specialized_command=hv_registry.id;

-- All command definitions in the database. Used to have the same column schema for all command types.
CREATE VIEW v_command_invocation_registry AS
  SELECT * 
  FROM v_api_command
  UNION (
    SELECT *
    FROM v_metric_command
  )
  UNION (
    SELECT *
    FROM v_shell_command
  )
  UNION (
    SELECT *
    FROM v_hypervisor_command
  )
  ORDER BY id_base_command;


-- DataTable Campaign Views


-- For each target of a campaign schedule, the standard output of its commands.
CREATE VIEW v_dt_target_command_result AS
SELECT
    id_campaign,
    id_schedule,
    id_target,
    GROUP_CONCAT(result SEPARATOR ', ') AS command_results
FROM command_invocation_result
GROUP BY id_target, id_schedule, id_campaign;

-- For each target, its campaign schedule and its campaign execution status.
CREATE VIEW v_dt_schedule_target_detail AS
SELECT 
    target_campaign_status.id_campaign AS id_campaign,
    target_campaign_status.id_schedule AS id_schedule,
    target_campaign_status.id_target AS id_target,
    target_campaign_status.name AS target_name,
    ROUND(target_campaign_status.progress, 1) AS target_progress,
    target_campaign_status.status AS target_status,
    UNIX_TIMESTAMP(target_campaign_status.start_time) AS target_start_time,
    UNIX_TIMESTAMP(target_campaign_status.end_time) AS target_end_time,
    
    v_dt_target_command_result.command_results AS command_results
FROM target_campaign_status
    JOIN v_dt_target_command_result ON (
        target_campaign_status.id_campaign = v_dt_target_command_result.id_campaign
        AND target_campaign_status.id_schedule = v_dt_target_command_result.id_schedule
        AND target_campaign_status.id_target = v_dt_target_command_result.id_target
    );


-- Log Table


CREATE TABLE log 
(
  id                          INTEGER             AUTO_INCREMENT,
  channel                     VARCHAR(256), 
  level                       VARCHAR(80)         NOT NULL, 
  message                     LONGTEXT            NOT NULL, 
  context                     LONGTEXT            , 
  extra                       LONGTEXT            ,
  creation_date               DATETIME            DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id)
);


-- Metric/Command Definitions


INSERT INTO shell_command(id, name, preview, is_metric, interpreter) VALUES
(1, "FREESPACE", "df --output=avail -k /home | sed '1d;s/[^0-9]//g'", true, "BASH"),
(2, "update", "sudo apt update", false, "BASH"),
(3, "upgrade", "sudo apt upgrade", false, "BASH"),
(4, "reboot", "sudo reboot", false, "BASH");

INSERT INTO shell_command_architecture (id_shell_command, architecture) VALUES 
(1, 'p37-linux-armv6-rpi'),
(1, 'p37-linux-armv6'),
(1, 'p37-linux-armv7'),
(1, 'p37-linux-armv7-rpi'),
(1, 'p37-linux-x86_64'),

(2, 'p37-linux-armv6-rpi'),
(2, 'p37-linux-armv6'),
(2, 'p37-linux-armv7'),
(2, 'p37-linux-armv7-rpi'),
(2, 'p37-linux-x86_64'),

(3, 'p37-linux-armv6-rpi'),
(3, 'p37-linux-armv6'),
(3, 'p37-linux-armv7'),
(3, 'p37-linux-armv7-rpi'),
(3, 'p37-linux-x86_64'),

(4, 'p37-linux-armv6-rpi'),
(4, 'p37-linux-armv6'),
(4, 'p37-linux-armv7'),
(4, 'p37-linux-armv7-rpi'),
(4, 'p37-linux-x86_64');


-- Command Type Routing


INSERT INTO command_type(name, entity_table) values 
("API", "api_command_invocation_registry"),
("METRIC", "metric_command_invocation_registry"),
("SHELL", "shell_command_invocation_registry"),
("PRESET", "hypervisor_command_invocation_registry");


-- Table Indexes


ALTER TABLE shell_command ADD INDEX index__shell_command__name (name);
ALTER TABLE shell_command ADD INDEX index__shell_command__preview (preview(50));
ALTER TABLE shell_command ADD INDEX index__shell_command__interpreter (interpreter);
ALTER TABLE shell_command ADD INDEX index__shell_command__is_metric (is_metric);
ALTER TABLE shell_command ADD INDEX index__shell_command__deprecation_date (deprecation_date);

ALTER TABLE shell_command_architecture ADD INDEX index__shell_command_architecture__architecture (architecture);

ALTER TABLE command_type ADD INDEX index__command_type__name (name);

ALTER TABLE campaign ADD INDEX index__campaign__name (name);
ALTER TABLE campaign ADD INDEX index__campaign__id_last_schedule (id_last_schedule);
ALTER TABLE campaign ADD INDEX index__campaign__scheduled (scheduled);
ALTER TABLE campaign ADD INDEX index__campaign__suspendable (suspendable);
ALTER TABLE campaign ADD INDEX index__campaign__abortable (abortable);
ALTER TABLE campaign ADD INDEX index__campaign__n_subjobs (n_subjobs);
ALTER TABLE campaign ADD INDEX index__campaign__on_error (on_error);

ALTER TABLE schedule ADD INDEX index__schedule__status (status);
ALTER TABLE schedule ADD INDEX index__schedule__start_time (start_time);
ALTER TABLE schedule ADD INDEX index__schedule__end_time (end_time);
ALTER TABLE schedule ADD INDEX index__schedule__n_exited_subjobs (n_exited_subjobs);

ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__id_target (id_target);
ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__id_subjob (id_subjob);
ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__architecture (architecture);
ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__status (status);
ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__start_time (start_time);
ALTER TABLE target_campaign_status ADD INDEX index__target_campaign_status__end_time (end_time);

ALTER TABLE task_invocation_result ADD INDEX index__task_invocation_result__id_target (id_target);
ALTER TABLE task_invocation_result ADD INDEX index__task_invocation_result__id_subjob (id_subjob);
ALTER TABLE task_invocation_result ADD INDEX index__task_invocation_result__status (status);
ALTER TABLE task_invocation_result ADD INDEX index__task_invocation_result__start_time (start_time);
ALTER TABLE task_invocation_result ADD INDEX index__task_invocation_result__end_time (end_time);

ALTER TABLE command_invocation_result ADD INDEX index__command_invocation_result__id_target (id_target);
ALTER TABLE command_invocation_result ADD INDEX index__command_invocation_result__id_subjob (id_subjob);
ALTER TABLE command_invocation_result ADD INDEX index__command_invocation_result__delivery_status (delivery_status);
ALTER TABLE command_invocation_result ADD INDEX index__command_invocation_result__status_code (status_code);
ALTER TABLE command_invocation_result ADD INDEX index__command_invocation_result__execution_date (execution_date);
