#!/usr/bin/env bash
if [ "$#" -ne 9 ]; then
  echo 
  echo "####################################################################################"
  echo
  echo "Wrong number of parameters ($# provided)"
  echo 
  echo "Usage: $0 <MYSQL_USER> <MYSQL_PASSWORD> <MYSQL_ROOT_PASSWORD> <MYSQL_HOST_PORT_TCP> <MYSQL_HOST_PORT_PROTOBUF> <MAX_LOG_DIM> <MAX_LOG_FILES> <IMAGE_TAG> <AS_CONTAINER_NAME>"
  echo 
  echo "Example: $0 asuser alfabeto alfabeto 3309 33090 10m 3 latest iotcatalyst_automation_service"
  echo 
  echo "arguments:"
  echo "MYSQL_HOST_PORT_TCP         Number - The host port used to expose MySQL TCP port"
  echo "MYSQL_HOST_PORT_PROTOBUF    Number - The host port used to expose MySQL Protobuf port"
  echo "MAX_LOG_DIM                 String - The maximum size of the log before it is rolled. A positive integer plus a modifier representing the unit of measure (k, m, or g)"
  echo "MAX_LOG_FILES               Number - The maximum number of log files that can be present"
  echo "IMAGE_TAG                   String - The tag of the DB Docker image"
  echo "AS_CONTAINER_NAME           String - The name of the Automation Service container. Used to associate DB storage with the container"
  echo
  echo "####################################################################################"
  echo
  exit 1
else
  MYSQL_USER="$1"
  MYSQL_PASSWORD="$2"
  MYSQL_ROOT_PASSWORD="$3"
  MYSQL_HOST_PORT_TCP="$4"
  MYSQL_HOST_PORT_PROTOBUF="$5"
  MAX_LOG_DIM="$6"
  MAX_LOG_FILES="$7"
  IMAGE_TAG="$8"
  AS_CONTAINER_NAME="$9"
  docker run -e MYSQL_USER="$MYSQL_USER" -e MYSQL_PASSWORD="$MYSQL_PASSWORD" -e MYSQL_ROOT_PASSWORD="$MYSQL_ROOT_PASSWORD" \
      -p "$MYSQL_HOST_PORT_TCP":3306 -p "$MYSQL_HOST_PORT_PROTOBUF":33060 \
      --log-driver json-file --log-opt max-size="$MAX_LOG_DIM" --log-opt max-file="$MAX_LOG_FILES" \
      --name iotcatalyst_automation_service_db \
      --mount source="$AS_CONTAINER_NAME"_mysql_storage,target=/var/lib/mysql \
      --restart unless-stopped \
      -d fwdigital/iotcatalyst_automation_service_db:"$IMAGE_TAG"
fi
