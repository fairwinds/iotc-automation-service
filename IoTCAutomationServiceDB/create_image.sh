#!/usr/bin/env bash
if [ "$#" -ne 1 ]; then
  echo 
  echo "####################################################################################"
  echo
  echo "Wrong number of parameters"
  echo 
  echo "Usage: $0 <IOT CATALYST AUTOMATION SERVICE IMAGE TAG>"
  echo 
  echo "####################################################################################"
  echo
else
  docker build -t fwdigital/iotcatalyst_automation_service_db:$1 .
fi
