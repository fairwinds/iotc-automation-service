from collections import namedtuple
from http import HTTPStatus
import json
import os
from pathlib import Path
import sys
from typing import List, Tuple, Dict, Any
import unittest
from unittest.mock import ANY, Mock, patch
import fakeredis
from parameterized import parameterized

from tests import load_test_data

sys.path.append(".")
os.environ["AUTOMATION_SERVICE_PORT"] = "0"
os.environ["REDIS_PORT"] = "0"
os.environ["CACHE_EX"] = "100000"
from IoTCAutomationService.common.enums import IoTCDBCursorMethod, IoTCEntityType, IoTCDBFilename, IoTCDBTablename, IoTCDBType

FAKE_DB_FILENAME = "IOTCAUTOMATIONSERVICE_DB_FOR_UNITTESTS.db"

from IoTCAutomationService.persistence.log import IoTCServiceLogger
IoTCServiceLogger.get = Mock()

from IoTCAutomationService.common.common import (MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST, MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE, IoTCAPIClientManager,
                                                 IoTCStudioCredentials, configure_logging)
from IoTCAutomationService.campaign.common import IoTCCampaignScheduler, IoTCJobPersistenceManager
from IoTCAutomationService.campaign.campaign import IoTCCampaign, IoTCCampaignDispatcher
from IoTCAutomationService.IoTCMqttClient import IoTCMqttClient, IoTCMqttClientManager
from IoTCAutomationService.mqtt_client import IoTCMqttClientGeneric


def FullName(obj):
    """ returns '<module name>.<class or method or attribute name>'"""
    return f"{obj.__module__}.{obj.__qualname__}"


CommandResult = namedtuple("CommandResult", ["status_code", "result", "execution_date"])


class TestJobs(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

    @classmethod
    def setUpClass(cls) -> None:
        try:
            TestJobs.credentials = IoTCStudioCredentials(
                os.environ["AS_STUDIO_USERID"], 
                os.environ["AS_STUDIO_PASSWORD"], 
                None, 
                os.environ["AS_STUDIO_URL"]
            )
        except KeyError:
            print("No env variables found: AS_STUDIO_USERID, AS_STUDIO_PASSWORD, AS_STUDIO_URL")
            sys.exit(1)
        TestJobs.redis_mock = fakeredis.FakeStrictRedis()
        TestJobs.api_client = IoTCAPIClientManager.create(TestJobs.credentials)
        TestJobs.hv_cache = IoTCCampaignDispatcher().extract_cache(IoTCEntityType.HYPERVISOR.value)
        TestJobs.hv_cache._redis_client = TestJobs.redis_mock
        TestJobs.campaign_dispatcher = IoTCCampaignDispatcher()
        IoTCJobPersistenceManager.redis_client = TestJobs.redis_mock
        TestJobs.MQTT_DOMAIN = "FWD"
        IoTCServiceLogger.get()._model = Mock()

    @classmethod
    def tearDownClass(cls) -> None:
        if Path(FAKE_DB_FILENAME).exists:
            Path(FAKE_DB_FILENAME).unlink()
        
    def setUp(self) -> None:
        with patch(FullName(IoTCMqttClientGeneric)):
            self.db = IoTCDB.get()
            self.campaign_model = IoTCJobModel()
            TestJobs.mqtt_client = IoTCMqttClient(log_name=None, transport="tcp", client_id=0, username="")
            TestJobs.mqtt_client.domain_name = TestJobs.MQTT_DOMAIN
            TestJobs.mqtt_client.send = Mock()
            TestJobs.mqtt_client.subscribe = Mock()
            TestJobs.mqtt_client.unsubscribe = Mock()
            IoTCMqttClientManager.get = Mock(return_value=TestJobs.mqtt_client)

    def tearDown(self) -> None:
        for table in IoTCDBTablename:
            self.db.execute(f"DROP TABLE IF EXISTS {table.value}", [], None)

    def mqtt_campaign_response_message(self, operation_type: str):
        return json.dumps({
            "MessageBody": {
                "operation": {
                    "type": operation_type,
                    "commands": []
                }
            }
        })

    def create_campaign_identity(self, campaign_def):
        """
        Create campaign identity data but do not run it.
        """
        with patch.object(TestJobs.campaign_dispatcher, "_execute_now"), patch.object(TestJobs.campaign_dispatcher, "_execute_delayed"), patch.object(TestJobs.campaign_dispatcher, "_execute_every_interval"), patch.object(TestJobs.campaign_dispatcher, "_execute_on_cron"):
            id_campaign, _ = TestJobs.campaign_dispatcher.create_campaign(campaign_def)
            return id_campaign

    def start_campaign_stuck(self, id_campaign, campaign_def):
        """
        Create a campaign object without starting any task.
        """
        with patch(FullName(IoTCCampaign._spawn_subjobs)):
            targets = campaign_def["target"]["ids"]
            return IoTCCampaign(id_campaign, campaign_def, "hypervisor", targets, 1, 1)

    def patch_subjobs_caches(self, campaign: IoTCCampaign):
        """Mock the subjobs' cache that internally uses redis"""
        for subjob in campaign._subjobs:
            subjob._hypervisor_cache = TestJobs.hv_cache

    def mqtt_response_topic(self, id_target, id_campaign, id_task, id_schedule, response_suffix):
        return MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE(TestJobs.MQTT_DOMAIN, id_target, id_campaign, id_task, id_schedule, response_suffix)

    def mqtt_request_topic(self, id_target, id_campaign, id_task, id_schedule):
        return MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST(TestJobs.MQTT_DOMAIN, id_target, id_campaign, id_task, id_schedule)

    def get_first_target(self, campaign_def):
        return campaign_def["target"]["ids"][0]

    def get_first_id_task(self, campaign_def):
        return campaign_def["tasks"][0]["id_task"]

    def test_extract_hv_cache(self):
        self.assertEqual("idHypervisor", TestJobs.hv_cache._entityid_key)

    def test_cache_get_entity_attributes(self):
        self.assertIn("idHypervisor", TestJobs.hv_cache.get_entity_attributes())

    def test_cache_at_least_one_hypervisor(self):
        hv_ids = TestJobs.hv_cache.get_all_ids()
        self.assertGreaterEqual(len(hv_ids), 1)

    @parameterized.expand(load_test_data)
    def test_create_job_execute_now(self, _, campaign_def: Dict[str, Any]):
        with patch.object(TestJobs.campaign_dispatcher, "_execute_now") as mock_exec_now:
            id_campaign, _ = TestJobs.campaign_dispatcher.create_campaign(campaign_def)
            mock_exec_now.assert_called_once_with(id_campaign, IoTCCampaign, campaign_def, ANY)

    # TODO:
    def test_spawn_subjobs(self):
        # create job
        # assert that the correct number of subjobs is spawned
        # delete job
        self.fail("To be done")
    
    @parameterized.expand(load_test_data)
    def test_persist_initial_campaign_state(self, _, campaign_def: Dict[str, Any]):
        """
        Given immediate campaign, when it is started, campaign and schedule are both persisted.
        """
        id_campaign = self.create_campaign_identity(campaign_def)
        self.start_campaign_stuck(id_campaign, campaign_def)
        self.assertTrue(self.campaign_model.exists(id_campaign))
        self.assertFalse(self.campaign_model.exists(id_campaign + 1))
        self.assertEqual(1, len(self.campaign_model.get_all_ids_schedules(id_campaign)))
        
    """TODO: Given a scheduled campaign, when the campaign information is persisted, there is no schedule associated."""

    @parameterized.expand(load_test_data)
    def test_campaign_never_run_has_progress_zero(self, _, campaign_def: Dict[str, Any]):
        """
        Given a campaign created but not yet executed, when its stats are retrieved, its progress is equal to zero.
        """
        id_campaign = self.create_campaign_identity(campaign_def)
        campaign = self.start_campaign_stuck(id_campaign, campaign_def)
        id_schedule = campaign.id_schedule
        progress = self.campaign_model.get_progress(id_campaign, id_schedule)
        self.assertEqual(0, int(progress))

    def test_subjob_launch_tasks(self):
        """
        TODO: Given campaign with single subjob and N tasks, when the subjob thread is started, then N tasks are executed
        """
        self.fail("To be done")
    
    @parameterized.expand(load_test_data)
    def test_pubsub_task_topic(self, _, campaign_def: Dict[str, Any]):
        """
        Given a task, when it is executed, then it sends mqtt operations to the broker and subscribes to the response topic.
        """
        id_target = self.get_first_target(campaign_def)

        id_campaign = self.create_campaign_identity(campaign_def)
        campaign = self.start_campaign_stuck(id_campaign, campaign_def)
        self.patch_subjobs_caches(campaign)

        id_schedule = campaign.id_schedule
        id_task = self.get_first_id_task(campaign_def)

        topic_response = self.mqtt_response_topic(id_target, id_campaign, id_task, id_schedule, "")
        def simulate_response(*args, **kwargs):
            response = bytes(self.mqtt_campaign_response_message("COMMAND"), "utf8")
            TestJobs.mqtt_client.receive(topic_response, response)

        TestJobs.mqtt_client.send.side_effect = simulate_response
        campaign._spawn_subjobs()
        TestJobs.mqtt_client.send.side_effect = None
        topic_request = self.mqtt_request_topic(id_target, id_campaign, id_task, id_schedule)
        TestJobs.mqtt_client.send.assert_called_once_with(topic_request, ANY)
        
        actual_request = TestJobs.mqtt_client.send.call_args.args[1]
        actual_request = json.loads(actual_request.decode())
        self.assertTrue("MessageBody" in actual_request)
        self.assertTrue("operation" in actual_request["MessageBody"])
        expected_request_fields = campaign_def["tasks"][0]["operations"][0]
        actual_request_fields = actual_request["MessageBody"]["operation"]
        self.assertEqual(expected_request_fields, actual_request_fields)

        topic_response = self.mqtt_response_topic(id_target, id_campaign, id_task, id_schedule, "")
        TestJobs.mqtt_client.subscribe.assert_called_with((topic_response, ANY))
    
    def simulate_task_output(self, topic, task_result):
        message = json.dumps({"MessageBody": task_result})
        TestJobs.mqtt_client.receive(topic, bytes(message, "utf8"))
    
    # TODO: test mixed mqtt/api tasks

    # TODO: Before launching a campaign, end time set as null

    # TODO: When campaign finishes its execution and all commands executed successfully, its overall status is "exited ok"

    """
    TODO: Given a campaign successfully completed, when statistics are retrieved,
    they contain, for each task-operation pair, the status_code, command, and result of each command
    """

    def test_call_api_method(self):
        api_result = IoTCAPIClientManager.get().call_api_method_raw("apiinfo.getAPIMethods", {})
        self.assertEqual(HTTPStatus.OK.value, api_result.status_code)
        self.assertIn("data", api_result.json())

    
class TestPersistence(unittest.TestCase):
        
    def tearDownClass(cls) -> None:
        if Path(FAKE_DB_FILENAME).exists:
            Path(FAKE_DB_FILENAME).unlink()
            
    def setUp(self) -> None:
        self.db = IoTCDB.get()
        self.campaign_model = IoTCJobModel()
        self.campaign_dispatcher = IoTCCampaignDispatcher()

    def tearDown(self) -> None:
        for table in IoTCDBTablename:
            self.db.execute(f"DROP TABLE IF EXISTS {table.value}", [], None)
    
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
    
    @parameterized.expand(load_test_data)
    def test_campaign_insert(self, _, campaign_def: Dict[str, Any]):
        """
        When the campaign data is persisted, performing a query on the DB will return it.
        """
        targets = self.campaign_dispatcher._extract_targets(campaign_def)
        row = self.campaign_dispatcher._convert_definition_to_row(campaign_def, targets)
        self.campaign_model.insert(row)
        query_result = self.db.execute(f"SELECT * FROM {IoTCDBTablename.JOB}", [], IoTCDBCursorMethod.FETCHONE)
        self.assertIsNotNone(query_result)
    
    @parameterized.expand(load_test_data)
    def test_campaign_exists(self, _, campaign_def: Dict[str, Any]):
        """
        When a campaign is inserted into the database,
        its persistence manager says that it exists.
        """
        # self.db.execute("INSERT INTO ", )