import os
from pathlib import Path
import sys
from typing import Dict
import unittest
from unittest.mock import Mock
from parameterized import parameterized

from tests import load_json_datasets, setUp

setUp()

from IoTCAutomationService.common.api_client import IoTCAPIClient
from IoTCAutomationService.common.enums import IoTCCampaignStatus
from IoTCAutomationService.persistence.command_library import IoTCCommandRepository
from IoTCAutomationService.api import _persist_shell_commands
from IoTCAutomationService.persistence.database import configure_database
from IoTCAutomationService.campaign.campaign import IoTCCampaignDispatcher
from IoTCAutomationService.campaign.target import IoTCTargetCalculator
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.campaign.stats import IoTCCampaignStats
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository


if os.environ.get("MYSQL_PORT") is None or os.environ.get("MYSQL_USER") is None or os.environ.get("MYSQL_PASSWORD") is None:
    print(f"ERROR: You must provide MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD environment variables.")
    sys.exit(1)

os.environ["MYSQL_HOST"] = "127.0.0.1"
os.environ["MYSQL_ROOT_PASSWORD"] = os.environ["MYSQL_PASSWORD"]
print(f"MYSQL_HOST = {os.environ['MYSQL_HOST']}")
print(f"MYSQL_PORT = {os.environ['MYSQL_PORT']}")
print(f"MYSQL_USER = {os.environ['MYSQL_USER']}")
print(f"MYSQL_PASSWORD = {os.environ['MYSQL_PASSWORD']}")         

class TestScheduleDetail(unittest.TestCase):
    
    PATH_CAMPAIGNS = Path(__file__).resolve().parent.parent / "datasets" / "campaign_definitions"
    
    @classmethod
    def setUpClass(cls):
        cls.engine = configure_database()
    
    @parameterized.expand(load_json_datasets(PATH_CAMPAIGNS))
    def test_add_campaign_and_retrieve(self, _, campaign_def: Dict):
        persistence = IoTCCampaignRepository(TestScheduleDetail.engine)
        campaign_stats = IoTCCampaignStats(persistence)
        
        hypervisor_cache = Mock(spec=IoTCHypervisorCache)
        hypervisor_cache.get_name.return_value = "dummy-hypervisor-name"
        hypervisor_cache.get_architecture.return_value = "dummy-hypervisor-architecture"
        target_calculator = IoTCTargetCalculator(hypervisor_cache=hypervisor_cache, api_client=Mock(spec=IoTCAPIClient))
        campaign_targets = target_calculator.extract_campaign_targets(campaign_def)
        
        # Create a new campaign.
        _persist_shell_commands(campaign_def, campaign_targets, IoTCCommandRepository(self.engine))
        n_subjobs = IoTCCampaignDispatcher(scheduler=Mock(), persistence=Mock(), hypervisor_cache=Mock(), task_factory=Mock())._compute_num_of_subjobs(campaign_def, campaign_targets)
        id_campaign, _ = persistence.add_campaign(campaign_def=campaign_def, targets=campaign_targets, n_subjobs=n_subjobs)
        
        # Retrieve the campaign.
        (
            campaigns,
            _,
            _,
            _,
            _
        ) = campaign_stats.get_all(only_scheduled=False, statuses=[status for status in IoTCCampaignStatus], page=1, results_per_page=10000)
        campaign_ids = [campaign["id_campaign"] for campaign in campaigns]
        self.assertIn(id_campaign, campaign_ids)
        campaign = [campaign for campaign in campaigns if campaign["id_campaign"] == id_campaign][0]
        
        # A never scheduled campaign should not have "last_schedule"
        self.assertIn("last_schedule", campaign)
        self.assertIsNone(campaign["last_schedule"])
        
        # Campaign details should include its last schedule status
        self.assertIn("status", campaign)
        self.assertEqual(IoTCCampaignStatus.NEVER_RUN, IoTCCampaignStatus(campaign["status"]))
        self.assertIn("progress", campaign)
        self.assertIn("start_time", campaign)
        self.assertIn("end_time", campaign)
        
        # Campaign details should include general information
        self.assertIn("targets", campaign)
        self.assertIsInstance(campaign["targets"], list)
        target_ids = [target.id_target for target in campaign_targets]
        self.assertEqual(sorted(target_ids), sorted(campaign["targets"]))
        
        # with patch("IoTCAutomationService.campaign.campaign.IoTCCampaign._spawn_subjobs"):
        #     target_to_subjob = IoTCCampaign(
        #         id=id_campaign,
        #         campaign_def=campaign_def,
        #         n_targets_per_subjob=
        #     )
        # # Now add a schedule to the campaign.
        # id_schedule = persistence.add_schedule(
        #     campaign_def=campaign_def,
        #     targets=campaign_targets,
        #     target_to_subjob=
        # )