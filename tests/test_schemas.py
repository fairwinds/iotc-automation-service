import json
from pathlib import Path
from typing import Any, Dict
import unittest
from parameterized import parameterized
from jsonschema import validate as jsonschema_validate

from tests import setUp
setUp()

from IoTCAutomationService.schemas import CAMPAIGN_JSON_SCHEMA, LIBRARY_COMMAND_JSON_SCHEMA
from tests import load_json_datasets


PATH_DATASETS = Path(__file__).resolve().parent / "datasets"


class TestCampaignValidation(unittest.TestCase):
    """
    Tests for campaign json definition validation.
    """
    
    PATH_VALID_CAMPAIGNS = PATH_DATASETS / "campaign_definitions"

    @parameterized.expand(load_json_datasets(PATH_VALID_CAMPAIGNS))
    def test_validate_campaign_definition(
        self, _, campaign_def: Dict
    ) -> None:
        jsonschema_validate(instance=campaign_def, schema=CAMPAIGN_JSON_SCHEMA)

        
class TestLibraryCommandValidation(unittest.TestCase):
    """
    Tests for library command body (used for creation/update) validation.
    """
    
    PATH_VALID_COMMANDS = PATH_DATASETS / "library_commands" / "should_pass"
    
    PATH_INVALID_COMMANDS = PATH_DATASETS / "library_commands" / "should_fail"
    
    @parameterized.expand(load_json_datasets(PATH_VALID_COMMANDS))
    def test_should_pass(self, _, command_def: Dict):
        jsonschema_validate(instance=command_def, schema=LIBRARY_COMMAND_JSON_SCHEMA)
    
    @parameterized.expand(load_json_datasets(PATH_INVALID_COMMANDS))
    def test_should_fail(self, _, command_def: Dict):
        """An invalid command is received -> An exception is raised."""
        self.assertRaises(Exception, jsonschema_validate, **{"instance": command_def, "schema": LIBRARY_COMMAND_JSON_SCHEMA})