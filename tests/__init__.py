import json
from pathlib import Path
from typing import List, Tuple
import os
import sys
from unittest.mock import Mock


def load_test_data() -> List[Tuple[str, dict]]:
    current_dir = Path(__file__).resolve().parent
    return load_json_datasets(Path(current_dir) / "datasets" / "campaign_definitions")
    # input_paths = list(
    #     Path(current_dir, "datasets", "campaign_definitions").glob("*.json")
    # )
    # inputs = [json.loads(path.read_text()) for path in input_paths]
    # return [
    #     (input_path.name, content) for input_path, content in zip(input_paths, inputs)
    # ]
    
    
def load_json_datasets(parent_directory: Path):
    input_paths = list(
        parent_directory.glob("*.json")
    )
    inputs = [json.loads(path.read_text()) for path in input_paths]
    return [
        (input_path.name, content) for input_path, content in zip(input_paths, inputs)
    ]


def setUp():
    """
    Basic configuration for all tests.

    NOTE: You must run this BEFORE importing the codebase modules.
    """
    sys.path.append(".")
    os.environ["AUTOMATION_SERVICE_PORT"] = "0"
    os.environ["REDIS_PORT"] = "0"
    os.environ["CACHE_EX"] = "100000"
    sys.argv.append("aaa") # Automation Service secret
    from IoTCAutomationService.persistence.log import IoTCServiceLogger
    from IoTCAutomationService.persistence.log import IoTCLogRepository

    # Redirect logs to stdout
    service_logger = IoTCServiceLogger(persistence=Mock(spec=IoTCLogRepository))
    service_logger._log = Mock()
    service_logger._log.side_effect = lambda level, message: print(
        f"{level.value} - {message}"
    )
    IoTCServiceLogger.instance = service_logger
    pass
