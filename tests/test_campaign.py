from typing import List
import unittest
from unittest.mock import ANY, Mock, patch
from parameterized import parameterized

from tests import setUp

setUp()

from IoTCAutomationService.common.enums import IoTCCampaignStatus, IoTCOnCampaignError
from IoTCAutomationService.common.types import IoTCTargetArchitecture
from IoTCAutomationService.campaign.campaign import IoTCCampaign
from IoTCAutomationService.campaign.task import IoTCTaskFactory
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository


class TestCampaign(unittest.TestCase):
    def test_add_schedule(self):
        """
        When a campaign object is created, a new schedule is created.
        """
        id_campaign = 1
        id_schedule = 42
        persistence = Mock(spec=IoTCCampaignRepository)

        persistence.add_schedule.return_value = id_schedule
        with patch(
            "IoTCAutomationService.campaign.campaign.IoTCCampaign._spawn_subjobs"
        ):
            campaign = IoTCCampaign(
                id=id_campaign,
                campaign_def={
                    "execution": {
                        "on_error": "exit",
                    },
                    "tasks": [],
                },
                target_type="hypervisor",
                targets=[],
                n_targets_per_subjob=0,
                n_subjobs=0,
                task_registry={},
                get_persistence=lambda: persistence,
                get_hypervisor_cache=lambda: Mock(spec=IoTCHypervisorCache),
                get_task_factory=lambda: Mock(spec=IoTCTaskFactory),
            )
            persistence.add_schedule.assert_called_once()
            self.assertEqual(id_schedule, campaign.id_schedule)

    @parameterized.expand(
        [
            (3, 2),
            (1, 50),
        ]
    )
    def test_create_subjob_quantity(self, n_subjobs: int, n_targets_per_subjob: int):
        """
        A campaign creates the correct number of subjobs.
        Every subjob has the correct number of targets.
        """
        id_campaign = 1

        campaign_def = {
            "execution": {
                "on_error": "exit",
            },
            "tasks": [],
        }

        with patch(
            "IoTCAutomationService.campaign.campaign.IoTCCampaign._spawn_subjobs"
        ):
            campaign = IoTCCampaign(
                id=id_campaign,
                campaign_def=campaign_def,
                target_type="hypervisor",
                targets=self._get_dummy_targets(n_subjobs, n_targets_per_subjob),
                n_targets_per_subjob=n_targets_per_subjob,
                n_subjobs=n_subjobs,
                task_registry={},
                get_persistence=lambda: Mock(spec=IoTCCampaignRepository),
                get_hypervisor_cache=lambda: Mock(spec=IoTCHypervisorCache),
                get_task_factory=lambda: Mock(spec=IoTCTaskFactory),
            )
            self.assertEqual(n_subjobs, len(campaign._subjobs))
            for subjob in campaign._subjobs:
                self.assertEqual(n_targets_per_subjob, len(subjob._targets))

    @parameterized.expand(
        [
            (
                1,
                1,
                [1],
                [0],
                IoTCOnCampaignError.EXIT,
                IoTCCampaignStatus.EXITED_OK,
            ),  # All targets succeeded
            (
                1,
                1,
                [0],
                [1],
                IoTCOnCampaignError.EXIT,
                IoTCCampaignStatus.EXITED_KO,
            ),  # Any failed, on_error exit
            (
                1,
                1,
                [0],
                [1],
                IoTCOnCampaignError.SKIP,
                IoTCCampaignStatus.EXITED_OK,
            ),  # Any failed, on_error skip
            (
                4,
                3,
                [3, 3, 3, 3],
                [0, 0, 0, 0],
                IoTCOnCampaignError.EXIT,
                IoTCCampaignStatus.EXITED_OK,
            ),  # All targets succeeded
            (
                4,
                3,
                [2, 3, 3, 3],
                [1, 0, 0, 0],
                IoTCOnCampaignError.EXIT,
                IoTCCampaignStatus.EXITED_KO,
            ),  # Any failed, on_error exit
            (
                4,
                3,
                [2, 3, 3, 3],
                [1, 0, 0, 0],
                IoTCOnCampaignError.SKIP,
                IoTCCampaignStatus.EXITED_OK,
            ),  # Any failed, on_error skip
        ]
    )
    def test_final_schedule_status(
        self,
        n_subjobs: int,
        n_targets_per_subjob: int,
        subjob_targets_succeeded: List[int],
        subjob_targets_failed: List[int],
        campaign_on_error: IoTCOnCampaignError,
        expected_status: IoTCCampaignStatus,
    ):
        """
        Set the correct schedule final status based on the results on the targets.
        Campaign on_error=skip AND any failed target -> exit ok
        Campaign on_error=exit AND any failed target -> exit ko
        Campaign on_error=<any> AND all targets succeeded -> exit ok
        """
        id_campaign = 42
        campaign_def = {
            "execution": {
                "on_error": campaign_on_error.value,
            },
            "tasks": [],
        }
        persistence = Mock(spec=IoTCCampaignRepository)
        with patch(
            "IoTCAutomationService.campaign.campaign.IoTCCampaign._spawn_subjobs"
        ):
            campaign = IoTCCampaign(
                id=id_campaign,
                campaign_def=campaign_def,
                target_type="hypervisor",
                targets=self._get_dummy_targets(n_subjobs, n_targets_per_subjob),
                n_targets_per_subjob=n_targets_per_subjob,
                n_subjobs=n_subjobs,
                task_registry={},
                get_persistence=lambda: persistence,
                get_hypervisor_cache=lambda: Mock(spec=IoTCHypervisorCache),
                get_task_factory=lambda: Mock(spec=IoTCTaskFactory),
            )
            for i, subjob in enumerate(campaign._subjobs):
                subjob.n_targets_succeeded = subjob_targets_succeeded[i]
                subjob.n_targets_failed = subjob_targets_failed[i]

        with patch("IoTCAutomationService.campaign.subjob.IoTCSubjob.start"), patch(
            "IoTCAutomationService.campaign.subjob.IoTCSubjob.join"
        ):
            campaign._spawn_subjobs()
            persistence.set_status_finished.assert_called_once_with(
                id_campaign=id_campaign,
                status=expected_status,
                end_time=ANY,
                id_schedule=ANY,
            )

    def _get_dummy_targets(
        self, n_subjobs: int, n_targets_per_subjob: int
    ) -> List[IoTCTargetArchitecture]:
        """Create a deterministic list of target ids."""
        n_total_targets = n_subjobs * n_targets_per_subjob
        targets = []
        for id_target in range(0, n_total_targets):
            targets.append(
                IoTCTargetArchitecture(id_target, "dummy-name", "dummy-architecture")
            )
        return targets
