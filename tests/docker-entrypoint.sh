#!/usr/bin/env bash

echo "Running unit tests..."
(.venv/bin/python -m coverage run --data-file=automationservice.coverage -m unittest discover -s tests --verbose) &&\
 (cd IoTCAutomationService && ../.venv/bin/python -m coverage run --data-file=datatable.coverage -m unittest discover -s DataTableServer/tests --verbose) &&\
 (.venv/bin/python -m coverage combine IoTCAutomationService/datatable.coverage automationservice.coverage) &&\
 (.venv/bin/python -m coverage report)