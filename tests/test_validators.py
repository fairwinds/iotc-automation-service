import unittest

from IoTCAutomationService.common.enums import IoTCDataType
from IoTCAutomationService.common.errors import WrongRequestError
from IoTCAutomationService.validators import IoTCValidator

from tests import setUp

setUp()


class TestValidateQueryParams(unittest.TestCase):
    def test_bool_invalid(self):
        """
        Required param is not valid -> An exception is raised
        """

        schema = [
            {
                "name": "only_scheduled",
                "required": True,
                "dtype": IoTCDataType.BOOL,
            }
        ]

        # param required but not provided
        params = []
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

        # invalid datatype
        params = {"only_scheduled": None}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

        # ensure int is not casted to bool
        params = {"only_scheduled": 1}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

    def test_bool(self):
        """
        Required param is valid -> It is converted to the right datatype
        """

        schema = [
            {
                "name": "only_scheduled",
                "required": True,
                "dtype": IoTCDataType.BOOL,
            }
        ]

        params = {"only_scheduled": "true"}
        self.assertEqual([True], IoTCValidator.validate_query_params(params, schema))

        params = {"only_scheduled": "false"}
        self.assertEqual([False], IoTCValidator.validate_query_params(params, schema))

        params = {"only_scheduled": True}
        self.assertEqual([True], IoTCValidator.validate_query_params(params, schema))

        params = {"only_scheduled": False}
        self.assertEqual([False], IoTCValidator.validate_query_params(params, schema))

    def test_bool_default(self):
        """
        The param is not provided -> The default value is returned
        The param is valid -> The converted param is returned
        """

        default_value = False
        schema = [
            {
                "name": "only_scheduled",
                "dtype": IoTCDataType.BOOL,
                "required": False,
                "default": default_value,
            }
        ]

        params = {}
        self.assertEqual(
            [default_value], IoTCValidator.validate_query_params(params, schema)
        )

        params = {"only_scheduled": "true"}
        self.assertEqual([True], IoTCValidator.validate_query_params(params, schema))

    def test_array(self):
        """
        Convert the parameter to a list of values.
        """

        schema = [
            {
                "name": "statuses",
                "dtype": IoTCDataType.ARRAY,
                "required": True,
            }
        ]

        params = {"statuses": "exited_ok,never_run,running"}
        self.assertEqual(
            [["exited_ok", "never_run", "running"]],
            IoTCValidator.validate_query_params(params, schema),
        )

        # WARNING: do not put spaces between params!
        params = {"statuses": "exited_ok, never_run, running"}
        self.assertEqual(
            [["exited_ok", " never_run", " running"]],
            IoTCValidator.validate_query_params(params, schema),
        )

        params = {"statuses": "1,2,3"}
        self.assertEqual(
            [["1", "2", "3"]], IoTCValidator.validate_query_params(params, schema)
        )

        # test default value
        schema[0]["required"] = False
        schema[0]["default"] = []
        params = {}
        self.assertEqual([[]], IoTCValidator.validate_query_params(params, schema))

    def test_default_must_be_present_if_optional_parameter(self):
        """
        Optional parameter, default value is not specified in the schema -> Raise an exception.
        """
        schema = [
            {
                "name": "parameter",
                "dtype": IoTCDataType.INT,
                "required": False,
            }
        ]
        params = {}
        self.assertRaises(
            Exception, IoTCValidator.validate_query_params, params, schema
        )

    def test_required_if(self):
        """
        Validate param 'b' based on the provided value of param 'a'.
        """

        default_value = 42
        schema = [
            {
                "name": "a",
                "dtype": IoTCDataType.INT,
                "required": True,
            },
            {
                "name": "b",
                "dtype": IoTCDataType.INT,
                "required-if": "a/>/1",
                "default": default_value,
            },
        ]

        # raise an exception if the condition is satisfied but the required-if param is not specified
        params = {"a": 2}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

        # condition is not satisfied and the required-if param is not specified -> its default value is used
        params = {"a": 0}
        self.assertEqual(
            [0, default_value], IoTCValidator.validate_query_params(params, schema)
        )

        # condition is satisfied and the param is provided -> return the provided values
        params = {"a": 2, "b": 666}
        self.assertEqual([2, 666], IoTCValidator.validate_query_params(params, schema))

    def test_numeric_interval(self):
        """The input value must be inside the specified interval, otherwise raise an exception."""
        schema = [
            {
                "name": "param",
                "dtype": IoTCDataType.INT,
                "interval": [0, 100],
            }
        ]

        params = {"param": 200}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

        params = {"param": 100}
        self.assertEqual([100], IoTCValidator.validate_query_params(params, schema))

    def test_wrong_datatype(self):
        """Input value is not of the right datatype -> raise an exception."""
        schema = [
            {
                "name": "param",
                "dtype": IoTCDataType.INT,
            }
        ]
        params = {"param": "hello"}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

    def test_allowed_values(self):
        """The input value must be one of the specified values, otherwise raise an exception."""
        schema = [
            {
                "name": "param",
                "dtype": IoTCDataType.STRING,
                "allowed_values": ["hello", "world"],
            }
        ]

        params = {"param": "hello"}
        self.assertEqual(["hello"], IoTCValidator.validate_query_params(params, schema))

        params = {"param": "not allowed"}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )

    def test_regex(self):
        """The input value must match the specified regex."""
        schema = [
            {
                "name": "param",
                "dtype": IoTCDataType.STRING,
                "regex": "^[0-9]{2}$",  # exactly 2 numeric digits
            }
        ]

        params = {"param": "12"}
        self.assertEqual(["12"], IoTCValidator.validate_query_params(params, schema))

        params = {"param": "hello"}
        self.assertRaises(
            WrongRequestError, IoTCValidator.validate_query_params, params, schema
        )
