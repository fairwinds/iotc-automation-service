import unittest
from unittest.mock import Mock

from IoTCAutomationService.common.types import IoTCTargetArchitecture
from IoTCAutomationService.campaign.campaign import IoTCCampaignDispatcher
from IoTCAutomationService.campaign.common import IoTCCampaignScheduler
from IoTCAutomationService.campaign.task import IoTCTaskFactory
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository

from tests import setUp

setUp()


class TestCreateCampaign(unittest.TestCase):
    """
    TODO:
    - Create a campaign -> there is no associated schedule
    - Scheduled campaign with invalid schedule -> raises an Exception
        - Every
        - In
        - Cron
    """

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.campaign_def = {
            "name": "test",
            "target": {"idComponentType": 5, "ids": [-1]},
            "execution": {
                "every": {
                    "hours": 0,
                    "minutes": 1,
                },
                "processes": 1,
                "on_error": "pause",
                "suspendable": False,
                "abortable": False,
            },
            "tasks": [],
        }
        self.campaign_targets = [
            IoTCTargetArchitecture(-1, "dummy-name", "dummy-architecture")
        ]

    def test_add_to_persistence(self):
        """
        Create a campaign -> its ID is retrieved from the persistence layer
        """
        scheduler = Mock(spec=IoTCCampaignScheduler)
        persistence = Mock(spec=IoTCCampaignRepository)
        cache = Mock(spec=IoTCHypervisorCache)
        task_factory = Mock(spec=IoTCTaskFactory)

        expected_id_campaign = 42
        campaign_dispatcher = IoTCCampaignDispatcher(
            scheduler, persistence, cache, task_factory
        )
        persistence.add_campaign.return_value = (expected_id_campaign, {})
        actual_id_campaign, _ = campaign_dispatcher.create_campaign(
            self.campaign_def,
            self.campaign_targets,
        )
        persistence.add_campaign.assert_called_once()
        self.assertEqual(expected_id_campaign, actual_id_campaign)

    def test_add_to_scheduler(self):
        """
        Create a scheduled campaign -> it is added to the scheduler
        """
        scheduler = Mock(spec=IoTCCampaignScheduler)
        persistence = Mock(spec=IoTCCampaignRepository)
        cache = Mock(spec=IoTCHypervisorCache)
        task_factory = Mock(spec=IoTCTaskFactory)

        jobs = []

        def add_job(*args, **kwargs):
            jobs.append(None)

        campaign_dispatcher = IoTCCampaignDispatcher(
            scheduler, persistence, cache, task_factory
        )
        scheduler.add_job.side_effect = add_job
        persistence.add_campaign.return_value = (1, {})
        campaign_dispatcher.create_campaign(
            self.campaign_def,
            self.campaign_targets,
        )
        scheduler.add_job.assert_called_once()
        self.assertEqual(1, len(jobs))
