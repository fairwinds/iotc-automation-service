import json
from typing import List, Literal, Union
import unittest
from unittest.mock import ANY, Mock, patch
from parameterized import parameterized
import requests

from tests import setUp

setUp()

from IoTCAutomationService.common.common import (
    MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST,
    MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE,
    PREPARE_MQTT_MESSAGE,
    IoTCStudioCredentials,
)
from IoTCAutomationService.IoTCMqttClient import IoTCMqttClient
from IoTCAutomationService.campaign.task import IoTCApiTask, IoTCMqttTask, IoTCTaskConfig
from IoTCAutomationService.common.enums import IoTCOnCampaignError, IoTCTaskStatus
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.common.api_client import IoTCAPIClient


class TestMqttTask(unittest.TestCase):
    def setUp(self) -> None:
        self.commands = [
            {
                "name": "update",
                "type": "SHELL",
                "command": "sudo apt update",
                "causes_reboot": False,
                "timeout": 30,
                "retries": 5,
            },
            {
                "name": "upgrade",
                "type": "SHELL",
                "command": "sudo apt upgrade",
                "causes_reboot": False,
                "timeout": 30,
                "retries": 5,
            },
            {
                "name": "reboot",
                "command": "sudo reboot",
                "type": "SHELL",
                "causes_reboot": True,
                "timeout": 30,
                "retries": 5,
            },
        ]
        self.task_def = {
            "name": "update OS and reboot",
            "description": "",
            "on_error_policy": "ignore",
            "conditions": [],
            "commands": self.commands,
        }
        self.campaign_name = "update OS and reboot"
        self.campaign_on_error = IoTCOnCampaignError.EXIT
        self.campaign_def = {
            "name": self.campaign_name,
            "execution": {
                "on_error": self.campaign_on_error.value,
            },
            "tasks": [self.task_def],
        }
        self.ids_commands = [1, 2, 3]
        self.id_campaign = 1
        self.id_schedule = 42
        self.id_target = 51
        self.id_task = 23
        self.response_topic_suffix = ""
        self.task_config = IoTCTaskConfig(
            task=self.task_def,
            commands=self.commands,
            ids_commands=self.ids_commands,
            campaign_def=self.campaign_def,
            on_error=self.campaign_on_error,
            id_campaign=self.id_campaign,
            id_schedule=self.id_schedule,
            id_target=self.id_target,
            id_task=self.id_task,
            response_topic_suffix=self.response_topic_suffix,
        )
        self.domain_name = ""
        self.expected_mqtt_pubsub_qos = 2
        return super().setUp()

    def test_publish_called(self):
        """Task execution involves sending commands to the broker."""
        mqtt_client = Mock(spec=IoTCMqttClient)
        mqtt_client.subscribe.return_value = (0, None)
        mqtt_client.connected = True
        mqtt_client.domain_name = self.domain_name
        task = IoTCMqttTask(
            config=self.task_config,
            get_persistence=lambda: Mock(spec=IoTCCampaignRepository),
            get_mqtt_client=lambda: mqtt_client,
        )
        task._message_fingerprint.save = Mock()
        task._can_exit = True

        with patch("time.time") as mock_time, patch(
            "random.getrandbits"
        ) as mock_getrandbits:
            mock_time.return_value = 0
            mock_getrandbits.return_value = 0
            task.execute()
            expected_mqtt_request = PREPARE_MQTT_MESSAGE(
                body={
                    "response_topic": self.response_topic_suffix,
                    "campaign_name": self.campaign_name,
                    "task": {
                        "type": "SHELL",
                        **self.task_def,
                    },
                    "task_hash": task._message_fingerprint.compute_fingerprint(),
                },
                message_type=task._message_type,
            )

        expected_topic_request = MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST(
            self.domain_name,
            self.id_target,
            self.id_campaign,
            self.id_task,
            self.id_schedule,
        )
        mqtt_client.send.assert_called_once_with(
            expected_topic_request,
            bytes(json.dumps(expected_mqtt_request), "utf8"),
            qos=self.expected_mqtt_pubsub_qos,
            retries=None,
        )

    def test_subscribe_called(self):
        """Task execution involves subscribing to the broker response topic."""
        mqtt_client = Mock(spec=IoTCMqttClient)
        mqtt_client.subscribe.return_value = (0, None)
        mqtt_client.connected = True
        mqtt_client.domain_name = self.domain_name
        task = IoTCMqttTask(
            config=self.task_config,
            get_persistence=lambda: Mock(spec=IoTCCampaignRepository),
            get_mqtt_client=lambda: mqtt_client,
        )
        task._message_fingerprint.save = Mock()
        task._can_exit = True
        task.execute()
        expected_topic_response = MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE(
            self.domain_name,
            self.id_target,
            self.id_campaign,
            self.id_task,
            self.id_schedule,
            self.response_topic_suffix,
        )
        mqtt_client.subscribe.assert_called_once_with(
            (expected_topic_response, self.expected_mqtt_pubsub_qos)
        )

    """If subscribe fails during execution, exit with status KO and don't publish."""

    """If publishing fails during execution, exit with status KO."""

    def test_exit_when_callback_is_called(self):
        """The task execution terminates after the subscription callback is called."""
        # mqtt_client.mqtt_client = Mock()
        # mqtt_client.subscribe = Mock()
        # mqtt_client.subscribe.side_effect = lambda (topic, qos): mqtt_client.receive(topic, bytes(json.dumps(mqtt_agent_response)))
        # task.execute()

    @parameterized.expand(
        [
            (
                [0, 0, 0],
                IoTCTaskStatus.EXITED_OK,
            ),  # Every command succeeded -> OK
            (
                [1, 0, 0],
                IoTCTaskStatus.EXITED_KO,
            ),  # Any command failed -> KO
            (
                [0, 1, 1],
                IoTCTaskStatus.EXITED_KO,
            ),  # Any command failed -> KO
            (
                [1, 1, 1],
                IoTCTaskStatus.EXITED_KO,
            ),  # Any command failed -> KO
        ]
    )
    def test_status_based_on_command_result(
        self,
        command_status_codes: List[int],
        expected_status: IoTCTaskStatus,
    ):
        """
        Task status depends on execution result of commands.
        """
        command_results = [
            {
                "type": "SHELL",
                "skipped": False,
                "status_code": status_code,
                "result": "",
                "execution_date": 0,
            }
            for status_code in command_status_codes
        ]
        mqtt_agent_response = {"commands": command_results}
        mqtt_client = Mock(spec=IoTCMqttClient)
        mqtt_client.subscribe.return_value = (0, None)
        mqtt_client.connected = True
        mqtt_client.domain_name = self.domain_name
        task = IoTCMqttTask(
            config=self.task_config,
            get_persistence=lambda: Mock(spec=IoTCCampaignRepository),
            get_mqtt_client=lambda: mqtt_client,
        )
        task._message_fingerprint.save = Mock()

        task._handle_result(mqtt_agent_response)
        self.assertEqual(expected_status, task._result)
        actual_status = task.execute()
        self.assertEqual(expected_status, actual_status)

    def test_persist_started(self):
        """
        Task execution involves persisting task and commands initial status.
        """
        persistence = Mock(spec=IoTCCampaignRepository)
        mqtt_client = Mock(spec=IoTCMqttClient)
        mqtt_client.subscribe.return_value = (0, None)
        mqtt_client.connected = True
        mqtt_client.domain_name = self.domain_name
        task = IoTCMqttTask(
            config=self.task_config,
            get_persistence=lambda: persistence,
            get_mqtt_client=lambda: mqtt_client,
        )
        task._message_fingerprint.save = Mock()
        task._can_exit = True

        task_set_status_args = {
            "id_campaign": self.id_campaign,
            "id_schedule": self.id_schedule,
            "id_target": self.id_target,
            "id_task": self.id_task,
            "start_time": ANY,
        }

        task.execute()
        persistence.set_status_started.assert_any_call(**task_set_status_args)

        for id_command in self.ids_commands:
            persistence.set_status_started.assert_any_call(
                **task_set_status_args,
                id_command=id_command,
            )

        persistence.set_status_finished.assert_not_called()

    @parameterized.expand(
        [
            ([0, 0, 1], IoTCTaskStatus.EXITED_KO),
            ([0, 0, 0], IoTCTaskStatus.EXITED_OK),
        ]
    )
    def test_persist_finished(self, command_status_codes: List[int], expected_status: IoTCTaskStatus):
        """
        Task and command final statuses are persisted once the subscription callback gets called.
        """
        persistence = Mock(spec=IoTCCampaignRepository)
        mqtt_client = Mock(spec=IoTCMqttClient)
        mqtt_client.subscribe.return_value = (0, None)
        mqtt_client.connected = True
        mqtt_client.domain_name = self.domain_name
        task = IoTCMqttTask(
            config=self.task_config,
            get_persistence=lambda: persistence,
            get_mqtt_client=lambda: mqtt_client,
        )
        task._message_fingerprint.save = Mock()
        task._can_exit = True
        
        command_results = [
            {
                "type": "SHELL",
                "skipped": False,
                "status_code": status_code,
                "result": "",
                "execution_date": 0,
            } for status_code in command_status_codes
        ]
        task._handle_result({"commands": command_results})
        task_set_status_args = {
            "id_campaign": self.id_campaign,
            "id_schedule": self.id_schedule,
            "id_target": self.id_target,
            "id_task": self.id_task,
            "end_time": ANY,
        }
        persistence.set_status_finished.assert_any_call(
            **task_set_status_args,
            status=expected_status,
        )

        for id_command, command_result in zip(self.ids_commands, command_results):
            persistence.set_status_finished.assert_any_call(
                **task_set_status_args,
                id_command=id_command,
                command_result=command_result,
            )


class TestApiTask(unittest.TestCase):
    
    def setUp(self) -> None:
        self.commands = [
            {
                "name": "",
                "type": "API",
                "command": "SnippetCategory.getAll",
                "causes_reboot": False,
                "timeout": 1,
                "retries": 0,
                "params": {}
            },
            {
                "name": "",
                "type": "API",
                "command": "Container.get",
                "causes_reboot": False,
                "timeout": 1,
                "retries": 0,
                "params": {
                    "idContainer": "3031"
                }
            },
            {
                "name": "",
                "type": "API",
                "command": "Container.getByName",
                "causes_reboot": False,
                "timeout": 1,
                "retries": 0,
                "params": {
                    "name": "OTConnector_physical_1"
                }
            }
        ]
        self.task_def = {
            "name": "api_task",
            "description": "Call some Studio API methods",
            "on_error_policy": "ignore",
            "conditions": [],
            "commands": self.commands,
        }
        self.campaign_name = "test_api_campaign"
        self.campaign_on_error = IoTCOnCampaignError.EXIT
        self.campaign_def = {
            "name": self.campaign_name,
            "execution": {
                "on_error": self.campaign_on_error.value,
            },
            "tasks": [self.task_def],
        }
        self.ids_commands = [1, 2, 3]
        self.id_campaign = 1
        self.id_schedule = 42
        self.id_target = 51
        self.id_task = 23
        self.response_topic_suffix = ""
        self.task_config = IoTCTaskConfig(
            task=self.task_def,
            commands=self.commands,
            ids_commands=self.ids_commands,
            campaign_def=self.campaign_def,
            on_error=self.campaign_on_error,
            id_campaign=self.id_campaign,
            id_schedule=self.id_schedule,
            id_target=self.id_target,
            id_task=self.id_task,
            response_topic_suffix=self.response_topic_suffix,
        )
        
    def test_refresh_studio_token(self):
        """
        If the API Client token is not valid, an attempt to refresh the token is made.
        
        If the API Client token is valid, the token is not refreshed.
        """
        studio_credentials = IoTCStudioCredentials(
            userid="",
            password="",
            passport=None,
            url="",
        )
        api_client = IoTCAPIClient(studio_credentials=studio_credentials)
        task = IoTCApiTask(
            config=self.task_config,
            get_persistence=lambda: Mock(spec=IoTCCampaignRepository),
            get_api_client=lambda: api_client,
        )
        
        # Simulate that the token is not valid at the first call. Valid at the next calls.
        api_client.verify_token = Mock()
        api_client.verify_token.side_effect = [False, True, True]
        
        # Avoid calling the actual HTTP Studio API.
        api_client.refresh_token = Mock()
        with patch("requests.post") as post:
            command_results = []
            command_result = requests.Response()
            command_result.status_code = 200
            command_result.json = Mock()
            command_result.json.return_value = {
                "success": True,
                "data": "",
            }
            command_results.append(command_result)
            command_results.append(command_result)
            command_results.append(command_result)
            
            post.side_effect = command_results
            task.execute()
        
        self.assertEqual(3, api_client.verify_token.call_count)
        api_client.refresh_token.assert_called_once()