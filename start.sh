#!/usr/bin/env bash

if [ "$#" -ne 5 ] && [ "$#" -ne 6 ]; then
    echo 
    echo "####################################################################################"
    echo
    echo "Wrong number of parameters"
    echo 
    echo "Usage: $0 <REDIS_PORT> <SECRET> <PYTHON_INTERPRETER> <AUTOMATION_SERVICE_PORT> <CACHE_EXPIRATION> [DEBUG PORT]"
    echo 
    echo "Example: $0 6380 aaa .venv/bin/python 9011 24 5678"
    echo 
    echo "arguments:"
    echo "REDIS_PORT                Number - The Automation Service Redis Server TCP Port" 
    echo "SECRET                    String - Secret key used to initialize the Automation Service" 
    echo "PYTHON_INTERPRETER        String - Path (relative or absolute) to the Python executable that will run the Automation Service" 
    echo "AUTOMATION_SERVICE_PORT   Number - The Automation Service Web Server TCP Port" 
    echo "CACHE_EXPIRATION          Number - IoT Catalyst data cache expiration expressed in hours"
    echo "DEBUG PORT                Number - Remote TCP port used to debug the Automation Service in development mode"
    echo
    echo "####################################################################################"
    echo
    exit 1
else
    if [ "$#" -eq 6 ]; then
        echo "Starting automation service in debug mode..."
        REDIS_PORT=$1 AUTOMATION_SERVICE_PORT=$4 CACHE_EX=$5 GEVENT_SUPPORT=True $3 -m debugpy --listen 0.0.0.0:$6 IoTCAutomationService/start.py $2
    else
        echo "Starting automation service..."
        REDIS_PORT=$1 AUTOMATION_SERVICE_PORT=$4 CACHE_EX=$5 $3 IoTCAutomationService/start.py $2
    fi
fi