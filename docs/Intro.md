The Automation Service is an IoT Catalyst add-on which allows to automate device management tasks over multiple Agents available in the IoT Catalyst domain. A typical use case can be updating the OS version of thousands of devices simultaneously.
IoT Catalyst Studio offers a graphical user interface which makes it possible to easily create and monitor the execution of so-called campaigns over multiple target devices. All of the Automation Service features are also exposed as RESTful API endpoints.
It is possible to create campaigns which will execute either immediately or postponed in time (specifying hours and/or minutes).
It is also possible to define a schedule to repeatedly execute a campaign, either specifying hours/minutes, or by using a crontab-like format.
The IoT Catalyst Studio GUI makes it possible to alter the execution status of a campaign via various buttons which allow to pause/abort a campaign, unschedule its execution, or to launch again a finished campaign by cloning it.

A campaign is a series of commands which need to be sequentially executed over one or more target devices.
If a campaign involves more that one device, it is possible to parallelize its execution by splitting it into multiple subjobs: for example, a campaign over 10 targets can be splitted across 10 subjobs (so that every subjob will manage the whole campaign execution for a single target).
The creation of a campaign involves the definition of one or more tasks, whose aim is to group many commands of the same kind:
- API command: it consists of an HTTP call to an IoT Catalyst API method. This is the only type of command which is directly executed by the Automation Service. The other kinds of commands (see below) will be taken in charge by an IoT Catalyst Agent.
- PRESET command: it consists of a command used to Start/Stop/Update an IoT Catalyst Hypervisor.
- SHELL command: it consists of an architecture-specific shell command. Examples of valid commands are 'sudo apt update', or 'df -h': basically anything that can be executed through a shell interface.
- METRIC command: it's a special case of a SHELL command. Whenever an IoT Catalyst Agent executes a METRIC command, the command result is published on an MQTT topic and the value is periodically updated. METRIC commands also allow users to specify conditional execution paths inside a campaign. For example, let's say you want to upgrade your Linux distribution only if there is enough disk space: you can define a METRIC task to check the overall storage availability, and subsequently a SHELL task (containing the commands to update your OS) which will execute based on the result of the previous task.

At any point in time, a campaign will be in one of the following states:
- NEVER_RUN: the campaign has been defined but has not started yet.
- RUNNING: the campaign has been launched but has not terminated yet.
- PAUSED: the campaign execution has been paused (either by an error on a target or by an outside request) and will remain in this state until the user requires to resume it.
- ABORTED: the campaign execution terminated before its time due to an outside request.
- EXITED_OK: the campaign execution completed successfully on all target devices.
- EXITED_KO: the campaign execution terminated before its time due to an error on a target device.