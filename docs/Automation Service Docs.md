# Automation Service

# Table of Contents
- [Introduction](#introduction)
- [Entities](#entities)
  - [Target](#target)
  - [Campaign](#campaign)
  - [Subjob](#subjob)
  - [Targeted Campaign](#targeted-campaign)
  - [Task](#task)
  - [Command](#command)
- [Campaign state diagram](#campaign-state-diagram)
- [Campaign execution - timing diagrams](#campaign-execution---timing-diagrams)
  - [Execution of the whole campaign](#execution-of-the-whole-campaign)
  - [Execution of a subjob](#execution-of-a-subjob)
  - [Execution of a targeted campaign](#execution-of-a-targeted-campaign)
- [Campaign execution - flowchart](#campaign-execution---flowchart)
  - [Targeted Schedule](#targeted-schedule)

<div style="page-break-after: always; break-after: page;"></div>

# Introduction

This document describes the concepts and the algorithms playing a role in the IoT Catalyst Automation Service. For an higher level discussion and API Docs, please refer to [this document](https://www.iotcatalyst.com/APIDocs/automationservice/APIDocs).

# Entities
Here are described the relationships between the entities playing a role in a campaign execution.

## Target 

A device inside the IoTCatalyst domain. An IoT Catalyst Agent running on this device is required.

## Campaign

A series of tasks which have to be executed on one or more targets. A simple, yet powerful campaign may perform the update and reboot procedure over a series of Linux machines.

Users can decide to split the overall campaign workload into multiple processes - called **subjobs** - which allow concurrent execution of the campaign on different targets. 

Every campaign can either be launched in a "single shot" way, or be scheduled over time. For instance, you may decide to trigger a campaign immediately once you've created it, or to schedule its execution every day at 8:00 AM.

Users can also specify an "on error" policy, so that the campaign execution can be interrupted should a task fail. It is then possible to manually resume a partially failed campaign.

Every campaign may (or may not, as specified by its creator) be aborted or suspended from the outside of the Automation Service: it's as simple as calling an API endpoint.

At any point in time after the campaign creation, users can interact with the Automation Service API to retrieve its status on a per-target basis, so as to guarantee the maximum level of detail.

## Subjob

A logical unit which takes in charge a subset of the overall campaign targets. 

## Targeted Campaign

Campaign executed over a single target.

## Task

A series of commands that will be executed sequentially on a specific target.

## Command

A command is the "atom" of an Automation Service campaign. Each command may be as simple as `sudo apt upgrade`, `df -h`, and so on. 

The following command types are currently supported:

- **API**: it is a HTTP call to an IoT Catalyst API method. This is the only type of command which is directly executed by the Automation Service. The other kinds of commands (see below) will be taken in charge by an IoT Catalyst Agent.
  
- **HYPERVISOR**: it is a command used to Start/Stop/Update an IoT Catalyst Hypervisor.
  
- **SHELL**: it is an architecture-specific shell command. Examples of valid commands are 'sudo apt update', or 'df -h': basically anything that can be executed through a shell interface.
  
- **METRIC**: it's a special case of a SHELL command. Whenever an IoT Catalyst Agent executes a METRIC command, the command result is published on an MQTT topic and the value is periodically updated.
METRIC commands also allow users to specify conditional execution paths inside a campaign.
For example, let's say you want to upgrade your Linux distribution only if there is enough disk space: you can define a METRIC task to check the overall storage availability, and subsequently a SHELL task (containing the commands to update your OS) which will execute based on the result of the previous task.

---

To make it easier to visualize the above concepts, here is a simple entity diagram.

```mermaid

    classDiagram
        Campaign "1" -- "*" Subjob
        Subjob "1" -- "*" TargetedCampaign
        Subjob "1" -- "*" Target
        TargetedCampaign "1" -- "1" Target
        TargetedCampaign "1" -- "*" Task
        Task "1" -- "*" Command
    class Campaign {
        name
        description
        n_pools
        on_error
        schedule
        abortable
        suspendable
        status()
    }
    class Subjob {
    }
    class Target {
        id
    }
    class TargetedCampaign {
    }
    class Task {
        type
        name
        description
        conditions
    }
    class Command {
        status_code
        stdout
        execution_date
    }

```

<div style="page-break-after: always; break-after: page;"></div>

# Campaign state diagram

Once a campaign is created, its status will be automatically set to `NEVER_RUN`.

When the campaign execution trigger is detected by the Automation Service, the campaign status is set to `RUNNING` and the campaign subjobs are launched.

If an error occurs while executing a task on any target, different things happen depending on the "on_error" policy defined during campaign creation:
* "skip": the error is ignored and the campaign execution continues
* "pause": the campaign is paused for the target that encountered the error (other targets aren't paused)
* "exit": the campaign execution terminates for the target that encountered the error. The overall campaign status is set to `EXITED_KO`

A `RUNNING` campaign can be set to `PAUSED` from the outside of the Automation Service. In that case, the campaign execution stops on **every** target. Later in time, the campaign can be resumed by calling another Automation Service API endpoint.

A `RUNNING` or `PAUSED` campaign can be `ABORTED` from the outside of the Automation Service. An aborted campaign stops its execution forever and its status cannot be recovered in any way. If the campaign was scheduled for multiple executions, the schedule is canceled.

When the last task is executed for every target, the overall campaign status becomes `EXITED_OK`.

Here is the graphical representation of the campaign state transitions.

```mermaid
stateDiagram-v2
    [*] --> NEVER_RUN: campaign created from API
    NEVER_RUN --> RUNNING: schedule triggered
    RUNNING --> PAUSED: paused from API
    PAUSED --> RUNNING: resumed from API
    PAUSED --> ABORTED: aborted from API
    RUNNING --> ABORTED: aborted from API
    RUNNING --> EXITED_KO: some task failed <br>and "on_error" policy is "exit"
    RUNNING --> EXITED_OK: all tasks completed, <br>or some task failed <br>and "on_error" policy is "skip"
```

<div style="page-break-after: always; break-after: page;"></div>

# Campaign execution - timing diagrams
## Execution of the whole campaign
A campaign can be parallelized into one or more *concurrent* <u>subjobs</u>. Every subjob will receive as input:
- The campaign tasks 
- The targets upon which the campaign tasks will be executed

The original amount of targets is therefore splitted between different subjobs, potentially optimizing the time required to execute the overall campaign. 
```mermaid
gantt
    axisFormat %
    Subjob 1: pool1, 0, 1h
    ...: pool2, 0, 1h
    Subjob N: pool3, 0, 1h
```

## Execution of a subjob
A subjob sequentially launches a <u>targeted campaign</u> for every target. Every targeted campaign will receive as input:
- The campaign tasks
- The (single) target upon which the campaign tasks will be executed
```mermaid
gantt
    axisFormat %
    section Target 1
    Targeted Campaign: tc1, 0, 1h
    section Target 2
    Targeted Campaign: tc2, after tc1, 1h
    section Target 3
    Targeted Campaign: tc3, after tc2, 1h
```


## Execution of a targeted campaign
A <u>targeted campaign</u> is nothing but a campaign *executed on a single target*. It therefore launches all the tasks *sequentially* on the given target, waiting for the result of the current task before launching the next one.
```mermaid
gantt
    axisFormat %
    section Tasks
    Task 1: task1, 0, 1h
    Task 2: task2, after task1, 1h
    section Commands
    [T1] Command 1: task1_op1, 0, 20m
    ...: task1_op2, after task1_op1, 20m
    [T1] Command N: task1_op3, after task1_op2, 20m
    [T2] Command 1: task2_op1, after task1, 20m
    ...: task2_op2, after task2_op1, 20m
    [T2] Command N: task2_op3, after task2_op2, 20m
```

<div style="page-break-after: always; break-after: page;"></div>

# Campaign execution - flowchart
Here are described in detail the steps taken to execute a schedule of a campaign.

<style>
    svg[id^="mermaid-"] { min-width: 200px; max-width: 500px; max-height: 750px; }
</style>

```mermaid
graph TD
    0[task data] --> 1
    1[target id] --> A
    A{any task pending}
    A --> |Yes| A1[task <-- the next task to be launched] -->C
    A --> |No| EXIT
    C[launch task]
    C --> D
    D{task result is available}
    D -->|No| D
    D -->|Yes| E
    E{task completed successfully}
    E -->|No| F
    E -->|Yes| A
    F{on_error = pause} -->|Yes| G[pause]
    F -->|No| H{on_error = skip} -->|Yes| A
    H -->|No| EXIT
    EXIT[exit]
```
