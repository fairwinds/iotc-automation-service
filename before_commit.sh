#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo 
    echo "####################################################################################"
    echo
    echo "Wrong number of parameters"
    echo 
    echo "Usage: $0 <PYTHON_INTERPRETER>"
    echo
    echo "Example: $0 .venv/bin/python"
    echo 
    echo "arguments:"
    echo "PYTHON_INTERPRETER        String - Path (relative or absolute) to the Python executable that will run the autoformatter" 
    echo
    echo "####################################################################################"
    echo
else
    echo "Autoformatting the code..."
    $1 -m black IoTCAutomationService
fi