#!/usr/bin/env bash

usage_message() {
  local message=$1
  echo
  echo "####################################################################################"
  echo
  echo "$message"
  echo
  echo "Usage: $0 <CREATE_VENV> <PYTHON_EXECUTABLE_PATH> <SOURCES_PATH>"
  echo
  echo "Example: $0 yes /home/devel/.pyenv/versions/3.8.1/envs/automation_service3.8/bin/python3.8 IoTCAutomationService"
  echo
  echo "Description: Run static type checker on python source files."
  echo
  echo "arguments:"
  echo "CREATE_VENV             String - Whether a Python virtualenv should be created. Must be either 'yes' or 'no'."
  echo "PYTHON_EXECUTABLE_PATH  String - Path of the Python executable. If CREATE_VENV='yes', it will be used to create the virtualenv. Otherwise, it will be used to run the static type checker."
  echo "SOURCES_PATH            String - The directory that contains Python source files to be checked."
  echo
  echo "####################################################################################"
  echo
}

if [ "$#" -ne 3 ]; then
  usage_message "Wrong number of parameters ($# provided)"
  exit 1
fi

CREATE_VENV="$1"
PYTHON_EXECUTABLE_PATH="$2"
SOURCES_PATH="$3"

set -e

if [ "$CREATE_VENV" = "yes" ]; then
    echo "INFO: Creating .venv in the current directory..."
    $PYTHON_EXECUTABLE_PATH -m venv .venv
    PYTHON_EXECUTABLE_PATH=".venv/bin/python"
fi

echo "INFO: Installing dependencies using python at path '$PYTHON_EXECUTABLE_PATH'..."
set -o xtrace
$PYTHON_EXECUTABLE_PATH -m pip install --upgrade pip
$PYTHON_EXECUTABLE_PATH -m pip install mypy
set +o xtrace

echo "INFO: Running the static type checker..."
set -o xtrace
$PYTHON_EXECUTABLE_PATH -m mypy --install-types --non-interactive --explicit-package-bases --ignore-missing-imports -p "$SOURCES_PATH"
set +o xtrace
