from abc import ABC, abstractmethod
from typing import Any, Dict, List


class IDBCursorResult(ABC):
    @abstractmethod
    def fetchone(self) -> Dict[str, Any]:
        """Fetch a row from the cursor result set."""
        pass

    @abstractmethod
    def fetchall(self) -> List[Dict[str, Any]]:
        """Fetch all rows from the cursor result set."""
        pass


class IDBConnection(ABC):
    @abstractmethod
    def execute(self, query: str, params: Dict[str, Any]) -> IDBCursorResult:
        """Execute query."""
        pass


class IDBConnectionCtx(ABC):
    """Context manager for a DB transaction."""

    @abstractmethod
    def __enter__(self, *args, **kwargs) -> IDBConnection:
        pass

    @abstractmethod
    def __exit__(self, *args, **kwargs) -> None:
        pass


class IDBConnectionProvider(ABC):
    @abstractmethod
    def begin(self) -> IDBConnectionCtx:
        """Start a database transaction."""
        pass


class IDBStatementFormatter(ABC):
    """DB query statement parameter formatter."""

    @abstractmethod
    def format(self, param_name: str) -> str:
        """
        Format name of a query parameter according to the DB driver implementation.

        Example for SQLAlchemy:
        >>> def format(..., param_name):
                return ":" + param_name
        """
        pass
