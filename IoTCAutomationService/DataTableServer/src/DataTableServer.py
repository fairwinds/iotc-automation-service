# Adapted from: https://legacy.datatables.net/development/server-side/python_mysql

# Modules
from typing import Any, Dict, List, Tuple, Union

from .interface import IDBConnectionProvider, IDBStatementFormatter


class DataTableServer:
    """
    Server-side processing class for DataTable feeding.

    NOTE: This class is meant to work with DataTable version 1.10.19+
    """

    def __init__(
        self,
        database: IDBConnectionProvider,
        stmt_formatter: IDBStatementFormatter,
        table: str,
        index_column: str,
        columns: Dict[str, int],
    ) -> None:
        """
        Args:
        - database: Interface to the database connection provider.
        - stmt_formatter: Interface for db statement parameter formatting.
        - table: Name of the table to be queried.
        - index_column: Name of the column index to be used for fast and accurate table cardinality.
        - columns: Key is the name of the database column, value is the DataTable column index.
        """
        self._database = database
        self._stmt_formatter = stmt_formatter
        self._table = table
        self._index_column = index_column
        self._columns = columns
        self._column_index_to_name: Dict[int, str] = {
            idx: column for column, idx in columns.items()
        }
        self._column_names = list(columns.keys())

    def get_rows(
        self, params: Dict[str, Any], where_result: str, where_all: str
    ) -> Dict[str, Any]:
        """
        Do a database query according to the DataTable param. Retrieve data in a DataTable-friendly format.

        * 'Result condition': This is applied to the result set, but not the
            overall paging information query (i.e. it will not effect the number
            of records that a user sees they can have access to). This should be
            used when you want apply a filtering condition that the user has sent.

            * 'All condition': This is applied to all queries that are made and
        reduces the number of records that the user can access. This should be
            used in conditions where you don't want the user to ever have access to
            particular records (for example, restricting by a login id).

        Args:
        - params: DataTable parameters as a dictionary.
        - where_result: SQL condition to apply to the result set.
        - where_all: SQL condition to apply to all queries.
        """

        bindparams = {}
        count = 0  # used to assign unique names to sql statement params

        format = self._stmt_formatter.format

        # LIMIT, OFFSET
        limit = ""
        if "start" in params and "length" in params:
            bindparams.update(
                {
                    "start": params["start"],
                    "length": params["length"],
                }
            )
            limit = f"LIMIT {format('length')} OFFSET {format('start')}"

        # ORDER BY
        order_by = ""
        if "order" in params:
            order_by_columns: List[
                Tuple[str, str]
            ] = []  # column name along with ASC/DESC
            for order_entry in params["order"]:
                idx_column = int(order_entry["column"])
                column = params["columns"][idx_column]
                order_dir: str = order_entry["dir"].upper()
                if (
                    ("orderable" in column)
                    and (column["orderable"] is True)
                    and ("name" in column)
                    and (column["name"] != "")
                    and (column["name"] in self._column_names)
                    and (order_dir in ["ASC", "DESC"])
                ):
                    order_by_columns.append((column["name"], order_dir))
            order_by = ", ".join(
                [f"{column_name} {dir}" for column_name, dir in order_by_columns]
            )
            if len(order_by) > 0:
                order_by = f"ORDER BY {order_by}"

        # WHERE
        def where_clause(column: Dict[str, Any], search_filter: str) -> str:
            """
            Return a condition to be put in a SQL WHERE clause.

            Args:
                - column_param: Item of DataTable 'columns' array.
                - search_filter: Value to search for in the where clause.
            """
            if (
                ("name" in column)
                and (column["name"] != "")
                and (column["name"] in self._column_names)
                and (column["searchable"] is True)
                and ("search" in column)
                and ("value" in column["search"])
            ):
                nonlocal count
                bindparam_name = f"bindparam_{count}"
                count += 1
                bindparams.update({bindparam_name: "%" + search_filter + "%"})
                return f"{column['name']} LIKE {format(bindparam_name)}"
            else:
                return "TRUE"

        where = ""
        if "columns" in params:
            where_clauses_global: List[str] = []
            where_clauses_per_column: List[str] = []
            columns = [
                column
                for column in params["columns"]
                if column["name"] != "" and column["name"] in self._column_names
            ]
            for column in columns:
                # Individual column filtering
                clause = where_clause(column, column["search"]["value"])
                where_clauses_per_column.append(clause)

                # Global filtering
                if (
                    ("search" in params)
                    and ("value" in params["search"])
                    and (params["search"]["value"] != "")
                ):
                    clause = where_clause(column, params["search"]["value"])
                    where_clauses_global.append(clause)

            if len(where_clauses_global) > 0:
                where = f"({' OR '.join(where_clauses_global)})"
            if len(where_clauses_per_column) > 0:
                if len(where_clauses_global) > 0:
                    where = f"{where} AND"
                where = f"{where} ({' OR '.join(where_clauses_per_column)})"
            if len(where_clauses_global) == 0 and len(where_clauses_per_column) == 0:
                where = "TRUE"
            where = f"WHERE {where}"

        if where_result != "":
            where = (
                f"{where} AND {where_result}"
                if where != ""
                else f"WHERE {where_result}"
            )
        if where_all != "":
            where = f"{where} AND {where_all}" if where != "" else f"WHERE {where_all}"

        target_list = ", ".join(
            [column_name for column_name in self._columns.keys() if column_name != ""]
        )

        query = f"SELECT {target_list} FROM {self._table} {where} {order_by} {limit}"

        with self._database.begin() as transaction:
            cursor = transaction.execute(query, bindparams)
            records = cursor.fetchall()

            query_n_total_records = f"SELECT COUNT({self._index_column}) AS total FROM {self._table} {f'WHERE {where_all}' if where_all != '' else ''}"
            cursor = transaction.execute(query_n_total_records, bindparams)
            n_total_records = int(cursor.fetchone()["total"])

            query_n_records_filtered = f"SELECT COUNT({self._index_column}) AS total FROM {self._table} {where}"
            cursor = transaction.execute(query_n_records_filtered, bindparams)
            n_records_filtered = int(cursor.fetchone()["total"])

        result = {
            "draw": int(params["draw"])
            if ("draw" in params) and (type(params["draw"]) == int)
            else 0,
            "recordsTotal": n_total_records,
            "recordsFiltered": n_records_filtered,
            "data": records,
        }
        return result
