"""
SQLAlchemy implementation for interfaces needed by DataTableServer.
"""

from typing import Any, Dict, List
import sqlalchemy

from DataTableServer.src.interface import (
    IDBConnection,
    IDBConnectionCtx,
    IDBConnectionProvider,
    IDBCursorResult,
    IDBStatementFormatter,
)


class DBCursorResult(IDBCursorResult):
    def __init__(self, cursor_result: sqlalchemy.engine.CursorResult) -> None:
        self._cursor_result = cursor_result

    def fetchone(self) -> Dict[str, Any]:
        row = self._cursor_result.fetchone()
        record = row._asdict()
        return record

    def fetchall(self) -> List[Dict[str, Any]]:
        rows = self._cursor_result.fetchall()
        records = [row._asdict() for row in rows]
        return records


class DBConnection(IDBConnection):
    def __init__(self, conn: sqlalchemy.engine.Connection) -> None:
        self._conn = conn

    def execute(self, query: str, params: Dict[str, Any]) -> IDBCursorResult:
        return DBCursorResult(self._conn.execute(sqlalchemy.text(query), params))


class DBConnectionCtx(IDBConnectionCtx):
    def __init__(self, conn: sqlalchemy.engine.Engine._trans_ctx) -> None:
        self._conn = conn

    def __enter__(self, *args, **kwargs):
        return DBConnection(self._conn.__enter__())

    def __exit__(self, type_, value, traceback):
        return self._conn.__exit__(type_, value, traceback)


class DBConnProvider(IDBConnectionProvider):
    def __init__(self, engine: sqlalchemy.engine.Engine) -> None:
        self._engine = engine

    def begin(self) -> IDBConnectionCtx:
        return DBConnectionCtx(self._engine.begin())


class DBStatementFormatter(IDBStatementFormatter):
    def format(self, param_name: str) -> str:
        return f":{param_name}"
