from typing import Dict, List
import unittest
import sqlalchemy
from parameterized import parameterized

from DataTableServer.src.DataTableServerImplSA import DBConnProvider
from DataTableServer.src.DataTableServer import DataTableServer
from DataTableServer.src.DataTableServerImplSA import DBStatementFormatter


DB_ENGINE = sqlalchemy.create_engine("sqlite:///:memory:")


class TestDataTableServer(unittest.TestCase):
    def setUp(self) -> None:
        self.engine = DB_ENGINE  # in-memory db
        self.db = DBConnProvider(self.engine)
        with self.engine.begin() as tx:
            tx.execute(sqlalchemy.text("DROP TABLE IF EXISTS test;"))
            tx.execute(
                sqlalchemy.text(
                    """
                CREATE TABLE test 
                (
                  id INTEGER NOT NULL,
                  name TEXT NOT NULL,
                  PRIMARY KEY (id)  
                );       
                """
                )
            )
        self.dt_params = {
            "draw": 0,
            "columns": [
                {
                    "data": "id",
                    "name": "id",
                    "searchable": True,
                    "orderable": True,
                    "search": {"value": "", "regex": False},
                },
                {
                    "data": "name",
                    "name": "name",
                    "searchable": True,
                    "orderable": True,
                    "search": {"value": "", "regex": False},
                },
            ],
        }
        self.column_mappings = {"id": 0, "name": 1}
        self.table_name = "test"
        self.table_index = "id"
        self.datatable_server = DataTableServer(
            database=self.db,
            stmt_formatter=DBStatementFormatter(),
            table=self.table_name,
            index_column=self.table_index,
            columns=self.column_mappings,
        )
        return super().setUp()

    def populate_db(self, records: List[Dict]) -> None:
        with self.engine.begin() as tx:
            tx.execute(
                sqlalchemy.text("INSERT INTO test (id, name) VALUES (:id, :name)"),
                records,
            )

    def test_has_datatable_fields(self):
        """
        Ensure the result object has the expected shape.
        """
        self.populate_db(
            [
                {"id": 0, "name": "Zorro"},
                {"id": 1, "name": "Alberto"},
            ],
        )
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=""
        )
        self.assertIn("draw", result)
        self.assertIn("recordsTotal", result)
        self.assertIn("recordsFiltered", result)
        self.assertIn("data", result)

    def test_has_datatable_rows(self):
        """
        Ensure the result rows have the expected values. Also ensure they are sorted by the database index column.
        """
        db_records = [
            {"id": 0, "name": "Zorro"},
            {"id": 1, "name": "Alberto"},
        ]
        self.populate_db(db_records)
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=""
        )
        rows = result["data"]
        n_expected_results = len(db_records)
        self.assertEqual(n_expected_results, len(rows))
        self.assertEqual("Zorro", rows[0]["name"])
        self.assertEqual("Alberto", rows[1]["name"])

    @parameterized.expand(
        [
            ("asc", 0, "Zorro"),
            ("asc", 1, "Alberto"),
            ("desc", 0, "Alberto"),
            ("desc", 1, "Zorro"),
        ]
    )
    def test_order(self, sort_direction: str, index_column: int, expected_first: str):
        """
        Rows are ordered ascending/descending according to the specified column index.
        """
        db_records = [
            {"id": 0, "name": "Zorro"},
            {"id": 1, "name": "Alberto"},
        ]
        self.populate_db(db_records)
        self.dt_params["order"] = [{"column": index_column, "dir": sort_direction}]
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=""
        )
        rows = result["data"]
        n_expected_results = len(db_records)
        self.assertEqual(n_expected_results, len(rows))
        self.assertEqual(expected_first, rows[0]["name"])

    @parameterized.expand(
        [
            ("ert", 1, ["Alberto"]),  # search on 1 column
            ("hdskjhdskjfka", 0, []),  # no matching records
            ("1", 2, ["Alberto", "Includes1"]),  # search on many columns
        ]
    )
    def test_search_global(
        self, search_filter: str, n_expected_results: int, expected_names: List[str]
    ):
        """
        Ensure the search filter actually filters out some rows.
        """
        db_records = [
            {"id": 0, "name": "Zorro"},
            {"id": 1, "name": "Alberto"},
            {"id": 2, "name": "Includes1"},
        ]
        self.populate_db(db_records)
        self.dt_params["search"] = {"value": search_filter, "regex": False}
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=""
        )
        n_total_records = len(db_records)
        self.assertEqual(n_total_records, result["recordsTotal"])
        self.assertEqual(n_expected_results, result["recordsFiltered"])
        rows = result["data"]
        self.assertEqual(n_expected_results, len(rows))
        for row, expected_name in zip(rows, expected_names):
            self.assertEqual(expected_name, row["name"])

    @parameterized.expand(
        [
            (0, 1, 1, "Zorro"),  # only the first record
            (2, 1, 1, "John"),  # 1 record starting from an offset
            (1, 2, 2, "Alberto"),  # some records starting from an offset
            (0, 2, 2, "Zorro"),  # some records starting from the first one
            (1000, 1, 0, None),  # offset greater than the available records quantity
        ]
    )
    def test_paging(
        self,
        page_start: int,
        page_length: int,
        n_expected_rows: int,
        expected_first: str,
    ):
        """Ensure that pagination returns a subset of the available total records."""
        db_records = [
            {"id": 0, "name": "Zorro"},
            {"id": 1, "name": "Alberto"},
            {"id": 2, "name": "John"},
        ]
        self.populate_db(db_records)

        self.dt_params["start"] = page_start
        self.dt_params["length"] = page_length
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=""
        )
        self.assertEqual(len(db_records), result["recordsTotal"])
        self.assertEqual(len(db_records), result["recordsFiltered"])
        rows = result["data"]
        self.assertEqual(n_expected_rows, len(rows))
        if n_expected_rows > 0:
            self.assertEqual(expected_first, rows[0]["name"])

    def test_where_all(self):
        """Where All condition affects the recordsTotal property."""
        db_records = [
            {"id": 0, "name": "Zorro"},
            {"id": 1, "name": "Alberto"},
            {"id": 2, "name": "John"},
        ]
        self.populate_db(db_records)

        where_all = "id = 2"
        result = self.datatable_server.get_rows(
            params=self.dt_params, where_result="", where_all=where_all
        )
        self.assertEqual(1, result["recordsTotal"])
        self.assertEqual(1, result["recordsFiltered"])
        rows = result["data"]
        self.assertEqual("John", rows[0]["name"])
