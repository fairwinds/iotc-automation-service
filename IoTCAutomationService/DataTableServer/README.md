# DataTable.net Server Side Processing in Python

## Compatibility
This module has been tested with Python 3.8 and Python 3.9

## Description
Python code that can be used to feed a DataTable with server-side processed data coming from a relational DBMS.

For more information take a look at: https://datatables.net/manual/server-side

## Usage
The typical use case is to feed a `DataTableServer` object with an HTTP request coming from the DataTable library. Below you can find additional notes.

1. Implement the interfaces defined in `interface.py` in a separate file.

    **NOTE**: if you use SQLAlchemy there is a predefined implementation inside `DataTableServerMediatorSA.py`

2. Take a look at the tests inside the `tests` folder for example usage.

## Running tests
1. `pip install -r DataTableServer/tests/requirements.txt`
2. `python -m unittest discover -s DataTableServer/tests`