from gevent import monkey

monkey.patch_all()  # needed in order to serve bottle through gevent
import math
from pathlib import Path
import time
from typing import Any, Dict, List, Tuple
import json
from bottle import request, response, get, post, put, delete, default_app, HTTPResponse
import urllib3
import http

from IoTCAutomationService.common.api_client import IoTCAPIClientManager
from IoTCAutomationService.campaign.target import IoTCTargetCalculator
from IoTCAutomationService.campaign.task import IoTCTaskFactory
from IoTCAutomationService.common.types import IoTCTargetArchitecture
from IoTCAutomationService.IoTCMqttClient import IoTCMqttClientManager
from IoTCAutomationService.campaign.campaign import IoTCCampaignDispatcher
from IoTCAutomationService.campaign.controller import IoTCCampaignController
from IoTCAutomationService.campaign.stats import IoTCCampaignStats
from IoTCAutomationService.common.common import (
    BottleManager,
    check_service_status,
    configure_logging,
    notify_campaign_removed,
    update_failed_attempts,
)
from IoTCAutomationService.common.enums import (
    IoTCActionOnCampaign,
    IoTCAutomationServiceStatus,
    IoTCCampaignSchedule,
    IoTCCampaignStatus,
    IoTCDataType,
    IoTCStudioStatus,
    LogLevel,
)
from IoTCAutomationService.common.errors import (
    APIClientError,
    CampaignNotFoundError,
    CountOnTableError,
    EmptyCommandsCampaignError,
    EmptyPageError,
    EmptyTargetsError,
    InvalidCampaignOptionError,
    InvalidCampaignScheduleError,
    InvalidCampaignTimeError,
    InvalidCommandTypeError,
    MissingCertificatesError,
    MissingSettingsError,
    MissingTargetsError,
    MultipleCommandTypesError,
    ScheduleNotFoundError,
    TooLowIntervalError,
    UnprocessableCampaignCommandError,
    UnrecognizedEntityTypeError,
    UnrecognizedTopicVariableError,
    WrongIntervalError,
    WrongRequestError,
)
from IoTCAutomationService.decorators import (
    bind_query_params,
    bind_request_body,
    eval_force_refresh,
    inject_json_body,
    needs_json_body,
    requires_secret,
    requires_token_verification,
    validate_json_body,
)
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.campaign.common import configure_scheduler
from IoTCAutomationService.persistence.command_library import IoTCCommandRepository
from IoTCAutomationService.persistence.common import (
    CommandDefinitionError,
    DuplicateEntityError,
    EntityIdNotFound,
)
from IoTCAutomationService.persistence.log import IoTCLogRepository, IoTCServiceLogger
from IoTCAutomationService.persistence.database import configure_database
from IoTCAutomationService.schemas import (
    LIBRARY_COMMAND_JSON_SCHEMA,
    CAMPAIGN_JSON_SCHEMA,
)
from IoTCAutomationService.common.cache import (
    IoTCHypervisorCache,
    configure_cache_refreshers,
)
from IoTCAutomationService.automation_service import IoTCAutomationService
from IoTCAutomationService.IoTCMessageFingerprint import IoTCMessageFingerprint


def _persist_shell_commands(
    campaign_def: Dict,
    targets: List[IoTCTargetArchitecture],
    persistence_command: IoTCCommandRepository,
) -> None:
    """
    Ensure that, if this function completes till the end, all the shell commands in the campaign definition are saved into the Automation Service command library.
    """

    def get_campaign_architectures() -> List[str]:
        """Infer the campaign list of architectures from the arch. of its targets"""
        architectures = list(set([target.architecture for target in targets]))
        return architectures

    campaign_architectures = get_campaign_architectures()
    commands_to_add: List[Dict] = []
    commands_to_update: List[Tuple[int, Dict]] = []
    for index_task, task in enumerate(campaign_def["tasks"]):
        shell_commands = [
            command
            for command in task["commands"]
            if command["type"] in ["METRIC", "SHELL"]
        ]
        for command in shell_commands:

            def sanitize_command(command: Dict) -> Dict:
                """Add missing properties to command if the front-end did not specify them."""
                if "interpreter" not in "command":
                    command.update({"interpreter": "BASH"})
                return command

            command = sanitize_command(command)
            try:
                command_in_library = persistence_command.get_by_name(command["name"])
                # If we're here, the command is already in the command library

                # Ensure the provided command matches the definition inside the library
                log_prefix = f"While adding campaign ['{campaign_def['name']}'], task #{index_task} ['{task['name']}'], command ['{command['name']}']: The provided command does not match the one inside the Automation Service Library. The provided command will replace the one in the library"
                needs_update = False
                if command_in_library["command"] != command["command"]:
                    IoTCServiceLogger.get().warning(
                        f"{log_prefix}. Reason: code listings (ie. the actual commands) are not equal. Received listing: '{command['command']}'. Listing in library: '{command_in_library['command']}'"
                    )
                    needs_update = True
                if command_in_library["type"] != command["type"]:
                    IoTCServiceLogger.get().warning(
                        f"{log_prefix}. Command type should be '{command_in_library['type']}', not '{command['type']}'"
                    )
                    needs_update = True
                if command_in_library["interpreter"] != command["interpreter"]:
                    IoTCServiceLogger.get().warning(
                        f"{log_prefix}. Interpreter should be '{command_in_library['interpreter']}', not '{command['interpreter']}'"
                    )
                    needs_update = True

                # Ensure the relationship between command and campaign architectures exist
                supported_architectures = list(command_in_library["architectures"])
                for campaign_arch in campaign_architectures:
                    if campaign_arch not in supported_architectures:
                        IoTCServiceLogger.get().warning(
                            f"{log_prefix}. Architecture '{campaign_arch}' is not supported by this command"
                        )
                        needs_update = True

                if needs_update:
                    # If the provided command doesn't match with the one in the library, update the command in the library.
                    # Also keep track of the previous supported architectures.
                    command_architectures = list(
                        set(campaign_architectures + supported_architectures)
                    )  # old command architectures + the architectures of this campaign
                    command.update({"architectures": command_architectures})
                    commands_to_update.append((command_in_library["id"], command))
            except EntityIdNotFound:
                # If the command is not in the command library, add it to the library (along with the campaign architecture)
                command.update(
                    {
                        "architectures": campaign_architectures,
                    }
                )
                commands_to_add.append(command)

    persistence_command.modify(
        add=commands_to_add, update=commands_to_update, remove=[]
    )


def main():
    configure_logging()
    check_service_status()
    db_connection_provider = configure_database()
    log_repository = IoTCLogRepository(db_connection_provider)
    IoTCServiceLogger.create(log_repository)
    IoTCServiceLogger.get().info(
        "************** IoT Catalyst Automation Service start **************"
    )
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    aservice = IoTCAutomationService()
    configure_cache_refreshers()
    hypervisor_cache = IoTCHypervisorCache()
    scheduler = configure_scheduler()
    campaign_persistence = IoTCCampaignRepository(db_connection_provider)
    campaign_controller = IoTCCampaignController(campaign_persistence, scheduler)
    campaign_stats = IoTCCampaignStats(campaign_persistence)
    task_factory = IoTCTaskFactory(
        get_api_client=lambda: IoTCAPIClientManager.get(),
        get_mqtt_client=lambda: IoTCMqttClientManager.get(),
        get_persistence=lambda: campaign_persistence,
    )
    campaign_dispatcher = IoTCCampaignDispatcher(
        scheduler, campaign_persistence, hypervisor_cache, task_factory
    )

    app = default_app()

    def UNEXPECTED_EXCEPTION_LOG_MESSAGE(request: Any, error: Any) -> str:
        method = getattr(request, "method", "[UNKNOWN HTTP METHOD]")
        path = getattr(request, "path", "[UNKNOWN ENDPOINT]")
        query_string = getattr(request, "query_string", "")
        return f"Unexpected exception in endpoint {method} '{path}{f'?{query_string}' if len(query_string) > 0 else ''}': {error}"

    @post("/automation-service/init")
    @requires_secret
    def init():
        try:
            payload = request.body.read()

            try:
                payload_dict = json.loads(payload)
            except json.decoder.JSONDecodeError:
                raise WrongRequestError("Request body is not a valid json")

            try:
                studio_url = payload_dict["studio_url"]
            except KeyError as err:
                raise WrongRequestError("Missing key 'studio_url' in request body")

            if "passport" in payload_dict:
                aservice.configure_from_outside_request(
                    {"passport": payload_dict["passport"], "studio_url": studio_url}
                )
            elif "userid" in payload_dict and "password" in payload_dict:
                aservice.configure_from_outside_request(
                    {
                        "userid": payload_dict["userid"],
                        "password": payload_dict["password"],
                        "studio_url": studio_url,
                    }
                )
            else:
                raise WrongRequestError("Missing Studio credentials")

            aservice.status = {
                "status": IoTCAutomationServiceStatus.OK.value,
                "info": "Configuration sent by api",
                "studio_status": {
                    "status": IoTCStudioStatus.OK.value,
                    "last_update": round(time.time()),
                    "info": {
                        "studio_url": studio_url,
                    },
                },
            }
            return HTTPResponse(
                json.dumps(
                    {
                        "data": aservice.status,
                        "message": "Automation Service init success",
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except WrongRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "%s. Please provide a valid 'studio_url' in the request body, together with valid 'userid' and 'password' or a valid 'passport'"
                        % err.args[0],
                    }
                ),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except (APIClientError, MissingCertificatesError) as err:
            aservice.status = {
                "status": IoTCAutomationServiceStatus.KO.value,
                "info": err.args[0],
                "studio_status": {
                    "status": IoTCStudioStatus.KO.value,
                    "last_update": round(time.time()),
                },
            }
            IoTCServiceLogger.get().error(err.args[0])
            update_failed_attempts("credentials")
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Automation Service init fail. Error: %s"
                        % err.args[0],
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )
        except MissingSettingsError as err:
            aservice.status = {
                "status": IoTCAutomationServiceStatus.KO.value,
                "info": err.args[0],
                "studio_status": {
                    "status": IoTCStudioStatus.KO.value,
                    "last_update": round(time.time()),
                },
            }
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Automation Service init fail. Error: %s"
                        % err.args[0],
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )
        except Exception as err:
            aservice.status = {
                "status": IoTCAutomationServiceStatus.KO.value,
                "info": "Unexpected error while initializing Automation Service",
                "studio_status": {
                    "status": IoTCStudioStatus.KO.value,
                    "last_update": round(time.time()),
                },
            }
            IoTCServiceLogger.get().error(str(err))
            update_failed_attempts("credentials")
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Automation Service init fail. Error: %s"
                        % err.args[0],
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/automation-service/status")
    def get_status():
        return HTTPResponse(
            json.dumps({"data": aservice.status}), status=http.HTTPStatus.OK.value
        )

    @get("/endpoints-collection")
    @requires_token_verification
    def get_endpoints_collection():
        file_path = Path.cwd() / "docs" / "endpoints-collection.json"
        if file_path.is_file():
            with file_path.open("r") as f:
                endpoints_json = json.load(f)

            response.add_header("Content-Type", "application/json")
            response.add_header(
                "Content-Disposition", "attachment; filename=endpoints-collection.json"
            )
            return endpoints_json
        else:
            IoTCServiceLogger.get().error(
                f"Unable to locate json endpoints collection in path '{file_path}'"
            )
            return HTTPResponse(
                {
                    "data": None,
                    "message": "Unable to retrieve the endpoints collection",
                },
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/help")
    @requires_token_verification
    def get_endpoints_guide():
        file_path = Path.cwd() / "docs" / "help.pdf"
        if file_path.is_file():
            with file_path.open("rb") as f:
                pdf = f.read()

            response.add_header("Content-Type", "application/pdf")
            response.add_header("Content-Disposition", "attachment; filename=help.pdf")

            return pdf
        else:
            IoTCServiceLogger.get().error(
                f"Unable to locate endpoints guide in path '{file_path}'"
            )
            return HTTPResponse(
                {
                    "data": None,
                    "message": "Unable to retrieve the endpoints guide",
                },
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post("/command")
    @requires_token_verification
    @eval_force_refresh
    @needs_json_body
    @validate_json_body(schema=CAMPAIGN_JSON_SCHEMA)
    @inject_json_body
    def create_campaign(campaign_def: Dict):
        try:
            hypervisor_cache = IoTCHypervisorCache()
            api_client = IoTCAPIClientManager.get()
            target_calculator = IoTCTargetCalculator(hypervisor_cache, api_client)
            targets = target_calculator.extract_campaign_targets(campaign_def)

            _persist_shell_commands(
                campaign_def, targets, IoTCCommandRepository(db_connection_provider)
            )

            id_campaign, message = campaign_dispatcher.create_campaign(
                campaign_def, targets
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {"id_campaign": id_campaign},
                        "message": message,
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.CREATED.value,
            )
        except (
            UnrecognizedTopicVariableError,
            InvalidCampaignScheduleError,
            InvalidCampaignTimeError,
            InvalidCampaignOptionError,
            EmptyTargetsError,
            UnrecognizedEntityTypeError,
            EmptyCommandsCampaignError,
            MultipleCommandTypesError,
            InvalidCommandTypeError,
            MissingTargetsError,
            WrongIntervalError,
            TooLowIntervalError,
            WrongRequestError,
            CommandDefinitionError,
        ) as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except Exception as err:
            campaign_name = (
                campaign_def["name"] if "name" in campaign_def else "[UNKNOWN]"
            )
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot create campaign '{campaign_name}'. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post("/command/verify")
    @requires_token_verification
    @needs_json_body
    @validate_json_body(schema=CAMPAIGN_JSON_SCHEMA)
    @inject_json_body
    def verify_campaign_definition(campaign_def: Dict):
        """
        Validate a campaign definition JSON. If the validation performed by the decorator succeeds,
        return an HTTP OK status code.
        """
        return HTTPResponse(
            {
                "data": campaign_def,
                "message": "Campaign definition is valid",
                "info": {},
            },
            status=http.HTTPStatus.OK.value,
        )

    @get("/campaigns")
    @bind_query_params(
        [
            {
                "name": "only_scheduled",
                "required": False,
                "dtype": IoTCDataType.BOOL,
                "default": False,
            },
            {
                "name": "status",
                "required": False,
                "dtype": IoTCDataType.ARRAY,
                "allowed_values": [status.value for status in IoTCCampaignStatus],
                "default": [],
            },
            {
                "name": "page",
                "required": False,
                "dtype": IoTCDataType.INT,
                "interval": [1, float("inf")],
                "default": 1,
            },
            {
                "name": "results_per_page",
                "required": False,
                "dtype": IoTCDataType.INT,
                "interval": [1, 10000],
                "default": 500,
            },
        ]
    )
    @requires_token_verification
    def get_all_campaigns(
        only_scheduled: bool, status: List, page: int, results_per_page: int
    ):
        try:
            statuses = [IoTCCampaignStatus(s) for s in status]
            if len(statuses) == 0:
                statuses = [s for s in IoTCCampaignStatus]
            (
                campaigns,
                page,
                results_in_page,
                total_pages,
                total_elements,
            ) = campaign_stats.get_all(
                only_scheduled,
                statuses,
                page,
                results_per_page,
            )
            info = {
                "page": page,
                "results_in_page": results_in_page,
                "total_pages": total_pages,
                "total_elements": total_elements,
            }
            message = ""
            if len(campaigns) == 0:
                if page > 1:
                    raise EmptyPageError("Page %s not found" % page)
                else:
                    campaigns = []
                    message = "No data found"
                    info = {
                        "page": 0,
                        "results_in_page": 0,
                        "total_pages": 0,
                        "total_elements": 0,
                    }

            return HTTPResponse(
                json.dumps(
                    {"data": {"campaigns": campaigns}, "message": message, "info": info}
                ),
                status=http.HTTPStatus.OK.value,
            )
        except EmptyPageError as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": info}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot retrieve campaigns information. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/campaigns/<id_campaign:int>")
    @bind_query_params(
        [
            {
                "name": "schedule",
                "required": True,
                "dtype": IoTCDataType.STRING,
                "regex": "^(all|[1-9]\d*)$",
                "default": "all",
            },
            {
                "name": "page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, float("inf")],
                "default": 1,
            },
            {
                "name": "results_per_page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, 10000],
                "default": 500,
            },
        ]
    )
    @requires_token_verification
    def get_campaign(schedule, page: int, results_per_page: int, id_campaign: int):
        try:
            if str(schedule) == "all":
                (
                    stats,
                    page,
                    results_in_page,
                    total_pages,
                    total_elements,
                ) = campaign_stats.get_all_schedules(
                    id_campaign, page, results_per_page
                )
            else:
                (
                    stats,
                    page,
                    results_in_page,
                    total_pages,
                    total_elements,
                ) = campaign_stats.get_schedule(
                    id_campaign, int(schedule), page, results_per_page
                )

            info = {
                "page": page,
                "results_in_page": results_in_page,
                "total_pages": total_pages,
                "total_elements": total_elements,
            }
            message = ""
            if results_in_page == 0:
                if page > 1:
                    raise EmptyPageError("Page %s not found" % page)
                else:
                    stats = None
                    message = "No data found"
                    info = {}
            return HTTPResponse(
                json.dumps({"data": stats, "message": message, "info": info}),
                status=http.HTTPStatus.OK.value,
            )
        except (CampaignNotFoundError, ScheduleNotFoundError) as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except EmptyPageError as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": info}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot retrieve information for campaign {id_campaign}. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post(
        "/campaigns/<id_campaign:int>/schedules/<id_schedule:int>/all-targets-datatable"
    )
    @requires_token_verification
    @needs_json_body
    @inject_json_body
    def get_schedule_detail_for_datatable(
        datatable_params: Dict, id_campaign: int, id_schedule: int
    ):
        """Server-Side DataTable processing to retrieve information of a campaign schedule."""
        try:
            from IoTCAutomationService.DataTableServer.src.DataTableServer import (
                DataTableServer as DTServer,
            )
            from IoTCAutomationService.DataTableServer.src.DataTableServerImplSA import (
                DBConnProvider as DTServerDB,
            )
            from IoTCAutomationService.DataTableServer.src.DataTableServerImplSA import (
                DBStatementFormatter as DTServerDBStatementFormatter,
            )

            datatable_db = DTServerDB(db_connection_provider)
            datatable_schedule_detail_table = "v_dt_schedule_target_detail"
            datatable_schedule_detail_index_column = "id_target"
            datatable_schedule_detail_column_mapping = {
                "id_target": 0,
                "target_name": 1,
                "target_progress": 2,
                "target_status": 3,
                "target_start_time": 4,
                "target_end_time": 5,
                "command_results": 6,
            }
            datatable_server = DTServer(
                database=datatable_db,
                stmt_formatter=DTServerDBStatementFormatter(),
                table=datatable_schedule_detail_table,
                index_column=datatable_schedule_detail_index_column,
                columns=datatable_schedule_detail_column_mapping,
            )
            datatable_result = datatable_server.get_rows(
                params=datatable_params,
                where_result="",
                where_all=f"id_campaign = {id_campaign} AND id_schedule = {id_schedule}",
            )
            # NOTE: the response body is different from the other endpoints
            # since this is not a "RESTful" endpoint.
            response_body = {
                **datatable_result,
                "data": {
                    "targets": datatable_result["data"],
                },
            }
            return HTTPResponse(
                json.dumps(response_body),
                status=http.HTTPStatus.OK.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot retrieve schedule status information. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/campaigns/<id_campaign>/schedules/<id_schedule>/targets/<id_target>")
    @requires_token_verification
    def get_target_execution_detail(id_campaign: int, id_schedule: int, id_target: int):
        """Return schedule details for a single target."""
        try:
            target_details = campaign_persistence.get_target_details(
                id_campaign, id_schedule, id_target
            )
            tasks = campaign_stats.get_target_tasks(id_campaign, id_schedule, id_target)
            target_details.update({"tasks": tasks})
            return HTTPResponse(
                json.dumps({"data": target_details, "message": "", "info": {}}),
                status=http.HTTPStatus.OK.value,
            )
        except EntityIdNotFound:
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"No data found for campaign {id_campaign}, schedule {id_schedule}, target {id_target}",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot retrieve campaign status information. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @delete("/campaigns/<id_campaign:int>")
    @requires_token_verification
    def remove_campaign(id_campaign: int):
        try:
            try:
                campaign = campaign_persistence.get_campaign(id_campaign)
            except EntityIdNotFound:
                raise CampaignNotFoundError(f"Campaign {id_campaign} not found")
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"Failed to retrieve campaign {id_campaign} before deletion. Error: {err}"
                )
                raise Exception(
                    f"Cannot remove campaign {id_campaign}. See logs for details"
                )

            scheduled = IoTCCampaignSchedule(campaign["scheduled"])
            if scheduled == IoTCCampaignSchedule.SCHEDULED:
                raise WrongRequestError(
                    "Cannot remove campaign %s because it is scheduled. You must remove the schedule first"
                    % id_campaign
                )

            if campaign.get("last_schedule") is not None:
                id_last_schedule = int(campaign["last_schedule"])
                last_schedule = campaign_persistence.get_schedule_details(
                    id_campaign, id_last_schedule
                )
                status = IoTCCampaignStatus(last_schedule["status"])
                if status in [
                    IoTCCampaignStatus.NEVER_RUN,
                    IoTCCampaignStatus.PAUSED,
                    IoTCCampaignStatus.RUNNING,
                ]:
                    raise WrongRequestError(
                        f"Cannot remove campaign {id_campaign} because its status is '{status.value}'. Only exited campaigns can be removed"
                    )

            targets = list(campaign["targets"])
            notify_campaign_removed(
                IoTCServiceLogger.get(),
                IoTCMqttClientManager.get(),
                id_campaign,
                targets,
            )
            n_deleted_entities = campaign_persistence.remove_campaign(id_campaign)
            IoTCServiceLogger.get().info(f"Campaign {id_campaign} has been deleted")
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {
                            "n_deleted_entities": n_deleted_entities,
                        },
                        "message": f"Campaign {id_campaign} deleted",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except CampaignNotFoundError as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except WrongRequestError as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot remove campaign {id_campaign}. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/campaigns/<id_campaign:int>/definition")
    @requires_token_verification
    def get_campaign_definition(id_campaign):
        try:
            campaign = campaign_stats.get_campaign_definition(id_campaign)
            return HTTPResponse(
                json.dumps({"data": campaign, "message": "", "info": {}}),
                status=http.HTTPStatus.OK.value,
            )
        except CampaignNotFoundError as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot retrieve definition for campaign {id_campaign}. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @delete("/scheduled-campaigns/<id_campaign:int>")
    @requires_token_verification
    def remove_schedule_from_campaign(id_campaign):
        try:
            message = campaign_controller.remove_scheduled(id_campaign)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.OK.value,
            )
        except (CampaignNotFoundError, UnprocessableCampaignCommandError) as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.UNPROCESSABLE_ENTITY.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot remove schedule from campaign {id_campaign}. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post("/campaigns/<id_campaign:int>/<command:re:abort|pause|resume>")
    @requires_token_verification
    def evaluate_campaign_action(id_campaign, command):
        try:
            message = ""
            if command == IoTCActionOnCampaign.ABORT.value:
                message = campaign_controller.abort(id_campaign)
            elif command == IoTCActionOnCampaign.PAUSE.value:
                message = campaign_controller.pause(id_campaign)
            elif command == IoTCActionOnCampaign.RESUME.value:
                message = campaign_controller.resume(id_campaign)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.ACCEPTED.value,
            )
        except (CampaignNotFoundError, UnprocessableCampaignCommandError) as err:
            message = err.args[0]
            IoTCServiceLogger.get().error("ERROR: %s" % message)
            return HTTPResponse(
                json.dumps({"data": None, "message": message, "info": {}}),
                status=http.HTTPStatus.UNPROCESSABLE_ENTITY.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Cannot {command} campaign {id_campaign}. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post("/validate-task")
    @requires_token_verification
    @bind_request_body(
        [
            {
                "name": "hash",
                "required": True,
                "dtype": IoTCDataType.STRING,
            }
        ]
    )
    def validate_message_hash(hash: str):
        try:
            message_fingerprint = IoTCMessageFingerprint(
                IoTCServiceLogger.get(), fingerprint=hash
            )
            message_fingerprint.find()
            message_fingerprint.remove()
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {"hash": hash},
                        "message": "Message hash is valid",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except KeyError:
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {"hash": hash},
                        "message": "Message hash is not valid",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Error while validating message hash '{hash}'. Error: {err}"
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {"hash": hash},
                        "message": "Could not validate the required message hash. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/command-library/commands")
    @bind_query_params(
        [
            {
                "name": "page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, float("inf")],
                "default": 1,
            },
            {
                "name": "results_per_page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, 10000],
                "default": 500,
            },
            {
                "name": "types",
                "required": False,
                "dtype": IoTCDataType.ARRAY,
                "allowed_values": [
                    "SHELL",
                    "METRIC",
                ],
                "default": [],
            },
        ]
    )
    @requires_token_verification
    def get_shell_commands(page: int, results_per_page: int, types: List[str]):
        persistence_command = IoTCCommandRepository(db_connection_provider)

        try:
            total_elements, commands = persistence_command.get_all(
                types=types,
                interpreters=[],
                architectures=[],
                page=page,
                results_per_page=results_per_page,
            )
            results_in_page = len(commands)
            total_pages = math.ceil(total_elements / int(results_per_page))
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {
                            "commands": commands,
                        },
                        "message": "",
                        "info": {
                            "page": page,
                            "results_in_page": results_in_page,
                            "total_pages": total_pages,
                            "total_elements": total_elements,
                        },
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Cannot retrieve shell commands from the Automation Service library. Error: {err}"
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot retrieve shell commands from the Automation Service library. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @post("/command-library/commands")
    @needs_json_body
    @validate_json_body(schema=LIBRARY_COMMAND_JSON_SCHEMA)
    @inject_json_body
    @requires_token_verification
    def create_shell_command(command_def: Dict):
        persistence_command = IoTCCommandRepository(db_connection_provider)
        try:
            ids_commands = persistence_command.add(command_def)
            id_command = ids_commands[0]
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {
                            "id_command": id_command,
                        },
                        "message": f"Command {command_def['name']} created successfully",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.CREATED.value,
            )
        except DuplicateEntityError as err:
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": err.args[0],
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(
                    request,
                    f"Cannot add shell command to the Automation Service library. Error: {err}. Command is: {command_def}",
                )
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot add shell command to the Automation Service library. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @put("/command-library/commands/<id_command:int>")
    @needs_json_body
    @validate_json_body(schema=LIBRARY_COMMAND_JSON_SCHEMA)
    @inject_json_body
    @requires_token_verification
    def update_shell_command(command_def: Dict, id_command: int):
        persistence_command = IoTCCommandRepository(db_connection_provider)
        try:
            ids = persistence_command.update((id_command, command_def))
            id_updated_command = ids[0]
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {
                            "id_command": id_updated_command,
                        },
                        "message": f"Command updated successfully",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except EntityIdNotFound:
            IoTCServiceLogger.get().warning(
                f"Cannot update shell command {id_command} because it does not exist in the command library. User provided this command: {command_def}"
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Command {id_command} not found",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Unexpected error while updating command {id_command}. Error: {err}. User provided this command: {command_def}"
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot update the requested shell command. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @delete("/command-library/commands/<id_command:int>")
    @requires_token_verification
    def remove_shell_command(id_command: int):
        persistence_command = IoTCCommandRepository(db_connection_provider)
        try:
            persistence_command.remove(id_command)
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {
                            "id_command": id_command,
                        },
                        "message": f"Command {id_command} deleted successfully",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except EntityIdNotFound:
            IoTCServiceLogger.get().warning(
                f"Cannot remove shell command {id_command} because it does not exist in the command library."
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": f"Command {id_command} not found",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Unexpected error while removing command {id_command}. Error: {err}"
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "Cannot remove the requested shell command. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    @get("/logs")
    @requires_token_verification
    @bind_query_params(
        [
            {
                "name": "page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, float("inf")],
                "default": 1,
            },
            {
                "name": "results_per_page",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, 10000],
                "default": 500,
            },
            {
                "name": "level",
                "required": False,
                "dtype": IoTCDataType.STRING,
                "allowed_values": [
                    LogLevel.DEBUG.value,
                    LogLevel.INFO.value,
                    LogLevel.WARNING.value,
                    LogLevel.ERROR.value,
                    LogLevel.ALL.value,
                ],
                "default": LogLevel.ALL.value,
            },
        ]
    )
    def get_all_service_logs(page: int, results_per_page: int, level: str):
        info = {}
        try:
            log_level = LogLevel(level)
            total_elements, logs = log_repository.get_all(
                log_level, page, results_per_page
            )
            results_in_page = len(logs)
            total_pages = math.ceil(total_elements / int(results_per_page))
            info = {
                "page": page,
                "results_in_page": results_in_page,
                "total_pages": total_pages,
                "total_elements": total_elements,
            }
            message = ""
            if len(logs) == 0:
                if page > 1:
                    raise EmptyPageError("Page %s not found" % page)
                else:
                    logs = None
                    message = "No data found"

            return HTTPResponse(
                json.dumps({"data": logs, "message": message, "info": info}),
                status=http.HTTPStatus.OK.value,
            )
        except CountOnTableError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": "%s" % err.args[0], "info": info}),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )
        except EmptyPageError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": "%s" % err.args[0], "info": info}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )

    @delete("/logs")
    @requires_token_verification
    @bind_query_params(
        [
            {
                "name": "date",
                "required": True,
                "dtype": IoTCDataType.INT,
                "interval": [1, float("inf")],
            }
        ]
    )
    def remove_service_logs(date: int):
        try:
            n_deleted_logs = log_repository.remove(date)
            return HTTPResponse(
                json.dumps(
                    {
                        "data": {"n_deleted_logs": n_deleted_logs},
                        "message": f"Deleted {n_deleted_logs} logs",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.OK.value,
            )
        except Exception as err:
            message = err.args[0]
            IoTCServiceLogger.get().error(
                UNEXPECTED_EXCEPTION_LOG_MESSAGE(request, message)
            )
            return HTTPResponse(
                json.dumps(
                    {
                        "data": None,
                        "message": "An error occurred while removing logs. See logs for details",
                        "info": {},
                    }
                ),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    BottleManager.start(app)
