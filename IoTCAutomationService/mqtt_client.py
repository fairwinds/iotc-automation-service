import sys
import threading
import ssl
import logging
import time
from typing import Callable, Tuple, Union
import paho.mqtt.client as mqtt
from paho.mqtt.client import MQTT_ERR_NO_CONN


class IoTCMqttClientGeneric(threading.Thread):
    """IoTCMqttClientGeneric class
    generic MQTT endpoint
    """

    def __init__(
        self,
        logger: Union[logging.Logger, None],
        transport,
        client_id,
        username,
        get_password: Union[Callable[[], str], None] = None,
        cacert=None,
        cert_file=None,
        key_file=None,
        clean_session=False,
    ):
        super().__init__()
        self.name = f"Mqtt Client [{client_id}]"
        self._logger = logger
        self.setDaemon(True)
        self.connected = False
        self.transport = transport
        self.client_id = client_id
        self.username = username
        self.password = None
        if get_password is not None:
            self._get_password = get_password
            self.password = get_password()
        self.clean_session = clean_session
        self.mqtt_client = mqtt.Client(
            transport=self.transport,
            client_id=self.client_id,
            clean_session=self.clean_session,
        )
        self.mqtt_client.message_retry_set(60)
        self.cacert = cacert
        self.cert_file = cert_file
        self.key_file = key_file
        self._lock = None
        if self.cacert is not None:
            try:
                self._lock = (
                    threading.Lock()
                )  # Use a lock to prevent issue with non-threadsafe TLS library
                with self._lock:
                    self.mqtt_client.tls_set(
                        ca_certs=self.cacert,
                        certfile=self.cert_file,
                        keyfile=self.key_file,
                        cert_reqs=ssl.CERT_REQUIRED,
                    )
            except Exception as err:
                self._log("error", "ERROR BAD Certificates [%s]" % str(err))
                sys.exit(-1)
        if self.password is not None:
            self.mqtt_client.username_pw_set(self.username, self.password)
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_subscribe = self.on_subscribe
        self.mqtt_client.on_publish = self.on_publish
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.mqtt_client.on_message = self.on_message

    def connect(self, host, port=8883):
        """connect method"""
        self._log("debug", "CLIENT_ID      [%s]" % self.client_id)
        if self.cacert is not None:
            self._log("debug", "Client [%s] TLS Secure Connecting" % self.client_id)
        if self.password is not None:
            self._log(
                "debug", "Client [%s] Authenticating with password" % self.client_id
            )
        try:
            self.mqtt_client.connect(host, port, 60)
        except Exception as err:
            self._log(
                "info",
                "Client [%s] Error [%s] can't connect to the broker [%s:%d]"
                % (self.client_id, str(err), host, port),
            )

    def subscribe(self, subscribe_topics) -> Tuple[int, int]:
        """subscribe method"""
        result, mid = self.mqtt_client.subscribe(subscribe_topics)
        if mid is not None:
            self._log(
                "info",
                "Client [%s] Subscribe request result code [%s] on topics [%s] mid [%d]"
                % (self.client_id, str(result), str(subscribe_topics), mid),
            )
        else:
            self._log(
                "error",
                "Client [%s] Subscribe request result code [%s] on topics [%s]"
                % (self.client_id, str(result), str(subscribe_topics)),
            )
        return result, mid

    def unsubscribe(self, unsubscribe_topics):
        """subscribe method"""
        result, mid = self.mqtt_client.unsubscribe(unsubscribe_topics)
        self._log(
            "info",
            "Client [%s] Unsubscribe [%s] request result code [%s] mid [%d]"
            % (self.client_id, str(unsubscribe_topics), str(result), mid),
        )
        return result

    def stop(self):
        """stop method"""
        self.mqtt_client.disconnect()
        self.mqtt_client.loop_stop()

    def onstart(self):
        pass

    def run(self):
        """run method"""
        self.onstart()
        self.mqtt_client.loop_forever()

    def send(
        self,
        topic: str,
        message: bytes,
        qos: int = 0,
        retain: bool = False,
        retries: Union[int, None] = 10,
    ) -> Tuple[int, Union[int, None]]:
        """
        Publish method. This will not ever raise an exception.

        Args:
        - topic: MQTT topic.
        - message: MQTT message payload.
        - qos: MQTT Quality of Service.
        - retain: MQTT Retained flag.
        - retries: Number of retries if an error is encountered during the publish. If None, it retries indefinitely.

        Returns:
        - A tuple in which:
            - The first element is the result code.
            - The second element is the publish message id (if any).
        """
        UNKNOWN_ERROR = (-1, None)

        def _publish() -> Tuple[int, Union[int, None]]:
            # Fail fast if the client is not connected.
            if not self.connected:
                self._log(
                    "warning",
                    f"Client [{self.client_id}] publish FAILED on topic [{topic}] because not connected to the broker - payload [{str(message)}]",
                )
                return (MQTT_ERR_NO_CONN, None)

            # Trap paho-mqtt exceptions
            try:
                pub_result = self.mqtt_client.publish(topic, message, qos, retain)
            except Exception as exception:
                self._log(
                    "warning",
                    f"Client [{self.client_id}] publish FAILED on topic [{topic}] due to unexpected exception [{exception}] payload [{str(message)}]"
                    % (self.client_id, exception, str(message)),
                )
                return UNKNOWN_ERROR

            err = pub_result.rc
            mid = pub_result.mid
            if err == 0:
                self._log(
                    "info",
                    f"Client [{self.client_id}] mid [{mid}] message sent on topic [{topic}] err [{err}] payload [{str(message)}]",
                )
            else:
                self._log(
                    "warning",
                    f"Client [{self.client_id}] mid [{mid}] message NOT sent on topic [{topic}] err [{err}] payload [{str(message)}]",
                )
            return err, mid

        def try_publish(retry_times: Union[int, None], sleep_between_retries: int):
            # if a number of retries is specified: "while retry_times >= 0"
            # otherwise: "while True"
            while ((type(retry_times) == int) and retry_times >= 0) or (
                retry_times is None
            ):
                if type(retry_times) == int:
                    retry_times -= 1
                if self._lock is None:
                    err, mid = _publish()
                else:
                    with self._lock:
                        err, mid = _publish()
                if err == 0:
                    return err, mid
                else:
                    time.sleep(sleep_between_retries)
            return UNKNOWN_ERROR

        sleep_between_retries = 2  # in seconds
        err, mid = try_publish(retries, sleep_between_retries)
        return (err, mid)

    def on_connect(self, client, userdata, flags, rc):
        """on_connect callback method"""
        self._log(
            "info",
            "Client [%s] connected with result code: [%s]" % (self.client_id, str(rc)),
        )
        self.connected = True

    def on_subscribe(self, client, userdata, mid, granted_qos):
        """on_subscribe callback method"""
        self._log(
            "info",
            "Client [%s] subscribed mid [%d] granted_qos [%s]"
            % (self.client_id, mid, str(granted_qos)),
        )

    def on_unsubscribe(self, client, userdata, mid):
        """on_subscribe callback method"""
        self._log("info", "Client [%s] unsubscribed mid [%d]" % (self.client_id, mid))

    def on_publish(self, client, userdata, mid):
        """on_publish callback method"""
        self._log(
            "debug",
            "Client [%s] mid [%d] The message is correctly delivered to the broker"
            % (self.client_id, mid),
        )

    def on_disconnect(self, client, userdata, rc):
        """on_disconnect callback method"""
        self._log(
            "info",
            "Client [%s] disconnected with result code [%s]" % (self.client_id, rc),
        )
        self._log("info", "Trying to reconnect...")
        self.password = self._get_password()
        self.mqtt_client.username_pw_set(self.username, self.password)
        self.connected = False

    def on_message(self, client, userdata, msg):
        """on_message callback method"""
        self.receive(msg.topic, msg.payload)

    def receive(self, topic, msg):
        """OVERRIDE THIS METHOD
        receive method
        """
        pass

    def _log(self, level: str, msg: str) -> None:
        try:
            level = level.lower()
            if level not in [
                "info",
                "debug",
                "warn",
                "warning",
                "error",
                "critical",
                "exception",
            ]:
                raise Exception(level)
        except:
            level = "info"

        if self._logger is not None:
            getattr(self._logger, level)(msg)
        else:
            print(msg)
