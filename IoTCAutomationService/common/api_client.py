from enum import Enum
import http
import json
import re
import threading
from typing import Dict
from requests import ReadTimeout
import requests

from IoTCAutomationService.common.common import (
    MAX_API_CALL_ATTEMPTS,
    IoTCStudioCredentials,
)
from IoTCAutomationService.common.enums import IoTCEntityCode
from IoTCAutomationService.common.errors import (
    APIClientError,
    HTTPConnectTimeoutError,
    HTTPConnectionError,
    HTTPMalformedResponseError,
    HTTPResponseFailError,
    HTTPResponseTimeoutError,
    HTTPWrongResponseCodeError,
    InvalidTokenError,
    MissingCredentialsError,
    TooManyApiCallAttemptsError,
)
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class APIMethod(Enum):
    TOKEN_VERIFY = "APISecurity.verifyToken"


class IoTCAPIClient:
    """Class responsible to send requests to IoTCatalyst Studio API."""

    def __init__(self, studio_credentials: IoTCStudioCredentials):
        self._studio_credentials = studio_credentials
        self._token = ""
        self._refresh_token_lock = threading.Lock()
        self._response_timeout_seconds = 60

    def refresh_token(self):
        if not self._studio_credentials.has_valid_credentials():
            raise MissingCredentialsError("Credentials are missing")

        old_token: str = self._token  # used for logging
        try:
            if self._studio_credentials.passport is not None:
                self._token = self._make_api_request(
                    self._api_request_body(
                        method="APISecurity.requestTokenByPassport",
                        token="",
                        params={"passport": self._studio_credentials.passport},
                    ),
                    response_key="APIToken",
                )
            else:
                self._token = self._make_api_request(
                    self._api_request_body(
                        method="APISecurity.requestToken",
                        token="",
                        params={
                            "userid": self._studio_credentials.userid,
                            "password": self._studio_credentials.password,
                        },
                    ),
                    response_key="APIToken",
                )
            IoTCServiceLogger.get().info(
                f"IoT Catalyst Studio Token refreshed successfully. New token is: {self._token}. Old token was: {old_token}"
            )
        except Exception as err:
            IoTCServiceLogger.get().warning(
                f"Failed refreshing IoT Catalyst Studio token. Error: {err}"
            )

    def get_iotc_settings(self):
        return self._make_authorized_api_request(
            self._api_request_body(
                method="General.settings",
                token=self._token,
                params={},
            ),
            response_key="settings",
        )

    def get_mqtt_certificates(self):
        return self._make_authorized_api_request(
            self._api_request_body(
                method="MQTT.createMQTTCertificates",
                token=self._token,
                params={},
            ),
            response_key="certs",
        )

    def verify_token(self, token: str):
        """Given an API Token, returns True if it is a valid token. False otherwise."""
        try:
            self._make_api_request(
                self._api_request_body(
                    method=APIMethod.TOKEN_VERIFY.value,
                    token=token,
                    params={"token": token},
                )
            )

            return True
        except InvalidTokenError as err:
            return False
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Unexpected error in verify_token method: {err}"
            )
            return False

    def verify_and_refresh_token(self) -> None:
        """
        Ensure that the API Client token refresh is done in an atomic way.
        If multiple threads try to validate an invalid token at the same time,
        the token will be refreshed only once.
        """
        with self._refresh_token_lock:
            if not self.verify_token(self._token):
                self.refresh_token()

    def get_all_hypervisor_registries(self):
        hypervisors = self._get_all_registries(IoTCEntityCode.HYPERVISOR)
        return hypervisors

    def get_all_container_registries(self):
        containers = self._get_all_registries(IoTCEntityCode.CONTAINER)
        return containers

    def _get_all_registries(self, entity_code: IoTCEntityCode):
        if entity_code == IoTCEntityCode.HYPERVISOR:
            method = "Hypervisor.getAll"
            response_key = "hypervisors"
        elif entity_code == IoTCEntityCode.CONTAINER:
            method = "Container.getAll"
            response_key = "containers"
        registries = self._make_authorized_api_request(
            self._api_request_body(
                method=method,
                token=self._token,
                params={},
            ),
            response_key=response_key,
        )
        return registries

    def get_hypervisors_ids_by_tags(self, tags):
        ids = self._get_ids_by_tags(IoTCEntityCode.HYPERVISOR, tags)
        return ids

    def get_containers_ids_by_tags(self, tags):
        ids = self._get_ids_by_tags(IoTCEntityCode.CONTAINER, tags)
        return ids

    def _get_ids_by_tags(self, entity_code: IoTCEntityCode, tags):
        res = self._make_authorized_api_request(
            self._api_request_body(
                method="Tags.getAllByIdComponentTagsCloudforHM",
                token=self._token,
                params={
                    "idComponentType": entity_code.value,
                    "tagsCloud": tags,
                    "tagsBooleanLogic": "AND",
                },
            )
        )
        return list(res.keys())

    def _make_authorized_api_request(self, *args, **kwargs):
        self.verify_and_refresh_token()
        args[0]["auth"] = self._token
        try:
            for i in range(MAX_API_CALL_ATTEMPTS):
                return self._make_api_request(*args, **kwargs)

            if i >= MAX_API_CALL_ATTEMPTS - 1:
                raise TooManyApiCallAttemptsError(
                    "Max number of API calls has been reached for this request"
                )
        except (
            HTTPResponseFailError,
            HTTPWrongResponseCodeError,
            HTTPResponseTimeoutError,
            HTTPConnectTimeoutError,
            HTTPConnectionError,
            HTTPMalformedResponseError,
            TooManyApiCallAttemptsError,
        ) as err:
            raise APIClientError(err.args[0])

    def call_api_method_raw(self, method, params):
        """Method used by Automation Service campaigns to execute API commands."""
        self.verify_and_refresh_token()
        payload = self._api_request_body(
            method=method,
            token=self._token,
            params=params,
        )
        res = requests.post(
            self._studio_credentials.url,
            json=payload,
            verify=False,
            stream=True,
            timeout=self._response_timeout_seconds,
        )
        return res

    def _make_api_request(self, payload: Dict, response_key=None):
        log_message = f"Calling HTTP POST [{self._studio_credentials.url}] with payload [{payload}]"

        try:
            res = requests.post(
                self._studio_credentials.url,
                json=payload,
                verify=False,
                stream=True,
                timeout=self._response_timeout_seconds,
            )
        except ReadTimeout:
            error_message = "Request took too long to return"
            IoTCServiceLogger.get().error(
                f"{log_message} NETWORK_ERROR [{error_message}]"
            )
            raise HTTPResponseTimeoutError(error_message)
        except requests.ConnectTimeout:
            error_message = f"Connection timeout for url {self._studio_credentials.url}"
            IoTCServiceLogger.get().error(
                f"{log_message} NETWORK_ERROR [{error_message}]"
            )
            raise HTTPConnectTimeoutError(error_message)
        except requests.ConnectionError:
            error_message = f"Connection error with url: {self._studio_credentials.url}"
            IoTCServiceLogger.get().error(
                f"{log_message} NETWORK_ERROR [{error_message}]"
            )
            raise HTTPConnectionError(error_message)
        if res.status_code == http.HTTPStatus.OK.value:
            try:
                response_dict = json.loads(res.content.decode())
                if response_dict["success"] is True:
                    # Removed because it logged too much.
                    # IoTCServiceLogger.get().info(f"{log_message} SUCCESS [{res.status_code}]")
                    if (
                        response_key is not None
                        and response_key in response_dict["data"]
                    ):
                        return response_dict["data"][response_key]
                    elif (
                        isinstance(response_dict["data"], list)
                        and len(response_dict["data"]) == 1
                        and "result" in response_dict["data"][0]
                    ):  # function response
                        return response_dict["data"][0]["result"]
                    elif isinstance(response_dict["data"], list):
                        return response_dict["data"]
                    elif "status" in response_dict["data"]:
                        return response_dict["data"]["status"][0][
                            "status"
                        ]  # status response
                    else:
                        return response_dict["data"]
                else:
                    error_message = f"Response Fail: {response_dict['message'] if 'message' in response_dict else ''}"
                    IoTCServiceLogger.get().error(
                        f"{log_message} API_ERROR [{error_message}]"
                    )
                    raise HTTPResponseFailError(error_message)
            except json.JSONDecodeError:
                error_message = "HTTP malformed response"
                IoTCServiceLogger.get().error(
                    f"{log_message} JSON_DECODE_ERROR [{error_message} {res.content.decode()}]"
                )
                raise HTTPMalformedResponseError(error_message)
        else:
            raise HTTPWrongResponseCodeError("Return code %s" % res.status_code)

    def _api_request_body(self, *, method: str, token: str, params: Dict):
        return {
            "jsonrpc": "2.0",
            "method": method,
            "params": params,
            "auth": token,
            "id": "0",
        }

    @property
    def token(self):
        return self._token

    @property
    def studio_url(self):
        return self._studio_credentials.url


class IoTCAPIClientManager:
    instance = None

    @classmethod
    def create(cls, studio_credentials: IoTCStudioCredentials):
        cls.instance = IoTCAPIClient(studio_credentials)

    @classmethod
    def get(cls):
        return cls.instance
