import logging
import logging.handlers
from pathlib import Path
import json
import random
from typing import List, Union
import os
import threading
import time
import redis
import sys

from IoTCAutomationService.common.enums import IoTCCampaignStatus
from IoTCAutomationService.common.errors import (
    SecretNotPassedError,
)


REDIS_HOST = "127.0.0.1"
SCHEDULER_WORKERS = 20
MAX_PROCESSES_PER_CAMPAIGN = 100
MQTT_PORT = 8883

AUTOMATION_SERVICE_PORT = os.environ["AUTOMATION_SERVICE_PORT"]
REDIS_PORT = os.environ["REDIS_PORT"]
CACHE_EX = int(os.environ["CACHE_EX"]) * 3600

NEVER_RUN_PLACEHOLDER_ID = 0

MAX_SECRET_ATTEMPTS = 3
secret_failed_attempts = 0
MAX_CREDENTIALS_ATTEMPTS = 3
credentials_failed_attempts = 0
MAX_API_CALL_ATTEMPTS = 10

BEARER_TOKEN_PREFIX = "Bearer "

TOPIC_VARIABLE_PREFIX = "$"

SERVICE_UNAVAILABLE_KEY = "service_unavailable"

POLLING_ON_JOB_PAUSED = 5

SEC_IN_MINUTE = 60
SEC_IN_HOUR = 60 * SEC_IN_MINUTE
SEC_IN_DAY = SEC_IN_HOUR * 24

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

ARRAY_TO_STRING_DELIMITER = ";"

CAMPAIGN_ABORTABLE_STATUSES = [IoTCCampaignStatus.RUNNING, IoTCCampaignStatus.PAUSED]
CAMPAIGN_PAUSABLE_STATUSES = [IoTCCampaignStatus.RUNNING]
CAMPAIGN_RESUMABLE_STATUSES = [IoTCCampaignStatus.PAUSED]


def MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST(
    mqtt_domain_name, id_target, id_campaign, id_task, id_schedule
):
    return f"/device_manager/devices/{id_target}/campaigns/{id_campaign}/in/tasks/{id_task}/{id_schedule}"


def MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE(
    mqtt_domain_name, id_target, id_campaign, id_task, id_schedule, response_topic
):
    return f"/device_manager/devices/{id_target}/campaigns/{id_campaign}/out/tasks/{id_task}/{id_schedule}/{response_topic}"


def MQTT_TOPIC_CAMPAIGN_REMOVE(id_campaign, id_target):
    return f"/device_manager/devices/{id_target}/campaigns/{id_campaign}/remove"


def EXTRACT_IDS_FROM_MQTT_TOPIC(topic):
    tokens = topic.split("/")
    id_target = tokens[1 + tokens.index("devices")]
    id_campaign = tokens[1 + tokens.index("campaigns")]
    id_task = tokens[1 + tokens.index("tasks")]
    return (id_target, id_campaign, id_task)


def PREPARE_MQTT_MESSAGE(body: dict, message_type: str):
    creation_date = round(time.time() * 1000)
    expiration_date = creation_date + 1000_000
    message_id = (
        "AUTOMATION_SERVICE_" + str(creation_date) + "_" + str(random.getrandbits(32))
    )
    return {
        "SenderId": 10,
        "SenderType": "10",
        "SenderName": "AUTOMATION_SERVICE",
        "MessageId": message_id,
        "RefersTo": "0",
        "MessageType": message_type,
        "MessageBody": body,
        "CreationDate": str(creation_date),
        "ExpirationDate": str(expiration_date),
    }


def notify_campaign_removed(
    logger: logging.Logger, mqtt_client, id_campaign, targets: List[int]
):
    message_type = "0x96"
    message_body = PREPARE_MQTT_MESSAGE({}, message_type)

    for id_target in targets:
        logger.info(
            f"Campaign {id_campaign}. Target {id_target}. Sending MQTT 'REMOVE CAMPAIGN' message"
        )
        mqtt_client.send(
            MQTT_TOPIC_CAMPAIGN_REMOVE(id_campaign, id_target),
            bytes(json.dumps(message_body), "utf8"),
            qos=2,
            retries=None,
        )


def check_service_status():
    redis_client = redis.Redis(REDIS_HOST, int(REDIS_PORT))
    service_unavailable = redis_client.get(SERVICE_UNAVAILABLE_KEY)
    if service_unavailable is not None:
        logging.error(
            "Automation Service unavailable. Key '%s' is currently '%s'"
            % (SERVICE_UNAVAILABLE_KEY, str(service_unavailable))
        )
        sys.exit(1)


def configure_logging(
    filename: Union[str, None] = "/var/log/automation-service/automation-service.log",
    use_stdout: bool = True,
    log_level: int = logging.INFO,
) -> None:
    try:
        handlers: List[logging.Handler] = []

        if filename is not None:
            Path(filename).parent.mkdir(parents=True, exist_ok=True)
            handlers.append(
                logging.handlers.RotatingFileHandler(
                    filename,
                    maxBytes=2_000_000,
                    backupCount=10,
                )
            )

        if use_stdout == True:
            handlers.append(logging.StreamHandler(sys.stdout))

        logger = logging.getLogger()

        formatter = logging.Formatter(
            "%(asctime)s;%(levelname)s;Thread '%(threadName)s';%(message)s"
        )

        for handler in handlers:
            handler.setFormatter(formatter)
            logger.addHandler(handler)
        logger.setLevel(log_level)
    except Exception as err:
        logging.error("Unexpected exception while configuring logging: %s", err)


def set_service_unavailable():
    redis_client = redis.Redis(REDIS_HOST, int(REDIS_PORT))
    redis_client.set(SERVICE_UNAVAILABLE_KEY, 1)
    wait_redis_save(redis_client)
    logging.error("Automation Service set to unavailable")
    logging.error("Automation Service exit")
    BottleManager.stop()


def wait_redis_save(redis_client):
    while True:
        try:
            redis_client.save()
            return
        except redis.ResponseError as err:
            logging.error("Could not save configuration in Redis: %s", err.args[0])
            time.sleep(2)


def update_failed_attempts(key):
    globals()[key + "_failed_attempts"] += 1
    if (
        globals()[key + "_failed_attempts"]
        >= globals()["MAX_" + key.upper() + "_ATTEMPTS"]
    ):
        logging.error(
            "Fatal error: the maximum number of %s attempts has been exceeded", key
        )
        set_service_unavailable()


class IoTCStudioCredentials:
    def __init__(self, userid: str, password: str, passport: str, url: str):
        self._userid = userid
        self._password = password
        self._passport = passport
        self._url = url
        self._token = None

    def has_valid_credentials(self):
        if self._passport is None and (self._userid is None or self._password is None):
            return False
        else:
            return True

    @property
    def userid(self):
        return self._userid

    @property
    def password(self):
        return self._password

    @property
    def passport(self):
        return self._passport

    @property
    def url(self):
        return self._url

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, value):
        self._token = value


class IoTCSecret:
    try:
        try:
            value = sys.argv[1]
        except IndexError:
            raise SecretNotPassedError("no secret arg passed to run script")
    except SecretNotPassedError as err:
        logging.error("Fatal error: %s" % err.args[0])
        sys.exit(1)


class BottleManager:
    server = "gevent"

    @classmethod
    def start(cls, app):
        threading.Thread(
            target=app.run,
            name="REST API",
            kwargs=dict(
                host="0.0.0.0", port=AUTOMATION_SERVICE_PORT, server=cls.server
            ),
        ).start()

    @classmethod
    def stop(cls):
        os._exit(1)


class IoTCRegex:
    @staticmethod
    def extract_topic_variables(topic):
        topic_parts = topic.split("/")
        return [
            part[1:] for part in topic_parts if part.startswith(TOPIC_VARIABLE_PREFIX)
        ]
