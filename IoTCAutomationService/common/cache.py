import json
import threading
import time
from typing import Union
import redis
from abc import ABC, abstractmethod

from IoTCAutomationService.common.api_client import IoTCAPIClientManager
from IoTCAutomationService.common.common import (
    CACHE_EX,
    REDIS_HOST,
    REDIS_PORT,
)
from IoTCAutomationService.common.enums import IoTCEntityType
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class IoTCHypervisorCache(ABC):
    def __init__(self):
        self._redis_client = redis.Redis(REDIS_HOST, int(REDIS_PORT))
        self._redis_keys = {
            "ids": "hypervisor_ids",
            "data": {"suffix": ":hypervisor_data"},
            "entity_attributes": "hypervisor_keys",
        }
        self._entityid_key = "idHypervisor"
        self._entity_name = "hypervisor"

    def get_all_ids(self):
        ids = self._redis_client.get(self._redis_keys["ids"])
        if ids is None:
            self.refresh_all_registries()
            ids = self._redis_client.get(self._redis_keys["ids"])
        return json.loads(ids) if ids is not None else []

    def get_data(self, id):
        data = self._redis_client.get(str(id) + self._redis_keys["data"]["suffix"])
        if data is None:
            self.refresh_all_registries()
            data = self._redis_client.get(str(id) + self._redis_keys["data"]["suffix"])
        return json.loads(data) if data is not None else None

    def get_name(self, id_target: int) -> Union[str, None]:
        """Return the name of the given target."""
        hypervisor_data = self.get_data(id_target)
        if hypervisor_data is None or "name" not in hypervisor_data:
            return None
        name = str(hypervisor_data["name"])
        return name

    def get_architecture(self, id_target: int) -> Union[str, None]:
        """Return the architecture name associated to the given hypervisor id."""
        hypervisor_data = self.get_data(id_target)
        if hypervisor_data is None or "architecture" not in hypervisor_data:
            return None
        architecture = str(hypervisor_data["architecture"])
        return architecture

    def get_entity_attributes(self):
        data = self._redis_client.get(self._redis_keys["entity_attributes"])
        if data is None:
            self.refresh_all_registries()
            data = self._redis_client.get(self._redis_keys["entity_attributes"])
        return json.loads(data)

    def refresh_all_registries(self):
        api_client = self._get_api_client()
        entities = self._get_all_registries(api_client)
        for entity in entities:
            self._redis_client.set(
                entity[self._entityid_key] + self._redis_keys["data"]["suffix"],
                json.dumps(entity),
            )

        if len(entities) > 0:
            self._redis_client.set(
                self._redis_keys["entity_attributes"],
                json.dumps(list(entities[0].keys())),
            )
            self._redis_client.set(
                self._redis_keys["ids"],
                json.dumps([entity[self._entityid_key] for entity in entities]),
            )
        else:
            IoTCServiceLogger.get().error(
                "No {0}s found in IoT Catalyst Studio library, cannot persist {0} keys".format(
                    self._entity_name.capitalize()
                )
            )

    def _get_api_client(self):
        api_client = IoTCAPIClientManager.get()
        if api_client is None:
            raise Exception(
                "No API Client available for %s Cache, is the service initialized?"
                % self._entity_name.capitalize()
            )
        return api_client

    def _get_all_registries(self, api_client):
        return api_client.get_all_hypervisor_registries()


class CacheRefresher(threading.Thread):
    def __init__(self, sleep_time):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.name = "Cache Refresher"

        self._stop_signal = False
        self._sleep_time = sleep_time

        self._caches = {
            IoTCEntityType.HYPERVISOR.value: {"cache_obj": IoTCHypervisorCache()},
        }

    def start(self):
        super().start()

    def stop(self):
        self._stop_signal = True

    def run(self):
        while True:
            if self._stop_signal:
                break

            time.sleep(self._sleep_time)

            self.work()

    @abstractmethod
    def work(self) -> None:
        pass


class CacheAutoRefresher(CacheRefresher):
    def __init__(self, sleep_time):
        super().__init__(sleep_time)
        self.name = "Cache Auto Refresher"

    def work(self):
        try:
            for cache in self._caches.values():
                try:
                    cache["cache_obj"].refresh_all_registries()
                except Exception as err:
                    IoTCServiceLogger.get().error(str(err))
        except Exception as err:
            IoTCServiceLogger.get().error(str(err))


def configure_cache_refreshers():
    scheduled_refresher = CacheAutoRefresher(CACHE_EX)
    scheduled_refresher.start()
