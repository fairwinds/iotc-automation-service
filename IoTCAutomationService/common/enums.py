from enum import Enum


class IoTCEntityCode(Enum):
    CONTAINER = "3"
    HYPERVISOR = "5"


class IoTCEntityType(Enum):
    HYPERVISOR = "hypervisor"
    CONTAINER = "container"


class IoTCAutomationServiceStatus(Enum):
    OK = "OK"
    KO = "KO"


class IoTCBrokerStatus(Enum):
    OK = "OK"
    KO = "KO"


class IoTCStudioStatus(Enum):
    OK = "OK"
    KO = "KO"


class IoTCCampaignSemaphoreValue(Enum):
    OK = 0
    PAUSE = 1
    ABORT = -1


class IoTCOnCampaignError(Enum):
    EXIT = "exit"
    PAUSE = "pause"
    SKIP = "skip"


class IoTCCampaignStatus(Enum):
    RUNNING = "running"
    PAUSED = "paused"
    ABORTED = "aborted"
    EXITED_OK = "exited_ok"
    EXITED_KO = "exited_ko"
    NEVER_RUN = "never_run"


class IoTCTaskStatus(Enum):
    NEVER_RUN = IoTCCampaignStatus.NEVER_RUN.value
    RUNNING = IoTCCampaignStatus.RUNNING.value
    EXITED_OK = IoTCCampaignStatus.EXITED_OK.value
    EXITED_KO = IoTCCampaignStatus.EXITED_KO.value


class IoTCActionOnCampaign(Enum):
    ABORT = "abort"
    PAUSE = "pause"
    RESUME = "resume"


class IoTCDataType(Enum):
    INT = "int"
    STRING = "str"
    ARRAY = "array"
    BOOL = "bool"


class IoTCCampaignSchedule(Enum):
    SCHEDULED = 1
    NOT_SCHEDULED = 0
    REMOVED = -1


class IoTCCommandDeliveryStatus(Enum):
    NOT_SENT = 0
    REQUEST_SENT = 1
    RESPONSE_RECEIVED = 2


class IoTCCacheManagerKey(Enum):
    HYPERVISORS_LAST_UPDATE = "hypervisors_last_update"
    CONTAINERS_LAST_UPDATE = "containers_last_update"


class LogLevel(Enum):
    ERROR = "error"
    WARNING = "warning"
    INFO = "info"
    DEBUG = "debug"
    ALL = "all"
