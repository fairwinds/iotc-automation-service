class MissingSettingsError(Exception):
    pass


class MissingCertificatesError(Exception):
    pass


class CampaignNotFoundError(Exception):
    pass


class SubjobNotFoundError(Exception):
    pass


class ProcessNotRunningError(Exception):
    pass


class SemaphoreKOError(Exception):
    pass


class UnrecognizedTopicVariableError(Exception):
    pass


class SecretNotPassedError(Exception):
    pass


class AutomationServiceAlreadyInitializedError(Exception):
    pass


class HTTPResponseFailError(Exception):
    pass


class InvalidTokenError(Exception):
    pass


class HTTPWrongResponseCodeError(Exception):
    pass


class HTTPResponseTimeoutError(Exception):
    pass


class HTTPConnectTimeoutError(Exception):
    pass


class HTTPConnectionError(Exception):
    pass


class HTTPMalformedResponseError(Exception):
    pass


class APIClientError(Exception):
    pass


class HTTPUnauthorizedRequestError(Exception):
    pass


class MissingCredentialsError(Exception):
    pass


class UnitializedAutomationServiceError(Exception):
    pass


class WrongRequestError(Exception):
    pass


class InvalidCampaignScheduleError(Exception):
    pass


class InvalidCampaignTimeError(Exception):
    pass


class UnprocessableCampaignCommandError(Exception):
    pass


class InvalidCampaignOptionError(Exception):
    pass


class CommandExecutionException(Exception):
    pass


class CommandHashRetrievalError(Exception):
    pass


class TargetNotFoundError(Exception):
    pass


class ScheduleNotFoundError(Exception):
    pass


class EmptyTargetsError(Exception):
    pass


class EmptyPageError(Exception):
    pass


class UnrecognizedEntityTypeError(Exception):
    pass


class MultipleCommandTypesError(Exception):
    pass


class InvalidCommandTypeError(Exception):
    pass


class MissingTargetsError(Exception):
    pass


class TooManyApiCallAttemptsError(Exception):
    pass


class CountOnTableError(Exception):
    pass


class EmptyCommandsCampaignError(Exception):
    pass


class TooLowIntervalError(Exception):
    pass


class WrongIntervalError(Exception):
    pass


class PayloadNotJsonError(Exception):
    pass


class UnrecognizedWildcardError(Exception):
    pass
