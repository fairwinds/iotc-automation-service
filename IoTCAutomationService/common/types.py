from typing import Dict, List, NamedTuple

from IoTCAutomationService.common.enums import LogLevel


class IoTCLog(NamedTuple):
    channel: str
    level: LogLevel
    message: str
    context: str
    extra: str


TaskIdRegistry = Dict[int, List[int]]
"""
A dict in which:
- Key is the id of the task definition/result inside the database.
- Value is a list of its command ids inside the database.

Example data:
>>> {
    1: [1, 2, 3], # Task with id 1. commands id are 1, 2, 3
    2: [4, 5], # Task with id 2. commands id are 4, 5
    3: [6],
}
"""


class IoTCTargetArchitecture(NamedTuple):
    """Relationship between a target and its architecture."""

    id_target: int
    name: str
    architecture: str
