from typing import Dict
from bottle import request
from bottle import HTTPResponse
from bottle import request
from jsonschema import validate as jsonschema_validate
from jsonschema.exceptions import ValidationError
import http
import json

from IoTCAutomationService.common.api_client import IoTCAPIClientManager
from IoTCAutomationService.common.common import (
    BEARER_TOKEN_PREFIX,
    IoTCSecret,
    update_failed_attempts,
)
from IoTCAutomationService.common.enums import IoTCDataType
from IoTCAutomationService.common.errors import (
    APIClientError,
    AutomationServiceAlreadyInitializedError,
    HTTPUnauthorizedRequestError,
    UnitializedAutomationServiceError,
    WrongRequestError,
)
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.validators import IoTCValidator
from IoTCAutomationService.persistence.log import IoTCServiceLogger


hypervisor_cache = IoTCHypervisorCache()


def requires_secret(func):
    def wrapper(*args, **kwargs):
        try:
            if IoTCSecret.value is None:
                raise AutomationServiceAlreadyInitializedError(
                    "Automation Service already initialized"
                )

            secret = _get_bearer_token_from_request()

            if secret != IoTCSecret.value:
                update_failed_attempts("secret")
                raise HTTPUnauthorizedRequestError(
                    "The request has not the correct authorization value"
                )

            return func(*args, **kwargs)
        except HTTPUnauthorizedRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.UNAUTHORIZED.value,
            )
        except AutomationServiceAlreadyInitializedError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.CONFLICT.value,
            )

    return wrapper


def requires_token_verification(func):
    def wrapper(*args, **kwargs):
        try:
            if IoTCSecret.value is not None:
                raise UnitializedAutomationServiceError(
                    "Automation Service needs to be initialized before calling the endpoint %s"
                    % request.fullpath
                )

            token = _get_bearer_token_from_request()

            if _is_token_verified(token):
                return func(*args, **kwargs)
            else:
                raise HTTPUnauthorizedRequestError("Token is not valid")

        except UnitializedAutomationServiceError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.NOT_FOUND.value,
            )
        except HTTPUnauthorizedRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.UNAUTHORIZED.value,
            )
        except APIClientError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )
        except Exception as err:
            error_message = "Unexpected exception"
            if hasattr(err, "args") and len(err.args) > 0:
                error_message += ": %s" % err.args[0]

            IoTCServiceLogger.get().error(error_message)
            return HTTPResponse(
                json.dumps({"data": None, "message": error_message, "info": {}}),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    return wrapper


def eval_force_refresh(func):
    def wrapper(*args, **kwargs):
        try:
            params = dict(request.query)
            if "force_refresh" in params:
                params_schema = [
                    {
                        "name": "force_refresh",
                        "default": "false",
                        "required": False,
                        "dtype": IoTCDataType.STRING,
                        "allowed_values": ["true", "false"],
                    }
                ]
                IoTCValidator.validate_query_params(params, params_schema)
                if params["force_refresh"] == "true":
                    hypervisor_cache.refresh_all_registries()

            return func(*args, **kwargs)
        except WrongRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )
        except Exception as err:
            error_message = "Unexpected exception"
            if hasattr(err, "args") and len(err.args) > 0:
                error_message += ": %s" % err.args[0]

            IoTCServiceLogger.get().error(error_message)
            return HTTPResponse(
                json.dumps({"data": None, "message": error_message, "info": {}}),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR.value,
            )

    return wrapper


def needs_json_body(func):
    def wrapper(*args, **kwargs):
        try:
            payload = request.body.read()
            try:
                json.loads(payload)
            except json.decoder.JSONDecodeError:
                raise WrongRequestError("Request body is not a valid json")
            return func(*args, **kwargs)
        except WrongRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )

    return wrapper


def validate_json_body(schema: Dict):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                payload_dict = json.loads(request.body.read())
                jsonschema_validate(instance=payload_dict, schema=schema)
                return func(*args, **kwargs)
            except ValidationError as err:
                try:
                    # Try to build a readable validation error message. If failure, fallback to default json schema error.
                    # Inspiration taken from jsonschema.exceptions.ValidationError.__str__()
                    readable_schema_path = ".".join(["schema", *err.schema_path][:-1])
                    message = f"Invalid request body. Failed validating {err.validator!r} in {readable_schema_path!r}. {err.args[0]}"
                except:
                    message = f"Invalid request body. {err.args[0]}"
                info = {"json_path": err.json_path}
                IoTCServiceLogger.get().warning(
                    f"User provided an invalid request body for endpoint '{request.path}'. Error: {err}. Body: {payload_dict}"
                )
                return HTTPResponse(
                    json.dumps({"data": None, "message": message, "info": info}),
                    status=http.HTTPStatus.BAD_REQUEST.value,
                )
            except Exception as err:
                return HTTPResponse(
                    json.dumps({"data": None, "message": err.args[0], "info": {}}),
                    status=http.HTTPStatus.BAD_REQUEST.value,
                )

        return wrapper

    return decorator


def inject_json_body(func):
    def wrapper(*args, **kwargs):
        try:
            payload_dict = json.loads(request.body.read())
            return func(payload_dict, *args, **kwargs)
        except WrongRequestError as err:
            IoTCServiceLogger.get().error(err.args[0])
            return HTTPResponse(
                json.dumps({"data": None, "message": err.args[0], "info": {}}),
                status=http.HTTPStatus.BAD_REQUEST.value,
            )

    return wrapper


def _get_bearer_token_from_request():
    auth_header = request.headers.get("Authorization")

    if auth_header is None:
        raise HTTPUnauthorizedRequestError("The request has no Authorization header")
    if auth_header[: len(BEARER_TOKEN_PREFIX)] != BEARER_TOKEN_PREFIX:
        raise HTTPUnauthorizedRequestError(
            "The request has no Bearer Token in the Authorization header"
        )

    return auth_header[len(BEARER_TOKEN_PREFIX) :]


def bind_query_params(params_schema):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                params = dict(request.query)
                query_params = IoTCValidator.validate_query_params(
                    params, params_schema
                )

                return func(*query_params, *args, **kwargs)
            except WrongRequestError as err:
                IoTCServiceLogger.get().error(err.args[0])
                return HTTPResponse(
                    json.dumps({"data": None, "message": err.args[0], "info": {}}),
                    status=http.HTTPStatus.BAD_REQUEST.value,
                )

        return wrapper

    return decorator


def bind_request_body(schema):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                try:
                    body = json.loads(request.body.read())
                except json.decoder.JSONDecodeError:
                    raise WrongRequestError("Request body needs to be a valid json")

                extracted_dict = IoTCValidator.validate_dict(
                    body, schema, origin="request body"
                )

                return func(*extracted_dict, *args, **kwargs)
            except WrongRequestError as err:
                IoTCServiceLogger.get().error(err.args[0])
                return HTTPResponse(
                    json.dumps({"data": None, "message": err.args[0], "info": {}}),
                    status=http.HTTPStatus.BAD_REQUEST.value,
                )

        return wrapper

    return decorator


def _is_token_verified(token):
    api_client = IoTCAPIClientManager.get()
    is_verified = api_client.verify_token(token)
    return is_verified
