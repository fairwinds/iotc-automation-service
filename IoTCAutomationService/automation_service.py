from pathlib import Path
import sys
from typing import Dict
import redis
import base64
from hashlib import sha256
from cryptography import fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

from IoTCAutomationService.IoTCMqttClient import IoTCMqttClientManager, IoTCMqttClient
from IoTCAutomationService.common.api_client import IoTCAPIClient, IoTCAPIClientManager
from IoTCAutomationService.common.common import (
    REDIS_HOST,
    REDIS_PORT,
    BottleManager,
    IoTCSecret,
    IoTCStudioCredentials,
    set_service_unavailable,
    wait_redis_save,
)
from IoTCAutomationService.common.enums import IoTCAutomationServiceStatus
from IoTCAutomationService.common.errors import (
    MissingCertificatesError,
    MissingSettingsError,
)
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class IoTCAutomationService:
    def __init__(self):
        self._status = {
            "status": None,
            "info": None,
            "broker_status": None,
            "studio_status": None,
        }
        self._api_client: IoTCAPIClient = None
        self._redis = redis.Redis(REDIS_HOST, REDIS_PORT)

        self._load_internal_config()

    def _parse_credentials(self, credentials):
        userid = credentials["userid"] if "userid" in credentials else None
        password = credentials["password"] if "password" in credentials else None
        passport = credentials["passport"] if "passport" in credentials else None
        studio_url = credentials["studio_url"]
        return {
            "userid": userid,
            "password": password,
            "passport": passport,
            "studio_url": studio_url,
        }

    def _remove_secret(self):
        IoTCSecret.value = None
        sys.argv[1] = None

    def _load_internal_config(self):
        passport, userid, password, studio_url = self._redis.mget(
            ["passport", "userid", "password", "studio_url"]
        )

        mqtt_settings_keys = [
            "PUBLIC_MQTT_BROKER_IP",
            "PUBLIC_MQTT_BROKER_TCP_PORT",
            "DOMAIN",
            "ca",
            "cert",
            "key",
        ]
        mqtt_settings_vals = self._redis.hmget("mqtt_settings", mqtt_settings_keys)

        auth1_list = [userid, password, studio_url] + mqtt_settings_vals
        auth2_list = [passport, studio_url] + mqtt_settings_vals
        if None in auth1_list and None in auth2_list:
            IoTCServiceLogger.get().info(
                "No credentials found in Redis, waiting for initialization"
            )
            return

        mqtt_settings = dict(
            zip(
                mqtt_settings_keys,
                [
                    val.decode() if isinstance(val, bytes) else val
                    for val in mqtt_settings_vals
                ],
            )
        )

        try:
            decrypted_studio_url = IoTCFernetCipher.decrypt(studio_url)

            if passport is not None:
                decrypted_passport = IoTCFernetCipher.decrypt(passport)

                self._configure_from_redis_storage(
                    {
                        "passport": decrypted_passport,
                        "studio_url": decrypted_studio_url,
                    },
                    mqtt_settings,
                )
            elif userid is not None and password is not None:
                decrypted_userid = IoTCFernetCipher.decrypt(userid)
                decrypted_password = IoTCFernetCipher.decrypt(password)

                self._configure_from_redis_storage(
                    {
                        "userid": decrypted_userid,
                        "password": decrypted_password,
                        "studio_url": decrypted_studio_url,
                    },
                    mqtt_settings,
                )
            else:
                IoTCServiceLogger.get().info(
                    "No credentials found in Redis, waiting for initialization"
                )
                return

            self.status = {
                "status": IoTCAutomationServiceStatus.OK.value,
                "info": "Configuration loaded from Redis",
            }
        except fernet.InvalidToken:
            IoTCServiceLogger.get().error(
                "Invalid Fernet decryption. Maybe the encrypted values have been changed?"
            )
            set_service_unavailable()
        except MissingSettingsError as err:
            IoTCServiceLogger.get().error("ERROR: %s" % err)
            BottleManager.stop()

    def configure_from_outside_request(self, credentials: Dict):
        self._configure_api_client(self._parse_credentials(credentials))
        self._persist_encrypted_keys_in_redis(credentials)
        self._instantiate_mqtt_client()
        self._remove_secret()

    def _configure_from_redis_storage(self, credentials: Dict, mqtt_settings):
        self._configure_api_client(self._parse_credentials(credentials))
        self._instantiate_mqtt_client(mqtt_settings)
        self._remove_secret()

    def _configure_api_client(self, credentials):
        IoTCAPIClientManager.create(
            IoTCStudioCredentials(
                userid=credentials["userid"],
                password=credentials["password"],
                passport=credentials["passport"],
                url=credentials["studio_url"],
            )
        )
        self._api_client = IoTCAPIClientManager.get()

    def _persist_encrypted_keys_in_redis(self, dict_to_save):
        for key, value in dict_to_save.items():
            if value is not None:
                self._redis.set(key, IoTCFernetCipher.encrypt(value))
        wait_redis_save(self._redis)

    def _persist_hash_in_redis(self, hash_key, dict_to_save):
        self._redis.hmset(hash_key, dict_to_save)
        wait_redis_save(self._redis)

    def _get_studio_settings(self):
        settings = self._api_client.get_iotc_settings()

        if (
            not isinstance(settings, dict)
            or "PUBLIC_MQTT_BROKER_IP" not in settings
            or "PUBLIC_MQTT_BROKER_TCP_PORT" not in settings
        ):
            raise MissingSettingsError(
                "Can't retrieve MQTT broker settings from %s"
                % self._api_client.studio_url
            )
        if "DOMAIN" not in settings:
            raise MissingSettingsError(
                "Can't retrieve setting 'DOMAIN' from %s" % self._api_client.studio_url
            )

        return settings

    def _get_mqtt_certificates(self):
        certificates = self._api_client.get_mqtt_certificates()
        if None in certificates.values():
            raise MissingCertificatesError("missing one or more MQTT certificates")
        return certificates

    def _instantiate_mqtt_client(self, mqtt_settings=None):
        IoTCAutomationService._stop_previous_mqtt_client()

        if mqtt_settings is not None:
            domain_name, broker_host, broker_port = (
                mqtt_settings["DOMAIN"],
                mqtt_settings["PUBLIC_MQTT_BROKER_IP"],
                int(mqtt_settings["PUBLIC_MQTT_BROKER_TCP_PORT"]),
            )
            ca, cert, key = (
                mqtt_settings["ca"],
                mqtt_settings["cert"],
                mqtt_settings["key"],
            )
        else:
            mqtt_studio_settings = {
                key: value
                for key, value in self._get_studio_settings().items()
                if key
                in ["DOMAIN", "PUBLIC_MQTT_BROKER_IP", "PUBLIC_MQTT_BROKER_TCP_PORT"]
            }
            mqtt_certificates = {
                key: value
                for key, value in self._get_mqtt_certificates().items()
                if key in ["ca", "cert", "key"]
            }

            self._persist_hash_in_redis(
                "mqtt_settings", {**mqtt_studio_settings, **mqtt_certificates}
            )

            domain_name, broker_host, broker_port = (
                mqtt_studio_settings["DOMAIN"],
                mqtt_studio_settings["PUBLIC_MQTT_BROKER_IP"],
                int(mqtt_studio_settings["PUBLIC_MQTT_BROKER_TCP_PORT"]),
            )

            ca = mqtt_certificates["ca"]
            cert = mqtt_certificates["cert"]
            key = mqtt_certificates["key"]

        def create_cert_files():
            if not Path("ca.crt").exists():
                with open("ca.crt", mode="w") as f:
                    f.write(ca)
            if not Path("client.crt").exists():
                with open("client.crt", mode="w") as f:
                    f.write(cert)
            if not Path("client.key").exists():
                with open("client.key", mode="w") as f:
                    f.write(key)

        create_cert_files()

        # MQTT Client id should always be the same so that,
        # if clean_session=False and if the AS is turned off,
        # when it is turned on again it receives the pending messages.
        client_id = (
            "iotcautomationservice_"
            # + str(round(time.time()))
            # + "_"
            # + str(random.getrandbits(32))
        )

        # Since the token is also used as MQTT password,
        # ensure it is valid before starting the MQTT client,
        # otherwise the client may not ever be able to connect to the broker.
        # api_client = IoTCAPIClientManager.get()
        # api_client.refresh_token()

        def get_mqtt_client_password() -> str:
            """Utility for the MQTT client to retrieve a valid password (ie. API token) on reconnect."""
            api_client = IoTCAPIClientManager.get()
            api_client.refresh_token()
            return api_client.token

        mqtt_client = IoTCMqttClient(
            transport="tcp",
            client_id=client_id,
            username="iotcautomationservice_1",
            get_password=get_mqtt_client_password,
            cacert="ca.crt",
            cert_file="client.crt",
            key_file="client.key",
            clean_session=False,
        )
        IoTCMqttClientManager.set_instance(mqtt_client)
        IoTCMqttClientManager.get().connect(broker_host, broker_port)
        IoTCMqttClientManager.get().domain_name = domain_name
        IoTCMqttClientManager.get().start()

    @staticmethod
    def _stop_previous_mqtt_client():
        mqtt_client = IoTCMqttClientManager.get()
        if mqtt_client is not None and isinstance(mqtt_client, IoTCMqttClient):
            mqtt_client.stop()

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status_dict):
        self._status.update(status_dict)

    @property
    def api_client(self):
        return self._api_client


class IoTCFernetCipher:
    @staticmethod
    def encrypt(text):
        return fernet.Fernet(IoTCFernetCipher.get_fernet_key()).encrypt(text.encode())

    @staticmethod
    def decrypt(encrypted_text):
        return (
            fernet.Fernet(IoTCFernetCipher.get_fernet_key())
            .decrypt(encrypted_text)
            .decode()
        )

    @staticmethod
    def get_fernet_key():
        salt = sha256(IoTCSecret.value.encode()).hexdigest().encode()
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(), length=32, salt=salt, iterations=100000
        )
        return base64.urlsafe_b64encode(kdf.derive(IoTCSecret.value.encode()))
