import json

from IoTCAutomationService.common.common import EXTRACT_IDS_FROM_MQTT_TOPIC
from IoTCAutomationService.persistence.log import IoTCServiceLogger
from IoTCAutomationService.mqtt_client import IoTCMqttClientGeneric


class IoTCMqttClientManager:
    instance = None

    @classmethod
    def set_instance(cls, mqtt_client: "IoTCMqttClient"):
        cls.instance = mqtt_client

    @classmethod
    def get(cls):
        return cls.instance


class IoTCMqttClient(IoTCMqttClientGeneric):
    def __init__(
        self,
        transport,
        client_id,
        username,
        get_password=None,
        cacert=None,
        cert_file=None,
        key_file=None,
        clean_session=False,
    ):
        super().__init__(
            IoTCServiceLogger.get(),
            transport,
            client_id,
            username,
            get_password,
            cacert,
            cert_file,
            key_file,
            clean_session,
        )
        self.domain_name = None
        self._callbacks = {}

    def add_callback(self, topic, callback):
        self._callbacks.update({topic: callback})

    def receive(self, topic, msg):
        try:
            id_target, id_campaign, id_task = EXTRACT_IDS_FROM_MQTT_TOPIC(topic)
        except Exception as err:
            IoTCServiceLogger.get().error(
                "Mqtt on message. Topic '%s'. Exception during topic parsing: %s"
                % (topic, err)
            )

        try:
            payload = json.loads(msg.decode())
            body = payload["MessageBody"]
            callback = self._callbacks.get(topic)
            if callback is None:
                IoTCServiceLogger.get().warning(
                    "Campaign %s. Target %s. Task %s. Mqtt on message. Topic '%s'. No such callback. Message will be discarded"
                    % (id_campaign, id_target, id_task, topic)
                )
            else:
                callback(self, body)
        except Exception as err:
            IoTCServiceLogger.get().error(
                "Mqtt on message. Topic '%s'. Exception during payload parsing: %s. Payload is: %s"
                % (topic, err, msg)
            )

    def on_publish(self, client, userdata, mid):
        return super().on_publish(client, userdata, mid)
