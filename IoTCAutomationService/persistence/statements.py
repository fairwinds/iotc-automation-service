"""
    A collection of SQL query statements. Each query is associated with a parameters dict.
"""

import json
from typing import Any, Dict, List, Optional, Tuple, Union
import sqlalchemy

from IoTCAutomationService.common.common import (
    CAMPAIGN_ABORTABLE_STATUSES,
    CAMPAIGN_PAUSABLE_STATUSES,
    CAMPAIGN_RESUMABLE_STATUSES,
)
from IoTCAutomationService.common.enums import (
    IoTCCampaignSchedule,
    IoTCCampaignStatus,
    IoTCCommandDeliveryStatus,
    IoTCCampaignSemaphoreValue,
    IoTCTaskStatus,
    LogLevel,
)
from IoTCAutomationService.common.types import IoTCLog, IoTCTargetArchitecture


DBStatement = Tuple[Any, Dict[str, Any]]
"""
A tuple in which:
    - The first element is the parameterized query
    - The second element is the dict of query parameters
"""


class Insert:
    """
    The Automation Service sql insert statements are defined here.
    """

    @staticmethod
    def CAMPAIGN(
        campaign_def: Dict[str, Any], targets: List[int], n_subjobs: int
    ) -> DBStatement:
        query = """
            INSERT INTO campaign 
            (
                id_component_type, 
                name,
                description,
                targets,
                scheduled,
                suspendable,
                abortable,
                schedule,
                n_subjobs,
                definition,
                on_error
            ) 
            values 
            (
                :id_component_type,
                :name,
                :description,
                :targets,
                :scheduled,
                :suspendable,
                :abortable,
                :schedule,
                :n_subjobs,
                :definition,
                :on_error
            );
        """

        scheduled = (
            IoTCCampaignSchedule.SCHEDULED.value
            if "schedule" in campaign_def["execution"]
            or "in" in campaign_def["execution"]
            or "every" in campaign_def["execution"]
            else IoTCCampaignSchedule.NOT_SCHEDULED.value
        )

        schedule_info = None
        if "schedule" in campaign_def["execution"]:
            schedule_info = json.dumps(
                {"crontab": campaign_def["execution"]["schedule"]}
            )
        elif "in" in campaign_def["execution"]:
            schedule_info = json.dumps({"when": campaign_def["execution"]["in"]})
        elif "every" in campaign_def["execution"]:
            schedule_info = json.dumps({"every": campaign_def["execution"]["every"]})

        id_component_type = (
            int(campaign_def["target"]["idComponentType"])
            if "target" in campaign_def
            else None
        )

        on_error = campaign_def["execution"]["on_error"]

        params = {
            "id_component_type": id_component_type,
            "name": campaign_def["name"],
            "description": campaign_def["description"],
            "targets": json.dumps(targets),
            "scheduled": scheduled,
            "suspendable": campaign_def["execution"]["suspendable"],
            "abortable": campaign_def["execution"]["abortable"],
            "schedule": schedule_info,
            "n_subjobs": n_subjobs,
            "definition": json.dumps(campaign_def),
            "on_error": on_error,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def CAMPAIGN_ARCHITECTURE(id_campaign: int, architecture: str) -> DBStatement:
        """Given a campaign, insert an entry to the list of its architectures."""
        query = """
            INSERT INTO campaign_architecture
            (
                id_campaign,
                architecture
            )
            VALUES
            (
                :id_campaign,
                :architecture
            );
        """
        params = {
            "id_campaign": id_campaign,
            "architecture": architecture,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TASK(id_campaign: int, task: Dict[str, Any]) -> DBStatement:
        query = """
            INSERT INTO task_invocation_registry
            (
                id_campaign,
                name,
                description,
                conditions,
                on_error_policy
            ) 
            values
            (
                :id_campaign, 
                :name, 
                :description, 
                :conditions, 
                :on_error_policy
            );
        """
        params = {
            "id_campaign": id_campaign,
            "name": task["name"],
            "description": task["description"],
            "conditions": json.dumps(task["conditions"]),
            "on_error_policy": task["on_error_policy"]
            if "on_error_policy" in task
            else None,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SPECIALIZED_COMMAND(command_type: str, command: Dict[str, Any]) -> DBStatement:
        query: Dict[str, str] = {
            "API": """
                INSERT INTO api_command_invocation_registry 
                (
                    method,
                    params
                ) 
                values 
                (
                    :method, 
                    :params
                );
            """,
            "METRIC": """
                INSERT INTO metric_command_invocation_registry 
                (
                    id_shell_command,
                    data_type,
                    date_format,
                    refresh_interval
                )
                values
                (
                    (
                        SELECT id FROM shell_command 
                        WHERE name=:shell_command_name
                            AND preview=:shell_command_preview
                            AND deprecation_date IS NULL
                    ),
                    :data_type,
                    :date_format,
                    :refresh_interval
                );
            """,
            "SHELL": """
                INSERT INTO shell_command_invocation_registry 
                (
                    id_shell_command, 
                    causes_reboot
                ) values
                (
                    (
                        SELECT id FROM shell_command 
                        WHERE name=:shell_command_name
                            AND preview=:shell_command_preview
                            AND deprecation_date IS NULL
                    ),
                    :causes_reboot
                )
            """,
            "PRESET": """
                INSERT INTO hypervisor_command_invocation_registry 
                (type) 
                values 
                (:type);
            """,
        }

        if command_type == "API":
            params = {
                "method": command["command"],
                "params": json.dumps(command["params"]),
            }
        elif command_type == "METRIC":
            params = {
                "shell_command_name": command["name"],
                "shell_command_preview": command["command"],
                "data_type": command["data_type"],
                "date_format": command["date_format"]
                if "date_format" in command
                else "NULL",
                "refresh_interval": command["refresh_interval"],
            }
        elif command_type == "SHELL":
            params = {
                "shell_command_name": command["name"],
                "shell_command_preview": command["command"],
                "causes_reboot": command["causes_reboot"],
            }
        elif command_type == "PRESET":
            params = {
                "type": command["command"],
            }
        else:
            raise ValueError(
                f"[CAMPAIGN PERSISTENCE] Cannot persist commands of the given type: '{command_type}'. Command is: {command}"
            )

        return sqlalchemy.text(query[command_type]), params

    @staticmethod
    def BASE_COMMAND(
        id_campaign: int,
        id_task: int,
        id_specialized_command: int,
        command_type: str,
        command: Dict,
    ) -> DBStatement:
        query = """
            INSERT INTO base_command_invocation_registry
            (
                id_specialized_command,
                id_command_type,
                id_campaign,
                id_task,
                definition,
                timeout,
                retries
            )
            values
            (
                :id_specialized_command,
                (SELECT id FROM command_type WHERE name=:command_type),
                :id_campaign,
                :id_task,
                :definition,
                :timeout,
                :retries
            );
        """
        params = {
            "id_specialized_command": id_specialized_command,
            "command_type": command_type,
            "id_campaign": id_campaign,
            "id_task": id_task,
            "definition": json.dumps(command),
            "timeout": command["timeout"],
            "retries": command["retries"],
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SCHEDULE(id_campaign: int, start_time: int) -> DBStatement:
        query = """
            INSERT INTO schedule 
            (
                id_campaign, 
                status, 
                campaign_semaphore, 
                start_time
            )
            values
            (
                :id_campaign, 
                :status, 
                :semaphore, 
                FROM_UNIXTIME(:start_time)
            );
        """
        params = {
            "id_campaign": id_campaign,
            "status": IoTCCampaignStatus.RUNNING.value,
            "semaphore": IoTCCampaignSemaphoreValue.OK.value,
            "start_time": start_time,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TARGET_STATUS(
        id_campaign: int,
        id_schedule: int,
        id_subjob: int,
        target: IoTCTargetArchitecture,
    ) -> DBStatement:
        query = """
            INSERT INTO target_campaign_status 
            (
                id_campaign,
                id_schedule,
                id_target,
                id_subjob,
                name,
                architecture,
                status
            )
            values
            (
                :id_campaign,
                :id_schedule,
                :id_target,
                :id_subjob,
                :name,
                :architecture,
                :never_run_status
            );
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": target.id_target,
            "id_subjob": id_subjob,
            "name": target.name,
            "architecture": target.architecture,
            "never_run_status": IoTCCampaignStatus.NEVER_RUN.value,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TASK_RESULT(
        id_campaign: int,
        id_schedule: int,
        id_task_registry: int,
        id_target: int,
        id_subjob: int,
    ) -> DBStatement:
        query = """
            INSERT INTO task_invocation_result 
            (
                id_task, 
                id_campaign, 
                id_schedule,
                id_target,
                id_subjob,
                status
            )
            values
            (
                :id_task_registry,
                :id_campaign,
                :id_schedule,
                :id_target,
                :id_subjob,
                :never_run_status
            );
        """
        params = {
            "id_task_registry": id_task_registry,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
            "id_subjob": id_subjob,
            "never_run_status": IoTCCampaignStatus.NEVER_RUN.value,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def LOG(log: IoTCLog) -> DBStatement:
        query = """
            INSERT INTO log 
            (
                channel,
                level,
                message,
                context,
                extra
            )
            values
            (
                :channel,
                :level,
                :message,
                :context,
                :extra
            );
        """
        params = {
            "channel": log.channel,
            "level": log.level.value,
            "message": log.message,
            "context": log.context,
            "extra": log.extra,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def COMMAND_RESULT(
        id_campaign: int,
        id_schedule: int,
        id_task_registry: int,
        id_task_result: int,
        id_command_registry: int,
        id_target: int,
        id_subjob: int,
    ) -> DBStatement:
        query = """
            INSERT INTO command_invocation_result
            (
                id_campaign,
                id_schedule,
                id_task,
                id_task_result,
                id_command,
                id_target,
                id_subjob,
                delivery_status
            )
            values
            (
                :id_campaign,
                :id_schedule,
                :id_task_registry,
                :id_task_result,
                :id_command_registry,
                :id_target,
                :id_subjob,
                :delivery_status
            );
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_task_registry": id_task_registry,
            "id_task_result": id_task_result,
            "id_command_registry": id_command_registry,
            "id_target": id_target,
            "id_subjob": id_subjob,
            "delivery_status": IoTCCommandDeliveryStatus.NOT_SENT.value,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def LIBRARY_COMMAND(
        command_def: Dict, id_previous_command: Union[int, None]
    ) -> DBStatement:
        """
        Insert a command in the Automation Service library.
        The command may be a brand new one, or a new version of an existing command.
        If it's a new version, it will be linked to the previous one.

        Args:
        - command_def: The command to be inserted into the library.
        - previous_command: None or ID of the previous command that has been deprecated in favour of this one. Used for command update purposes.
        """

        query = f"""
            INSERT INTO shell_command 
            (
                name,
                id_original_shell_command,
                preview, 
                interpreter, 
                is_metric
            )
            VALUES
            (
                :name,
                {":previous_command" if id_previous_command is not None else "NULL"},
                :preview,
                :interpreter,
                :is_metric
            );
        """
        is_metric = True if command_def["type"] == "METRIC" else False
        params = {
            "name": command_def["name"],
            "preview": command_def["command"],
            "interpreter": command_def["interpreter"],
            "is_metric": is_metric,
        }
        if id_previous_command is not None:
            params.update({"previous_command": id_previous_command})

        return sqlalchemy.text(query), params

    @staticmethod
    def LIBRARY_COMMAND_ARCHITECTURE(id_command: int, architecture: str) -> DBStatement:
        """Given the ID of a library command, add an entry to the list of its supported architectures."""

        query = """
            INSERT INTO shell_command_architecture (id_shell_command, architecture)
            VALUES (:id_command, :architecture);
        """
        params = {
            "id_command": id_command,
            "architecture": architecture,
        }
        return sqlalchemy.text(query), params


class Delete:
    """The Automation Service delete statements are defined here."""

    @staticmethod
    def LOGS(date: int) -> DBStatement:
        query = """
            DELETE FROM log
            WHERE creation_date < FROM_UNIXTIME(:date);
        """
        params = {
            "date": date,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def CAMPAIGN(id_campaign: int) -> DBStatement:
        query = """
            DELETE FROM campaign
            WHERE id=:id_campaign;
        """
        params = {
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def LIBRARY_COMMAND(id_command: int) -> DBStatement:
        query = """
            DELETE FROM shell_command
            WHERE id=:id_command
            AND deprecation_date IS NULL;
        """
        params = {
            "id_command": id_command,
        }
        return sqlalchemy.text(query), params


class Update:
    """The Automation Service update statements are defined here."""

    @staticmethod
    def SPECIALIZED_COMMAND_RELATIONSHIP(
        id_base_command: int, id_specialized_command: int, command_type: str
    ) -> DBStatement:
        """Bind a specialized command to its base command."""

        specialized_command_table: Dict[str, str] = {
            "API": "api_command_invocation_registry",
            "METRIC": "metric_command_invocation_registry",
            "SHELL": "shell_command_invocation_registry",
            "PRESET": "hypervisor_command_invocation_registry",
        }
        query = f"""
            UPDATE {specialized_command_table[command_type]}
            SET id_base_command=:id_base_command
            WHERE id=:id_specialized_command;
        """
        params = {
            "id_base_command": id_base_command,
            "id_specialized_command": id_specialized_command,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def LAST_SCHEDULE(id_campaign: int, id_schedule: int) -> DBStatement:
        """Update the last schedule id of a campaign."""

        query = """
            UPDATE campaign
            SET id_last_schedule=:id_schedule
            WHERE id=:id_campaign;
        """
        params = {
            "id_schedule": id_schedule,
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SCHEDULE_STATUS(
        *,
        id_campaign: int,
        id_schedule: int,
        time: Union[int, None],
        status: IoTCCampaignStatus,
    ) -> DBStatement:
        """
        Update the status of a campaign schedule.

        Args:
        - id_campaign
        - id_schedule
        - time: The time at which the status has been set. If None, it won't be persisted.
        - status: The new status of the schedule.
        """

        query = f"""
            UPDATE schedule
            SET status=:status
                { 
                    f", {'start_time' if status == IoTCCampaignStatus.RUNNING else 'end_time'}=FROM_UNIXTIME(:time)"
                    if time is not None
                    else ""
                }
            WHERE id_campaign=:id_campaign
                AND id=:id_schedule;
        """

        params = {
            "status": status.value,
            "time": time,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TARGET_STATUS(
        id_campaign: int,
        id_schedule: int,
        id_target: int,
        time: int,
        status: IoTCCampaignStatus,
    ) -> DBStatement:
        """Update the status of a campaign on a single target."""

        query = f"""
            UPDATE target_campaign_status
            SET status=:status,
                {"start_time" if status == IoTCCampaignStatus.RUNNING else "end_time"}=FROM_UNIXTIME(:time)
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND id_target=:id_target;
        """
        params = {
            "status": status.value,
            "time": time,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def COMMAND_STATUS(
        id_command_result: int,
        delivery_status: IoTCCommandDeliveryStatus,
        command_result: Optional[Dict[str, Any]] = None,
    ) -> DBStatement:
        """Update the delivery status of a campaign command on a target. If command result is not None, save into the db."""

        query = """
            UPDATE command_invocation_result
            SET delivery_status=:delivery_status"""

        if command_result is not None:
            query += """
                , raw=:raw,
                execution_date=FROM_UNIXTIME(:execution_date), 
                status_code=:status_code,
                result=:result
            """

        query += """ WHERE id=:id_command_result"""

        params: Dict = {
            "delivery_status": delivery_status.value,
            "id_command_result": id_command_result,
        }

        if command_result is not None:
            # If the command output is a JSON, convert it to a string.
            result = command_result["result"]
            if result is not None and type(result) != str:
                result = json.dumps(result)
            params.update(
                {
                    "raw": json.dumps(command_result),
                    "execution_date": command_result["execution_date"],
                    "status_code": command_result["status_code"],
                    "result": result,
                }
            )

        return sqlalchemy.text(query), params

    @staticmethod
    def TASK_STATUS(
        id_task_result: int, time: int, status: IoTCTaskStatus
    ) -> DBStatement:
        """
        Update the status of a campaign task on a target.

        Args:
        - id_task_result: The task result id into the database.
        - time: Either start or end time.
        - status: The task status to be persisted.
        """

        query = f"""
            UPDATE task_invocation_result
            SET status=:status,
                {"start_time" if status == IoTCTaskStatus.RUNNING else "end_time"}=FROM_UNIXTIME(:time)
            WHERE id=:id_task_result;
        """
        params = {
            "status": status.value,
            "time": time,
            "id_task_result": id_task_result,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SCHEDULE_PROGRESS(
        id_campaign: int, id_schedule: int, n_finished_commands: int
    ) -> DBStatement:
        """
        Update schedule progress based on how many commands have finished.

        Progress is computed as follows:
        progress = old_progress + [100 * n_finished_commands / (n_targets * n_total_commands)]
        """

        query = """
            UPDATE schedule
            SET progress = 
                progress +
                (
                    SELECT 100 * :n_finished_commands / 
                    (
                        (SELECT COUNT(*) AS n_targets FROM target_campaign_status WHERE id_campaign=:id_campaign AND id_schedule=:id_schedule) 
                        * (SELECT n_total_commands FROM v_total_campaign_commands WHERE id_campaign=:id_campaign)
                    )
                )
            WHERE id_campaign=:id_campaign
                AND id=:id_schedule;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "n_finished_commands": n_finished_commands,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TARGET_PROGRESS(
        id_campaign: int, id_schedule: int, id_target: int, n_finished_commands: int
    ) -> DBStatement:
        """Update campaign execution progress on a given target."""

        query = """
            UPDATE target_campaign_status
            SET progress = 
                progress +
                (
                    SELECT 100 * (:n_finished_commands / (SELECT n_total_commands FROM v_total_campaign_commands WHERE id_campaign=:id_campaign))
                )
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND id_target=:id_target;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
            "n_finished_commands": n_finished_commands,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TASK_PROGRESS(id_task_result: int, n_finished_commands: int) -> DBStatement:
        """Update task execution progress on a given target."""

        query = """
            UPDATE task_invocation_result
            SET progress = 
                progress +
                (
                    SELECT 100 * (:n_finished_commands / (SELECT n_total_commands FROM v_total_task_result_commands WHERE id_task_result=:id_task_result))
                )
            WHERE id=:id_task_result;
        """
        params = {
            "id_task_result": id_task_result,
            "n_finished_commands": n_finished_commands,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SET_SEMAPHORE(
        id_campaign: int, id_schedule: int, semaphore: IoTCCampaignSemaphoreValue
    ) -> DBStatement:
        query = """
            UPDATE schedule
            SET campaign_semaphore=:semaphore
            WHERE id_campaign=:id_campaign
                AND id=:id_schedule;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "semaphore": semaphore.value,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def PAUSE_RUNNING_TARGETS(
        id_campaign: int, id_schedule: int, time: int
    ) -> DBStatement:
        """Given a campaign schedule, pause its running targets."""

        query_str = """
            UPDATE target_campaign_status
            SET status=:paused_status
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND status IN :pausable_statuses;
        """
        params = {
            "paused_status": IoTCCampaignStatus.PAUSED.value,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "pausable_statuses": [
                status.value for status in CAMPAIGN_PAUSABLE_STATUSES
            ],
        }
        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("pausable_statuses", expanding=True)
        )
        return query, params

    @staticmethod
    def RESUME_PAUSED_TARGETS(
        id_campaign: int, id_schedule: int, time: int
    ) -> DBStatement:
        """Given a campaign schedule, resume its paused targets."""

        query_str = """
            UPDATE target_campaign_status
            SET status=:resumed_status
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND status IN :resumable_statuses;
        """
        params = {
            "resumed_status": IoTCCampaignStatus.RUNNING.value,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "resumable_statuses": [
                status.value for status in CAMPAIGN_RESUMABLE_STATUSES
            ],
        }
        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("resumable_statuses", expanding=True)
        )
        return query, params

    @staticmethod
    def ABORT_TARGETS(id_campaign: int, id_schedule: int, end_time: int) -> DBStatement:
        """Abort the targets that are still running the campaign schedule."""

        query_str = """
            UPDATE target_campaign_status
            SET status=:aborted_status,
                end_time=FROM_UNIXTIME(:end_time)
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND status IN :abortable_statuses;
        """
        params = {
            "aborted_status": IoTCCampaignStatus.ABORTED.value,
            "end_time": end_time,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "abortable_statuses": [
                status.value for status in CAMPAIGN_ABORTABLE_STATUSES
            ],
        }
        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("abortable_statuses", expanding=True)
        )
        return query, params

    @staticmethod
    def INCREMENT_EXITED_SUBJOBS(
        id_campaign: int, id_schedule: int, amount: int
    ) -> DBStatement:
        """Given a campaign schedule, increment the number of exited subjobs by the given amount."""

        query = """
            UPDATE schedule
            SET n_exited_subjobs=n_exited_subjobs+:amount
            WHERE id_campaign=:id_campaign
                AND id=:id_schedule;
        """
        params = {
            "amount": amount,
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def SET_SCHEDULED_STATUS(
        id_campaign: int, schedule_status: IoTCCampaignSchedule
    ) -> DBStatement:
        """Given a campaign, set its schedule to the provided status."""

        query = """
            UPDATE campaign
            SET scheduled=:schedule_status
            WHERE id=:id_campaign;
        """
        params = {
            "schedule_status": schedule_status.value,
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def DEPRECATE_LIBRARY_COMMAND(id_command: int) -> DBStatement:
        """Deprecate (ie. hide from the library and make it unusable) a command that has been replaced by a new version."""

        query = """
            UPDATE shell_command
            SET deprecation_date=CURRENT_TIMESTAMP
            WHERE id=:id_command
                AND deprecation_date IS NULL;
        """
        params = {
            "id_command": id_command,
        }
        return sqlalchemy.text(query), params


class Select:
    """The Automation Service select statements are defined here."""

    @staticmethod
    def COUNT_ALL_CAMPAIGNS(
        only_scheduled: bool, statuses: List[IoTCCampaignStatus]
    ) -> DBStatement:
        """Total number of campaigns according to the specified filters."""

        query_str = f"""
            SELECT COUNT(*) AS total 
            FROM campaign LEFT JOIN schedule
            ON campaign.id_last_schedule=schedule.id
            WHERE (schedule.status IN :statuses {"OR campaign.id_last_schedule IS NULL" if IoTCCampaignStatus.NEVER_RUN in statuses else ""}) 
                {f" AND campaign.scheduled=1" if only_scheduled else ""}
        """
        params = {
            "never_run_status": IoTCCampaignStatus.NEVER_RUN.value,
            "statuses": [status.value for status in statuses],
        }

        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("statuses", expanding=True)
        )

        return query, params

    @staticmethod
    def GET_CAMPAIGNS(
        only_scheduled: bool,
        statuses: List[IoTCCampaignStatus],
        limit: int,
        offset: int,
    ) -> DBStatement:
        """Status of the last schedule of all campaigns ("all" with respect to the specified filters)."""

        query_str = f"""
            SELECT campaign.id AS id_campaign,
                campaign.id_last_schedule AS last_schedule,
                campaign.name AS name,
                campaign.description AS description,
                campaign.id_component_type AS id_component_type,
                campaign.targets AS targets,
                campaign.scheduled AS scheduled,
                campaign.suspendable AS suspendable,
                campaign.abortable AS abortable,
                campaign.schedule AS schedule,
                campaign.n_subjobs AS n_subjobs,
                campaign.definition AS definition,
                campaign.on_error AS on_error,
                IFNULL(ROUND(schedule.progress, 1), 0) AS progress,
                UNIX_TIMESTAMP(schedule.start_time) AS start_time,
                UNIX_TIMESTAMP(schedule.end_time) AS end_time,
                IFNULL(schedule.n_exited_subjobs, 0) AS n_exited_subjobs,
                IFNULL(schedule.status, :never_run_status) AS status
            FROM campaign LEFT JOIN schedule
                ON campaign.id_last_schedule=schedule.id
            WHERE (schedule.status IN :statuses {"OR campaign.id_last_schedule IS NULL" if IoTCCampaignStatus.NEVER_RUN in statuses else ""}) 
                {f" AND campaign.scheduled=1" if only_scheduled else ""} 
            ORDER BY campaign.id ASC
            LIMIT :limit
            OFFSET :offset;
        """

        params = {
            "never_run_status": IoTCCampaignStatus.NEVER_RUN.value,
            "statuses": [status.value for status in statuses],
            "limit": limit,
            "offset": offset,
        }

        # Allow to bind a list of statuses (needed for the WHERE IN clause). Otherwise mysql raises the error: "Python type tuple cannot be converted"
        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("statuses", expanding=True)
        )

        return query, params

    @staticmethod
    def GET_CAMPAIGN(id_campaign: int) -> DBStatement:
        query = f"""
            SELECT
                campaign.id AS id_campaign,
                campaign.id_last_schedule AS last_schedule,
                campaign.id_component_type AS id_component_type,
                campaign.name AS name,
                campaign.description AS description,
                campaign.targets AS targets,
                campaign.scheduled AS scheduled,
                campaign.suspendable AS suspendable,
                campaign.abortable AS abortable,
                campaign.schedule AS schedule,
                campaign.n_subjobs AS n_subjobs,
                campaign.definition AS definition,
                campaign.on_error AS on_error
            FROM campaign
            WHERE campaign.id=:id_campaign;
        """
        params = {
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def COUNT_ALL_SCHEDULES(id_campaign: int) -> DBStatement:
        """Count the total number of schedules for a given campaign."""

        query = """
            SELECT COUNT(*) AS total
            FROM schedule
            WHERE schedule.id_campaign=:id_campaign;
        """
        params = {
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_SCHEDULES(id_campaign: int, limit: int, offset: int) -> DBStatement:
        """The status of the schedules for a given campaign."""

        query = """
            SELECT
                schedule.id_campaign AS id_campaign,
                schedule.id AS id_schedule,
                ROUND(schedule.progress, 1) AS progress,
                schedule.status AS status,
                schedule.n_exited_subjobs AS n_exited_subjobs,
                UNIX_TIMESTAMP(schedule.start_time) AS start_time,
                UNIX_TIMESTAMP(schedule.end_time) AS end_time
            FROM schedule
            WHERE schedule.id_campaign=:id_campaign
            ORDER BY schedule.id ASC
            LIMIT :limit 
            OFFSET :offset;
        """
        params = {
            "id_campaign": id_campaign,
            "limit": limit,
            "offset": offset,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_SCHEDULE(id_campaign: int, id_schedule: int) -> DBStatement:
        """The status of a specific campaign schedule."""

        query = """
            SELECT
                schedule.id_campaign AS id_campaign,
                schedule.id AS id_schedule,
                ROUND(schedule.progress, 1) AS progress,
                schedule.status AS status,
                schedule.n_exited_subjobs AS n_exited_subjobs,
                UNIX_TIMESTAMP(schedule.start_time) AS start_time,
                UNIX_TIMESTAMP(schedule.end_time) AS end_time
            FROM schedule
            WHERE schedule.id_campaign=:id_campaign
                AND schedule.id=:id_schedule;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def COUNT_ALL_TARGETS(id_campaign: int) -> DBStatement:
        """The total number of targets in a campaign."""

        query = """
            SELECT COUNT(DISTINCT id_target) AS total
            FROM target_campaign_status
            WHERE id_campaign=:id_campaign;
        """
        params = {
            "id_campaign": id_campaign,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_TARGET(id_campaign: int, id_schedule: int, id_target: int) -> DBStatement:
        """The status of a campaign schedule on a target."""
        query = """
            SELECT 
                id_target, 
                id_subjob, 
                name,
                ROUND(progress, 1) AS progress, 
                status, 
                UNIX_TIMESTAMP(start_time) AS start_time, 
                UNIX_TIMESTAMP(end_time) AS end_time
            FROM target_campaign_status
                WHERE id_campaign=:id_campaign
                    AND id_schedule=:id_schedule
                    AND id_target=:id_target;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_TARGETS(
        id_campaign: int, id_schedule: int, limit: int, offset: int
    ) -> DBStatement:
        """The status of the targets in a given campaign schedule."""

        query = """
            SELECT 
                DISTINCT id_target, 
                id_subjob, 
                ROUND(progress, 1) AS progress, 
                status, 
                UNIX_TIMESTAMP(start_time) AS start_time, 
                UNIX_TIMESTAMP(end_time) AS end_time
            FROM target_campaign_status
            WHERE id_campaign=:id_campaign 
                AND id_schedule=:id_schedule
            LIMIT :limit
            OFFSET :offset;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "limit": limit,
            "offset": offset,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_TASKS(id_campaign: int, id_schedule: int, id_target: int) -> DBStatement:
        """The execution status of all the tasks on a given target for a given campaign schedule."""
        query = """
            SELECT
                task_registry.id AS id_task_registry,
                task_registry.name AS name, 
                task_registry.description AS description, 
                task_registry.conditions AS conditions,
                task_registry.on_error_policy AS on_error_policy,
                task_result.id AS id_task_result, 
                ROUND(task_result.progress, 1) AS progress,
                task_result.status AS status,
                UNIX_TIMESTAMP(task_result.start_time) AS start_time,
                UNIX_TIMESTAMP(task_result.end_time) AS end_time
            FROM task_invocation_registry task_registry 
                JOIN task_invocation_result task_result 
                ON task_registry.id=task_result.id_task
            WHERE task_result.id_campaign=:id_campaign 
                AND task_result.id_schedule=:id_schedule 
                AND task_result.id_target=:id_target;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_COMMANDS(
        id_campaign: int, id_schedule: int, id_target: int, id_task_registry: int
    ) -> DBStatement:
        """The execution status of all the commands inside a task, for a given campaign schedule on a target."""
        query = """
            SELECT 
                cmd_registry.type AS type,
                cmd_registry.timeout AS timeout,
                cmd_registry.retries AS retries,
                cmd_registry.name AS name,
                cmd_registry.params AS params,
                cmd_registry.causes_reboot AS causes_reboot,
                cmd_registry.data_type AS data_type,
                cmd_registry.date_format AS date_format,
                cmd_registry.refresh_interval AS refresh_interval,
                cmd_registry.raw_definition AS raw_definition,
                UNIX_TIMESTAMP(cmd_result.execution_date) AS execution_date,
                cmd_result.result AS result, 
                cmd_result.status_code AS status_code,
                cmd_result.raw AS raw_result
            FROM v_command_invocation_registry cmd_registry
                JOIN command_invocation_result cmd_result
                    ON cmd_registry.id_base_command=cmd_result.id_command
            WHERE cmd_result.id_campaign=:id_campaign
                AND cmd_result.id_schedule=:id_schedule
                AND cmd_result.id_target=:id_target
                AND cmd_result.id_task=:id_task_registry;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
            "id_task_registry": id_task_registry,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_SEMAPHORE(id_campaign: int, id_schedule: int) -> DBStatement:
        query = """
            SELECT campaign_semaphore AS semaphore
            FROM schedule
            WHERE id_campaign=:id_campaign
                AND id=:id_schedule;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def TASK_RESULT_REGISTRY(
        id_campaign: int, id_schedule: int, id_target: int
    ) -> DBStatement:
        query = """
            SELECT id_task_result, id AS id_command_result
            FROM command_invocation_result
            WHERE id_campaign=:id_campaign
                AND id_schedule=:id_schedule
                AND id_target=:id_target
            ORDER BY id_task_result, id;
        """
        params = {
            "id_campaign": id_campaign,
            "id_schedule": id_schedule,
            "id_target": id_target,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def COUNT_ALL_LIBRARY_COMMANDS(
        *,
        command_type: Union[str, None],
        interpreters: List[str],
        architectures: List[str],
    ) -> DBStatement:
        """
        Count all commands in the library according to the required filters.

        Args:
        - command_type: Filter commands by type. If None, the filter is not applied.
        """

        query_str = f"""
            SELECT COUNT(DISTINCT cmd.id) as total
            FROM shell_command cmd 
                JOIN shell_command_architecture arch
                    ON cmd.id=arch.id_shell_command
            WHERE
                cmd.deprecation_date IS NULL
                {" AND cmd.interpreter IN :interpreters" if len(interpreters) > 0 else ""}
                {" AND arch.architecture IN :architectures" if len(architectures) > 0 else ""}
        """

        if command_type == "METRIC":
            query_str += " AND cmd.is_metric=1"
        elif command_type == "SHELL":
            query_str += " AND cmd.is_metric=0"

        query_str += ";"
        query = sqlalchemy.text(query_str)

        params = {}
        if len(interpreters) > 0:
            query.bindparams(
                sqlalchemy.bindparam("interpreters", expanding=True),
            )
            params.update({"interpreters": interpreters})
        if len(architectures) > 0:
            query.bindparams(
                sqlalchemy.bindparam("architectures", expanding=True),
            )
            params.update({"architectures": architectures})
        return query, params

    @staticmethod
    def GET_LIBRARY_COMMANDS(
        *,
        command_type: Union[str, None],
        interpreters: List[str],
        architectures: List[str],
        limit: int,
        offset: int,
    ) -> DBStatement:
        """
        Retrieve a paginated list of library commands.

        Args:
        - command_type: Filter commands by type. If None, the filter is not applied.
        """

        query_str = f"""
            SELECT 
                DISTINCT cmd.id AS id,
                cmd.name AS name,
                cmd.preview AS command,
                cmd.interpreter AS interpreter,
                (CASE
                    WHEN is_metric=1 THEN 'METRIC'
                    ELSE 'SHELL'
                END) AS type
            FROM shell_command cmd 
                JOIN shell_command_architecture arch
                    ON cmd.id=arch.id_shell_command
            WHERE
                cmd.deprecation_date IS NULL
                {" AND cmd.interpreter IN :interpreters" if len(interpreters) > 0 else ""}
                {" AND arch.architecture IN :architectures" if len(architectures) > 0 else ""}
        """

        if command_type == "METRIC":
            query_str += " AND cmd.is_metric=1"
        elif command_type == "SHELL":
            query_str += " AND cmd.is_metric=0"

        query_str += """
            ORDER BY cmd.id
            LIMIT :limit
            OFFSET :offset;
        """
        query = sqlalchemy.text(query_str)

        params: Dict = {
            "limit": limit,
            "offset": offset,
        }
        if len(interpreters) > 0:
            query.bindparams(
                sqlalchemy.bindparam("interpreters", expanding=True),
            )
            params.update({"interpreters": interpreters})
        if len(architectures) > 0:
            query.bindparams(
                sqlalchemy.bindparam("architectures", expanding=True),
            )
            params.update({"architectures": architectures})
        return query, params

    @staticmethod
    def GET_LIBRARY_COMMAND_BY_NAME(name: str) -> DBStatement:
        """Retrieve a library command by its name."""

        query = """
            SELECT 
                id,
                name,
                preview AS command,
                interpreter,
                (CASE
                    WHEN is_metric=1 THEN 'METRIC'
                    ELSE 'SHELL'
                END) AS type
            FROM shell_command
            WHERE name=:name
                AND deprecation_date IS NULL;
        """
        params = {
            "name": name,
        }
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_LIBRARY_COMMANDS_ARCHITECTURES(identifiers: List[int]) -> DBStatement:
        """
        A table with two columns:
        - Command id
        - Command architecture

        Example result:
        +------------+------------------+
        | id_command | architecture     |
        +------------+------------------+
        |         13 | p37-linux-x86_64 |
        |         14 | p37-linux-x86_64 |
        |         16 | p37-linux-armv6  |
        |         16 | p37-linux-armv7  |
        |         16 | p37-linux-x86_64 |
        +------------+------------------+
        """
        query_str = f"""
            SELECT id_shell_command AS id_command, architecture
            FROM shell_command_architecture
            WHERE id_shell_command IN :identifiers;
        """
        query = sqlalchemy.text(query_str).bindparams(
            sqlalchemy.bindparam("identifiers", expanding=True)
        )
        params = {
            "identifiers": identifiers,
        }
        return query, params

    @staticmethod
    def COUNT_ALL_LOGS(level: LogLevel) -> DBStatement:
        query = f"""
            SELECT COUNT(*) as total 
            FROM log 
            {"WHERE level=:level;" if level != level.ALL else ""}
        """
        if level != level.ALL:
            params = {
                "level": level.value,
            }
        else:
            params = {}
        return sqlalchemy.text(query), params

    @staticmethod
    def GET_LOGS(level: LogLevel, limit: int, offset: int) -> DBStatement:
        query = f"""
            SELECT 
                channel,
                level,
                message,
                context,
                extra,
                UNIX_TIMESTAMP(creation_date) AS creation_date
            FROM log
            {"WHERE level=:level" if level != level.ALL else ""}
            ORDER BY creation_date ASC
            LIMIT :limit
            OFFSET :offset;
        """
        params: Dict = {
            "limit": limit,
            "offset": offset,
        }
        if level != level.ALL:
            params.update({"level": level.value})
        return sqlalchemy.text(query), params
