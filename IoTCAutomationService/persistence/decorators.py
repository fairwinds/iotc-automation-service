import functools
import json
from typing import Any, Callable, Dict, List, Tuple, Union


def generic_deserializer(decorator_name: str, parser: Callable[[Dict], Dict]):
    """Boilerplate for concrete decorators."""

    def decorator(func: Callable[..., Union[Dict, List[Dict], Tuple[int, List[Dict]]]]):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            log_suffix = (
                f"Decorated function name: {func.__name__}. Received data: {result}"
            )

            if type(result) == dict:
                result = parser(result)
            elif type(result) == tuple:
                _, results = result
                for element in results:
                    element = parser(element)
            elif type(result) == list:
                for element in result:
                    element = parser(element)
            else:
                raise Exception(
                    f"{decorator_name} has been called on function that produces an invalid result of type {type(result)}. Allowed types are: [list, tuple, dict]. {log_suffix}"
                )
            return result

        return wrapper

    return decorator


def deserialize_json_columns(columns: List[Dict[str, Any]]):
    """
    Decorator that can be applied to functions that fetch single/many results from database.
    Try to JSON parse the columns specified in column_names. Return a JSON-parsed version of the result.

    Args:
        - columns: A list of dictionary. Each element must have the following keys:
            - "name": str. The name of the column to be parsed.
            - "allow_null": bool. Whether to raise an Exception if the column value is None.
            - "default": Any. Evaluated only if "allow_null" is True. Substitute the null value with the supplied default value.
    """

    def parse_columns(element: Dict) -> Dict:
        for column in columns:
            column_name: str = column["name"]
            allow_null: bool = column["allow_null"]
            try:
                element[column_name] = json.loads(element[column_name])
            except json.JSONDecodeError:
                # The column does not contain a JSON. Keep the column as a string.
                pass
            except TypeError as err:
                if (element[column_name] is None) and (allow_null == True):
                    if "default" not in column:
                        raise Exception(
                            f"'{column_name}' has been specified with 'allow_null' set to True but a default value has not been provided"
                        )
                    element[column_name] = column["default"]
                else:
                    raise err
        return element

    return generic_deserializer(deserialize_json_columns.__name__, parse_columns)


def boolify_columns(column_names: List[str]):
    """
    Decorator that can be applied to functions that fetch single/many results from database.
    Convert the required columns to boolean values.
    This is required for MySQL backend as it stores BOOL columns as bits.
    Note: value 0 is converted to False. Other values are converted to True.

    Args:
        - column_names: Columns that should be casted to boolean.
    """

    def parse_columns(element: Dict) -> Dict:
        for column in column_names:
            element[column] = bool(element[column])
        return element

    return generic_deserializer(boolify_columns.__name__, parse_columns)
