from typing import Dict, Any, Iterable, List, Tuple, Union
import sqlalchemy

from IoTCAutomationService.common.enums import (
    IoTCCampaignSchedule,
    IoTCCampaignSemaphoreValue,
    IoTCCampaignStatus,
    IoTCCommandDeliveryStatus,
    IoTCTaskStatus,
)
from IoTCAutomationService.common.types import IoTCTargetArchitecture, TaskIdRegistry
from IoTCAutomationService.persistence.common import (
    CommandDefinitionError,
    EntityIdNotFound,
    sql_offset,
)
from IoTCAutomationService.persistence.decorators import (
    boolify_columns,
    deserialize_json_columns,
)
from IoTCAutomationService.persistence.log import IoTCServiceLogger
from IoTCAutomationService.persistence.statements import Delete, Insert, Select, Update


class IoTCCampaignRepository:
    """
    Interface to provide easy access to campaign entities stored inside a database.
    """

    """
    Every method can throw an Exception if an error occurred while accessing the DB.
    Exceptions are thrown, for example, if no result is found for a given identifier.
    """

    def __init__(self, db_engine: sqlalchemy.engine.Engine) -> None:
        self._engine = db_engine
        self._log_prefix = "[CAMPAIGN PERSISTENCE] "

    def add_campaign(
        self,
        campaign_def: Dict[str, Any],
        targets: List[IoTCTargetArchitecture],
        n_subjobs: int,
    ) -> Tuple[int, TaskIdRegistry]:
        """
        Given a campaign definition, persist it into the database. This includes the following entities:
        - Campaign
        - All tasks
        - For each task, all commands

        Args:
            - campaign_def: Campaign JSON definition as a dict.
            - targets: List of campaign targets.
            - n_subjobs: The actual number of subjobs used to execute the campaign.

        Returns:
            A tuple in which:
            - The first element is the id of the campaign.
            - The second element is the task registry of the campaign.
        """
        task_registry: TaskIdRegistry = {}

        with self._engine.begin() as tx:
            # insert campaign
            ids_targets = [target.id_target for target in targets]
            cursor = tx.execute(*Insert.CAMPAIGN(campaign_def, ids_targets, n_subjobs))
            id_campaign = int(cursor.lastrowid)

            # insert campaign architectures
            architectures = set([target.architecture for target in targets])
            for architecture in architectures:
                tx.execute(*Insert.CAMPAIGN_ARCHITECTURE(id_campaign, architecture))

            # insert tasks
            for index_task, task in enumerate(campaign_def["tasks"]):
                cursor = tx.execute(*Insert.TASK(id_campaign, task))

                # insert commands
                id_task = int(cursor.lastrowid)
                ids_commands: List[int] = []
                for command in task["commands"]:
                    try:
                        # Insert specialized command
                        command_type = command["type"]
                        cursor = tx.execute(
                            *Insert.SPECIALIZED_COMMAND(command_type, command)
                        )
                    except sqlalchemy.exc.IntegrityError as err:
                        IoTCServiceLogger.get().error(
                            f"{self._log_prefix}. Campaign ['{campaign_def['name']}']. Unable to create the campaign due to a command integrity error: {err}"
                        )
                        raise CommandDefinitionError(
                            f"Error found in task {index_task}. Cannot create campaign with command '{command['name']}' of type '{command['type']}'. Check that the command has an exact correspondence inside the Automation Service library"
                        )

                    # Insert base command
                    id_specialized_command = int(cursor.lastrowid)
                    cursor = tx.execute(
                        *Insert.BASE_COMMAND(
                            id_campaign,
                            id_task,
                            id_specialized_command,
                            command_type,
                            command,
                        )
                    )
                    id_base_command = int(cursor.lastrowid)
                    tx.execute(
                        *Update.SPECIALIZED_COMMAND_RELATIONSHIP(
                            id_base_command, id_specialized_command, command_type
                        )
                    )
                    ids_commands.append(id_base_command)
                task_registry.update({id_task: ids_commands})

        IoTCServiceLogger.get().info(
            f"{self._log_prefix} Campaign {id_campaign} definition persisted successfully"
        )
        return id_campaign, task_registry

    def remove_campaign(self, id_campaign: int) -> int:
        """
        Given a campaign ID, remove all its associated data.

        Returns:
        - The number of removed campaign-related entities (ie. schedules, ...).
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Delete.CAMPAIGN(id_campaign))
            if cursor.rowcount == 0:
                raise EntityIdNotFound()
            return cursor.rowcount

    @deserialize_json_columns(
        [
            {"name": "definition", "allow_null": False},
            {"name": "targets", "allow_null": False},
            {"name": "schedule", "allow_null": True, "default": None},
        ]
    )
    @boolify_columns(["suspendable", "abortable"])
    def get_all_campaigns_details(
        self,
        only_scheduled: bool,
        statuses: List[IoTCCampaignStatus],
        page: int,
        results_per_page: int,
    ) -> Tuple[int, List[Dict]]:
        """
        Return a tuple in which:
        - The first element is the number of total campaigns in the database
        - The second element is a list of paginated campaigns, along with the runtime data of their last schedule (if there's any)

        Args:
            - only_scheduled: Whether only scheduled campaigns should be returned.
            - statuses: Campaign status filter. If empty, the filter won't be applied.
        """

        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.COUNT_ALL_CAMPAIGNS(only_scheduled, statuses))
            total = int(cursor.first().total)  # type: ignore

            if total == 0:
                campaigns = []
            else:
                limit = results_per_page
                offset = sql_offset(page, results_per_page)
                cursor = tx.execute(
                    *Select.GET_CAMPAIGNS(only_scheduled, statuses, limit, offset)
                )
                campaigns_rows = cursor.fetchall()
                campaigns = [dict(campaign) for campaign in campaigns_rows]
            return total, campaigns

    def count_campaigns_by_status(self) -> Dict[IoTCCampaignStatus, int]:
        """For each type of status, return the number of campaigns whose last schedule has that status."""
        # with self._engine.begin() as tx:
        raise NotImplementedError()

    @deserialize_json_columns(
        [
            {"name": "definition", "allow_null": False},
            {"name": "targets", "allow_null": False},
        ]
    )
    @boolify_columns(["suspendable", "abortable"])
    def get_campaign(self, id_campaign: int) -> Dict:
        """Given a campaign id, return its associated data."""

        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_CAMPAIGN(id_campaign))
            row = cursor.first()
            if (row is None) or (cursor.rowcount != 1):
                raise EntityIdNotFound()
            campaign = row._asdict()
            return campaign

    def get_semaphore_status(
        self, id_campaign: int, id_schedule: int
    ) -> IoTCCampaignSemaphoreValue:
        """Given a campaign schedule, returns the status of its controlling semaphore."""

        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_SEMAPHORE(id_campaign, id_schedule))
            row = cursor.first()
            if (row is None) or (cursor.rowcount != 1):
                raise EntityIdNotFound()
            semaphore = IoTCCampaignSemaphoreValue(row.semaphore)
            return semaphore

    def set_semaphore_status(
        self, id_campaign: int, id_schedule: int, semaphore: IoTCCampaignSemaphoreValue
    ):
        """Given a campaign schedule, set the status of its controlling semaphore."""

        with self._engine.begin() as tx:
            tx.execute(*Update.SET_SEMAPHORE(id_campaign, id_schedule, semaphore))

    def get_campaign_targets(self, id_campaign: int) -> List[int]:
        """
        Given a campaign, returns the list of target ids.
        """
        raise NotImplementedError()

    def add_schedule(
        self,
        campaign_def: Dict[str, Any],
        targets: List[IoTCTargetArchitecture],
        target_to_subjob: Dict[int, int],
        id_campaign: int,
        task_registry: Dict[int, List[int]],
        start_time: int,
    ) -> int:
        """Persist a new schedule given its campaign ID.

        Args:
            - campaign_def: Campaign JSON definition as a dict.
            - targets: List of campaign targets.
            - target_to_subjob: Dict that associates target id with its subjob id.
            - id_campaign: The ID of the campaign the schedule refers to.
            - task_registry: Dict that associates task ids to command ids. Needed to insert task result entities.
            - start_time: The linux timestamp at which the schedule has started.

        Returns:
            int: The ID of the new schedule.
        """

        with self._engine.begin() as tx:
            # Insert schedule
            cursor = tx.execute(*Insert.SCHEDULE(id_campaign, start_time))
            id_schedule = int(cursor.lastrowid)

            # Update last schedule id
            tx.execute(*Update.LAST_SCHEDULE(id_campaign, id_schedule))

            for target in targets:
                # Insert target campaign status
                id_target = target.id_target
                id_subjob = target_to_subjob[id_target]
                tx.execute(
                    *Insert.TARGET_STATUS(
                        id_campaign,
                        id_schedule,
                        id_subjob,
                        target,
                    )
                )

                for id_task_registry in task_registry.keys():
                    # Insert task invocation result
                    cursor = tx.execute(
                        *Insert.TASK_RESULT(
                            id_campaign,
                            id_schedule,
                            id_task_registry,
                            id_target,
                            id_subjob,
                        )
                    )
                    id_task_result = int(cursor.lastrowid)

                    command_registry = task_registry[id_task_registry]
                    for id_command_registry in command_registry:
                        # Insert command invocation result
                        cursor = tx.execute(
                            *Insert.COMMAND_RESULT(
                                id_campaign,
                                id_schedule,
                                id_task_registry,
                                id_task_result,
                                id_command_registry,
                                id_target,
                                id_subjob,
                            )
                        )

        IoTCServiceLogger.get().info(
            f"{self._log_prefix} Schedule {id_schedule} of campaign {id_campaign} persisted its intial state successfully"
        )
        return id_schedule

    def set_status_started(
        self,
        *,
        start_time: int,
        id_campaign: int,
        id_schedule: int,
        id_target: Union[int, None] = None,
        id_task: Union[int, None] = None,
        id_command: Union[int, None] = None,
    ):
        """Given a campaign entity, set its status to started.
        If id_task is None, entity=target.
        If id_target is None, entity=schedule.
        And so on...

        Args:
            id_campaign (int): The ID of the campaign
            id_schedule (int): The ID of the campaign schedule
            id_target (Union[int, None], optional): The ID of the target. Defaults to None.
            id_task (Union[int, None], optional): The ID of the task on the target. Defaults to None.
            id_command (Union[int, None], optional): The ID of the command on the target. Defaults to None.

        Example usage:
        >>> persistence = IoTCCampaignRepository(...)
        persistence.set_status_started(id_campaign=1, id_schedule=2, id_target=927, start_time=round(time.time())) # Set status of target 927 on campaign 1, schedule 2.
        persistence.set_status_started(id_campaign=1, id_schedule=2, id_target=927, id_task=1, start_time=round(time.time())) # Set status of task 1 on target 927, campaign 1, schedule 2.
        """
        if (
            (id_command is not None)
            and (id_task is not None)
            and (id_target is not None)
        ):
            # Update command status.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.COMMAND_STATUS(
                        id_command, IoTCCommandDeliveryStatus.REQUEST_SENT
                    )
                )
        elif (id_task is not None) and (id_target is not None):
            # Update task status.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.TASK_STATUS(id_task, start_time, IoTCTaskStatus.RUNNING)
                )
        elif id_target is not None:
            # Update target status.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.TARGET_STATUS(
                        id_campaign,
                        id_schedule,
                        id_target,
                        start_time,
                        IoTCCampaignStatus.RUNNING,
                    )
                )
        else:
            raise NotImplementedError()

    def set_status_finished(
        self,
        *,
        id_campaign: int,
        id_schedule: int,
        id_target: Union[int, None] = None,
        id_task: Union[int, None] = None,
        id_command: Union[int, None] = None,
        status: Union[IoTCTaskStatus, IoTCCampaignStatus, None] = None,
        command_result: Union[Dict[str, Any], None] = None,
        end_time: int,
    ):
        if (
            (id_command is not None)
            and (id_task is not None)
            and (id_target is not None)
        ):
            # Update command status and update campaign progress. Use different transactions so that if set_progress fails, at least the command result is persisted.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.COMMAND_STATUS(
                        id_command,
                        IoTCCommandDeliveryStatus.RESPONSE_RECEIVED,
                        command_result,
                    )
                )
            self._set_progress(
                id_campaign=id_campaign,
                id_schedule=id_schedule,
                id_target=id_target,
                id_task=id_task,
                n_finished_commands=1,
            )
        elif (
            (id_task is not None)
            and (id_target is not None)
            and (IoTCTaskStatus == type(status))
        ):
            # Update task status.
            with self._engine.begin() as tx:
                tx.execute(*Update.TASK_STATUS(id_task, end_time, status))
        elif (id_target is not None) and (IoTCCampaignStatus == type(status)):
            # Update target status.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.TARGET_STATUS(
                        id_campaign, id_schedule, id_target, end_time, status
                    )
                )
        elif IoTCCampaignStatus == type(status):
            # Update schedule status.
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.SCHEDULE_STATUS(
                        id_campaign=id_campaign,
                        id_schedule=id_schedule,
                        time=end_time,
                        status=status,
                    )
                )

    def set_actionable_status(
        self,
        *,
        id_campaign: int,
        id_schedule: int,
        time: int,
        status: IoTCCampaignStatus,
    ):
        """
        Given a campaign schedule, either set its status to:
        - Paused
        - Resumed
        - Aborted

        Args:
        - id_campaign
        - id_schedule
        - time: The time at which the action has been requested.
        - status: The requested status.
        """

        if status == IoTCCampaignStatus.PAUSED:
            """
            Set status to paused:
            - set semaphore to paused
            - set schedule to paused
            - set all running targets to paused
            """
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.SET_SEMAPHORE(
                        id_campaign, id_schedule, IoTCCampaignSemaphoreValue.PAUSE
                    )
                )
                tx.execute(
                    *Update.SCHEDULE_STATUS(
                        id_campaign=id_campaign,
                        id_schedule=id_schedule,
                        time=None,
                        status=IoTCCampaignStatus.PAUSED,
                    )
                )
                tx.execute(
                    *Update.PAUSE_RUNNING_TARGETS(id_campaign, id_schedule, time)
                )
        elif status == IoTCCampaignStatus.RUNNING:
            """
            Set status to resumed:
            - set semaphore to ok
            - set schedule to running
            - set all paused targets to running
            """
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.SET_SEMAPHORE(
                        id_campaign, id_schedule, IoTCCampaignSemaphoreValue.OK
                    )
                )
                tx.execute(
                    *Update.SCHEDULE_STATUS(
                        id_campaign=id_campaign,
                        id_schedule=id_schedule,
                        time=None,
                        status=IoTCCampaignStatus.RUNNING,
                    )
                )
                tx.execute(
                    *Update.RESUME_PAUSED_TARGETS(id_campaign, id_schedule, time)
                )
        elif status == IoTCCampaignStatus.ABORTED:
            """
            Set status to aborted:
            - set semaphore to aborted
            - set schedule to aborted
            - set all running|paused targets to aborted
            """
            with self._engine.begin() as tx:
                tx.execute(
                    *Update.SET_SEMAPHORE(
                        id_campaign, id_schedule, IoTCCampaignSemaphoreValue.ABORT
                    )
                )
                tx.execute(
                    *Update.SCHEDULE_STATUS(
                        id_campaign=id_campaign,
                        id_schedule=id_schedule,
                        time=time,
                        status=IoTCCampaignStatus.ABORTED,
                    )
                )
                tx.execute(*Update.ABORT_TARGETS(id_campaign, id_schedule, time))

    def _set_progress(
        self,
        *,
        id_campaign: int,
        id_schedule: int,
        id_target: int,
        id_task: int,
        n_finished_commands: int,
    ):
        """
        Given a campaign task, update its progress.
        The new progress is calculated as follows:

        new_progress = prev_progress + [100 * (n_finished_commands / n_entity_commands)]

        Example usage:
            >>> # ... launch a campaign with 10 commands
            # ... 3 out of 10 commands finish
            persistence.set_progress(id_campaign, id_schedule, id_target, id_task, n_finished_commands=3)
            # now the schedule progress is 30%

        Args:
            n_steps (int): The number of campaign atomic operations that have been executed since the last update (ie. how many commands have finished).
        """
        with self._engine.begin() as tx:
            tx.execute(
                *Update.SCHEDULE_PROGRESS(id_campaign, id_schedule, n_finished_commands)
            )
            tx.execute(
                *Update.TARGET_PROGRESS(
                    id_campaign, id_schedule, id_target, n_finished_commands
                )
            )
            tx.execute(*Update.TASK_PROGRESS(id_task, n_finished_commands))

    def set_subjob_finished(
        self,
        id_campaign: int,
        id_schedule: int,
        id_subjob: int,
    ):
        """
        Given a campaign schedule, increase the number of exited subjobs.
        """

        with self._engine.begin() as tx:
            tx.execute(
                *Update.INCREMENT_EXITED_SUBJOBS(id_campaign, id_schedule, amount=1)
            )

    def set_campaign_unscheduled(self, id_campaign: int):
        """
        Given a scheduled campaign, set it to unscheduled.
        """

        with self._engine.begin() as tx:
            tx.execute(
                *Update.SET_SCHEDULED_STATUS(id_campaign, IoTCCampaignSchedule.REMOVED)
            )

    def get_schedule_details(
        self, id_campaign: int, id_schedule: int
    ) -> Dict[str, Any]:
        """
        Given a campaign schedule, return its runtime data, including status, progress and so on.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_SCHEDULE(id_campaign, id_schedule))
            row = cursor.first()
            if (row is None) or (cursor.rowcount != 1):
                raise EntityIdNotFound()
            schedule = row._asdict()
            return schedule

    def get_all_schedules_details(
        self, id_campaign: int, page: int, results_per_page: int
    ) -> Tuple[int, List[Dict[str, Any]]]:
        """
        Given a campaign, return a tuple in which:
        - The first element is the total number of schedules associated to the campaign
        - The second element is a list of paginated schedule details for that campaign
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.COUNT_ALL_SCHEDULES(id_campaign))
            total = int(cursor.first().total)  # type: ignore

            if total == 0:
                schedules = []
            else:
                limit = results_per_page
                offset = sql_offset(page, results_per_page)
                cursor = tx.execute(*Select.GET_SCHEDULES(id_campaign, limit, offset))
                rows = cursor.fetchall()
                schedules = [dict(row) for row in rows]
            return total, schedules

    def get_target_details(
        self,
        id_campaign: int,
        id_schedule: int,
        id_target: int,
    ) -> Dict[str, Any]:
        """
        Given a campaign schedule and a target, returns the runtime data of the schedule for that target.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_TARGET(id_campaign, id_schedule, id_target))
            row = cursor.first()
            if (row is None) or (cursor.rowcount != 1):
                raise EntityIdNotFound()
            schedule = row._asdict()
            return schedule

    def get_all_targets_details(
        self, id_campaign: int, id_schedule: int, page: int, results_per_page: int
    ) -> Tuple[int, List[Dict[str, Any]]]:
        """
        Given a campaign schedule, return a tuple in which:
        - The first element is the total number of target details in the database for that campaign schedule.
        - The second element is a page of the execution status data of all of its targets.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.COUNT_ALL_TARGETS(id_campaign))
            total = int(cursor.first().total)  # type: ignore

            if total == 0:
                targets = []
            else:
                limit = results_per_page
                offset = sql_offset(page, results_per_page)
                cursor = tx.execute(
                    *Select.GET_TARGETS(id_campaign, id_schedule, limit, offset)
                )
                rows = cursor.fetchall()
                targets = [dict(row) for row in rows]
            return total, targets

    @deserialize_json_columns(
        [
            {
                "name": "conditions",
                "allow_null": False,
            },
        ]
    )
    def get_all_tasks_details(
        self, id_campaign: int, id_schedule: int, id_target: int
    ) -> List[Dict[str, Any]]:
        """
        Given a campaign schedule and a target, return the runtime data of all the tasks on that target.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_TASKS(id_campaign, id_schedule, id_target))
            rows = cursor.fetchall()
            tasks = [dict(row) for row in rows]
            return tasks

    @deserialize_json_columns(
        [
            {"name": "raw_definition", "allow_null": False},
            {"name": "raw_result", "allow_null": True, "default": {}},
        ]
    )
    def get_all_commands_details(
        self, id_campaign: int, id_schedule: int, id_target: int, id_task_registry: int
    ) -> List[Dict[str, Any]]:
        """
        Given a campaign task and a target, return the runtime data of all the commands inside that task (on that target).

        Args:
        - id_campaign: The ID of the campaign.
        - id_schedule: The ID of the schedule.
        - id_target: The ID of the target.
        - id_task_registry: The ID of the campaign task definition.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(
                *Select.GET_COMMANDS(
                    id_campaign, id_schedule, id_target, id_task_registry
                )
            )
            rows = cursor.fetchall()
            commands = [dict(row) for row in rows]
            return commands

    def get_status(
        self, id_campaign: int, id_schedule: int
    ) -> List[IoTCCampaignStatus]:
        """
        Given a campaign schedule, return a list of target statuses.
        """
        raise NotImplementedError()

    def get_task_result_registry(
        self, id_campaign: int, id_schedule: int, id_target: int
    ) -> TaskIdRegistry:
        """
        Given a schedule and a target, return all its task result ids in the database.

        Returns:
            For each task result id, the list of its command ids.
        """

        with self._engine.begin() as tx:
            cursor = tx.execute(
                *Select.TASK_RESULT_REGISTRY(id_campaign, id_schedule, id_target)
            )
            rows: Iterable[sqlalchemy.engine.Row] = cursor.fetchall()
            rows_as_dict: Iterable[Dict[str, int]] = [dict(row) for row in rows]

            # The for loop takes the following Input: [{"id_task_result": 1, "id_command_result": 1}, {"id_task_result": 1, "id_command_result": 2}, {"id_task_result": 2, "id_command_result": 3}]
            # and produces the following Output: {1: [1, 2], 2: [3]}
            task_result_registry: TaskIdRegistry = {}
            for row_dict in rows_as_dict:
                id_task = row_dict["id_task_result"]
                if id_task not in task_result_registry:
                    task_result_registry[id_task] = []
                id_command = row_dict["id_command_result"]
                task_result_registry[id_task].append(id_command)
            return task_result_registry
