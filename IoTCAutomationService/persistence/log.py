import logging
import traceback
from typing import Dict, List, Any, Tuple
import sqlalchemy

from IoTCAutomationService.common.enums import LogLevel
from IoTCAutomationService.common.types import IoTCLog
from IoTCAutomationService.persistence.common import sql_offset
from IoTCAutomationService.persistence.statements import Delete, Insert, Select


class IoTCLogRepository:
    def __init__(self, engine: sqlalchemy.engine.Engine) -> None:
        self._engine = engine

    def get_all(
        self, level: LogLevel, page: int, results_per_page: int
    ) -> Tuple[int, List[Dict[str, Any]]]:
        """
        Return a tuple in which:
        - The first element is the total number of logs in the database.
        - The second element is a list of paginated logs

        Args:
        - level: Log level filter.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.COUNT_ALL_LOGS(level))
            total = int(cursor.first().total)  # type: ignore

            if total == 0:
                logs = []
            else:
                limit = results_per_page
                offset = sql_offset(page, results_per_page)
                cursor = tx.execute(*Select.GET_LOGS(level, limit, offset))
                logs_rows = cursor.fetchall()
                logs = [dict(log) for log in logs_rows]
            return total, logs

    def add(self, log: IoTCLog) -> int:
        """
        Save the log into database.

        Returns:
            The ID of the inserted log.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Insert.LOG(log))
            id_log = cursor.lastrowid
            return id_log

    def remove(self, date: int) -> int:
        """
        Delete logs created before the specified date.

        Args:
        - date: Date expressed in unix timestamp (seconds).

        Returns:
        - The number of deleted logs.
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(*Delete.LOGS(date))
            return cursor.rowcount


class IoTCServiceLogger:
    instance = None

    def __init__(self, persistence: IoTCLogRepository):
        self._persistence = persistence
        self._logger = logging.getLogger()

    def debug(self, message):
        self._log(LogLevel.DEBUG, message)

    def info(self, message):
        self._log(LogLevel.INFO, message)

    def warning(self, message):
        self._log(LogLevel.WARNING, message)

    def error(self, message):
        self._log(LogLevel.ERROR, message)

    def _log(self, level, message):
        if level == LogLevel.ERROR:
            message += " - %s" % traceback.format_exc()

        getattr(self._logger, level.value)(message)
        log = IoTCLog(
            channel="",
            level=level,
            message=message,
            context="",
            extra="",
        )
        self._persistence.add(log)

    @classmethod
    def create(cls, persistence: IoTCLogRepository) -> None:
        cls.instance = IoTCServiceLogger(persistence)

    @classmethod
    def get(cls):
        if cls.instance is None:
            raise Exception(
                f"Cannot call {cls.__name__}.{cls.get.__name__}. You must call {cls.__name__}.{cls.create.__name__} first"
            )

        return cls.instance
