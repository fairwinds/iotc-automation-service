from typing import Iterable, List, Dict, Union, Tuple
import sqlalchemy

from IoTCAutomationService.persistence.common import (
    DuplicateEntityError,
    EntityIdNotFound,
    sql_offset,
)
from IoTCAutomationService.persistence.log import IoTCServiceLogger
from IoTCAutomationService.persistence.statements import Delete, Insert, Select, Update


class IoTCCommandRepository:
    """This class is used to add, update, retrieve and remove shell commands from the database command library."""

    def __init__(self, engine: sqlalchemy.engine.Engine) -> None:
        self._engine = engine
        self._log_prefix = "[COMMAND LIBRARY PERSISTENCE]"

    def add(self, *commands: Dict) -> List[int]:
        """
        Given one or more commands, add them to the library.
        Return the ids of the added commands.

        Args:
            - commands: The commands to be added.
        """
        ids = []
        with self._engine.begin() as tx:
            for command in commands:
                cursor = tx.execute(
                    *Select.GET_LIBRARY_COMMAND_BY_NAME(command["name"])
                )
                if cursor.rowcount != 0:
                    raise DuplicateEntityError(
                        f"Cannot create command '{command['name']}' of type '{command['type']}' because there is already a command with the same name in the Automation Service library"
                    )
                cursor = tx.execute(
                    *Insert.LIBRARY_COMMAND(command, id_previous_command=None)
                )
                id_command = cursor.lastrowid
                architectures = list(command["architectures"])
                for architecture in architectures:
                    tx.execute(
                        *Insert.LIBRARY_COMMAND_ARCHITECTURE(id_command, architecture)
                    )
                ids.append(id_command)
        IoTCServiceLogger.get().info(
            f"{self._log_prefix} #{len(commands)} commands added successfully. IDs are: {ids}"
        )
        return ids

    def remove(self, *identifiers: int) -> Iterable[int]:
        """
        Given a list of command ids, remove the commands.

        Args:
            - identifiers: List of command ids.

        Returns:
            The ids of the removed commands.

        Example usage:
        >>> commands.remove(1, 2, 3) # Commands 1,2,3 will be removed
        """
        with self._engine.begin() as tx:
            for id in identifiers:
                cursor = tx.execute(*Delete.LIBRARY_COMMAND(id))
                if cursor.rowcount != 1:
                    raise EntityIdNotFound()
        IoTCServiceLogger.get().info(
            f"{self._log_prefix} #{len(identifiers)} commands removed successfully. IDs are: {identifiers}"
        )
        return identifiers

    def update(
        self,
        *commands: Tuple[int, Dict],
    ) -> List[int]:
        """
        Given a list of command identifiers along with a new command version, update the commands.

        Args:
            - commands: A list of command ids along with the new command version.

        Returns:
            The ids of the updated commands.

        Example usage:
        >>> commands.update((1, {...}), (2, {...}))
        """
        ids = []
        with self._engine.begin() as tx:
            for id_previous_command, new_command in commands:
                # deprecate old command
                cursor = tx.execute(
                    *Update.DEPRECATE_LIBRARY_COMMAND(id_previous_command)
                )
                if cursor.rowcount == 0:
                    # the given command id does not exist
                    raise EntityIdNotFound()
                # insert new command
                cursor = tx.execute(
                    *Insert.LIBRARY_COMMAND(
                        new_command, id_previous_command=id_previous_command
                    )
                )
                id_command = int(cursor.lastrowid)
                architectures = new_command["architectures"]
                # insert new command architectures
                for architecture in architectures:
                    tx.execute(
                        *Insert.LIBRARY_COMMAND_ARCHITECTURE(id_command, architecture)
                    )
                ids.append(id_command)

            IoTCServiceLogger.get().info(
                f"#{len(commands)} command(s) updated successfully. List of (id_old_command, new_command) is: {commands}. New ids are: {ids}"
            )
            return ids

    def modify(
        self, add: List[Dict], update: List[Tuple[int, Dict]], remove: List[int]
    ):
        """Perform add, update and remove in the same transaction."""

        with self._engine.begin():
            self.add(*add)
            self.update(*update)
            self.remove(*remove)

    def get_by_id(self, id: int) -> Dict:
        """Given a command id, retrieve the command."""
        raise NotImplementedError("This method is not implemented")

    def get_by_name(self, name: str) -> Dict:
        """Given a command unique name, retrieve the command."""
        with self._engine.begin() as tx:
            cursor = tx.execute(*Select.GET_LIBRARY_COMMAND_BY_NAME(name))
            row = cursor.first()
            if (row is None) or (cursor.rowcount != 1):
                raise EntityIdNotFound()
            command = row._asdict()
            command_architectures = self._get_architectures(command["id"])
            architectures = command_architectures[command["id"]]
            command.update({"architectures": architectures})
            return command

    def get_all(
        self,
        *,
        types: List[str],
        interpreters: List[str],
        architectures: List[str],
        page: int,
        results_per_page: int,
    ) -> Tuple[int, List[Dict]]:
        """
        Get all library commands according to the required filters. If any filter is empty, it won't be applied.

        Args:
        - interpreters: Filter by command interpreter (eg. BASH, Python)
        - types: Filter by command type (SHELL or METRIC)
        - architectures: Filter by command architecture

        Return a tuple in which:
        - The first element is the number of total commands in the library
        - The second element is a list of paginated commands
        """
        command_type = None
        if "METRIC" in types and "SHELL" not in types:
            command_type = "METRIC"
        elif "SHELL" in types and "METRIC" not in types:
            command_type = "SHELL"

        with self._engine.begin() as tx:
            cursor = tx.execute(
                *Select.COUNT_ALL_LIBRARY_COMMANDS(
                    command_type=command_type,
                    interpreters=interpreters,
                    architectures=architectures,
                )
            )
            total = int(cursor.first().total)  # type: ignore

            if total == 0:
                commands = []
            else:
                limit = results_per_page
                offset = sql_offset(page, results_per_page)
                cursor = tx.execute(
                    *Select.GET_LIBRARY_COMMANDS(
                        command_type=command_type,
                        interpreters=interpreters,
                        architectures=architectures,
                        limit=limit,
                        offset=offset,
                    )
                )
                rows = cursor.fetchall()
                commands = [dict(row) for row in rows]

            # Append to each command the list of supported architectures.
            ids_commands = [int(command["id"]) for command in commands]
            command_architectures = self._get_architectures(*ids_commands)
            for command in commands:
                command.update(
                    {"architectures": command_architectures.get(command["id"], [])}
                )
            return total, commands

    def _get_architectures(self, *identifiers: int) -> Dict[int, List[str]]:
        """
        Given a list of command ids, retrieve the supported architectures for each command.

        Returns:
        - A dict in which:
            - Key is command id
            - Value is the list of its supported architectures
        """
        with self._engine.begin() as tx:
            cursor = tx.execute(
                *Select.GET_LIBRARY_COMMANDS_ARCHITECTURES(list(identifiers))
            )
            rows = cursor.fetchall()
            command_architectures: Dict[int, List[str]] = {
                id_command: [] for id_command in identifiers
            }
            for row in rows:
                id_command = int(row.id_command)
                architecture = row.architecture
                command_architectures[id_command].append(architecture)

            return command_architectures
