import logging
import sys
import os
import sqlalchemy


def configure_database():
    """
    Initialize the database driver. Return the database connection provider.
    """

    logger = logging.getLogger("[CONFIGURE DATABASE]")

    def fatal_error_missing_ennvar(var_name: str) -> None:
        logger.error(
            f"Fatal error during database driver initialization: missing {var_name} environment variable. Environment variables are: {os.environ.keys()}"
        )
        sys.exit(1)

    if "MYSQL_HOST" not in os.environ.keys():
        fatal_error_missing_ennvar("MYSQL_HOST")

    if "MYSQL_PORT" not in os.environ.keys():
        fatal_error_missing_ennvar("MYSQL_PORT")

    if "MYSQL_USER" not in os.environ.keys():
        fatal_error_missing_ennvar("MYSQL_USER")

    db_url = sqlalchemy.engine.URL(
        drivername="mysql+mysqlconnector",
        host=os.environ["MYSQL_HOST"],
        port=int(os.environ["MYSQL_PORT"]),
        username=os.environ["MYSQL_USER"],
        password=os.environ["MYSQL_PASSWORD"]
        if "MYSQL_PASSWORD" in os.environ.keys()
        else "",
        database="IoTCatalystAutomationService",
    )

    # Test database reachability. If failed, consider it a fatal error.
    try:
        engine = sqlalchemy.engine.create_engine(
            url=db_url, pool_size=10, pool_pre_ping=True
        )
        with engine.connect():
            pass
    except Exception as err:
        logger.error(
            f"Fatal error connecting to the database for the first time: {err}. DB config is: {db_url}"
        )
        sys.exit(1)

    logger.info(f"Initialized MySQL Database Driver {str(engine)}")

    return engine
