class EntityIdNotFound(Exception):
    """This exception is thrown when a query tries to retrieve a database row that does not exist."""

    pass


class DuplicateEntityError(Exception):
    """This exception is thrown when a query tries to insert a row that already exists in the database."""

    pass


class CommandDefinitionError(Exception):
    """This exception is thrown when a campaign contains a command that cannot be accepted by the Automation Service."""

    pass


def sql_offset(page: int, results_per_page: int) -> int:
    """
    Given pagination options, return the value to put in a SQL "offset" clause.
    ATTENTION: Assumes that pages start from zero.
    """
    return (page - 1) * results_per_page
