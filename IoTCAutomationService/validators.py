import re
from typing import Dict, List

from IoTCAutomationService.common.enums import IoTCDataType
from IoTCAutomationService.common.errors import (
    InvalidCampaignOptionError,
    WrongRequestError,
)


class IoTCValidator:
    @staticmethod
    def validate_query_params(query_params, schema):
        res = []
        for schema_el in schema:
            key = schema_el["name"]
            if key in query_params:
                if schema_el["dtype"].value == IoTCDataType.ARRAY.value:
                    value = IoTCValidator.validate_array(
                        key, str(query_params[key]), schema_el
                    )
                    res.append(value)
                    continue
                elif schema_el["dtype"].value == IoTCDataType.BOOL.value:
                    value = str(query_params[key]).lower()
                    if value not in ["true", "false"]:
                        raise WrongRequestError(
                            "error while retrieving query parameter '%s' with value %s in request: parameter should be a boolean"
                            % (key, value)
                        )
                    value = True if value == "true" else False
                else:
                    try:
                        value = eval(schema_el["dtype"].value)(str(query_params[key]))
                    except ValueError:
                        raise WrongRequestError(
                            "error while retrieving query parameter '%s' with value %s in request: parameter should be a %s"
                            % (key, query_params[key], schema_el["dtype"].value)
                        )

                if "regex" in schema_el:
                    if not bool(re.findall(r"%s" % schema_el["regex"], str(value))):
                        raise WrongRequestError(
                            "value %s not allowed for parameter '%s' in query parameters. Allowed values should match the regex: %s"
                            % (query_params[key], key, schema_el["regex"])
                        )
                elif "interval" in schema_el:
                    if (
                        not schema_el["interval"][0]
                        <= value
                        <= schema_el["interval"][1]
                    ):
                        raise WrongRequestError(
                            "value %s not allowed for parameter '%s' in query parameters. Allowed values are in range: %s"
                            % (
                                query_params[key],
                                key,
                                ", ".join(
                                    [str(value) for value in schema_el["interval"]]
                                ),
                            )
                        )

                if (
                    "allowed_values" in schema_el
                    and value not in schema_el["allowed_values"]
                ):
                    raise WrongRequestError(
                        "value %s not allowed for parameter '%s' in query parameters. Allowed values are: %s"
                        % (
                            query_params[key],
                            key,
                            ", ".join(schema_el["allowed_values"]),
                        )
                    )
            elif "required" in schema_el and schema_el["required"] is True:
                raise WrongRequestError(
                    "Parameter '%s' is required but was not found in query parameters"
                    % key
                )
            elif "required-if" in schema_el:
                condition_var, condition_operator, condition_literal = schema_el[
                    "required-if"
                ].split("/")
                if condition_var in query_params:
                    condition_var_value = query_params[condition_var]
                    if eval(
                        "%s %s %s"
                        % (condition_var_value, condition_operator, condition_literal)
                    ):
                        raise WrongRequestError(
                            "Parameter '%s' is required when %s %s %s but was not found in query parameters"
                            % (
                                key,
                                condition_var,
                                condition_operator,
                                condition_literal,
                            )
                        )
                    else:
                        value = schema_el["default"]
                else:
                    value = schema_el["default"]
            else:
                value = schema_el["default"]

            res.append(value)

        return res

    @staticmethod
    def validate_value(key, value, valid_values):
        if value not in valid_values:
            raise InvalidCampaignOptionError(
                "invalid value '%s' for key '%s': valid values are '%s'"
                % (value, key, ("', '").join(valid_values))
            )

    @staticmethod
    def validate_array(key, value, schema_el):
        values = value.split(",")
        for val in values:
            if "allowed_values" in schema_el and val not in schema_el["allowed_values"]:
                raise WrongRequestError(
                    "value %s not allowed for parameter '%s' in query parameters. Allowed values are: %s"
                    % (val, key, ", ".join(schema_el["allowed_values"]))
                )

        return values

    @staticmethod
    def validate_uniqueness(key: str, objects: List[Dict]):
        values = [obj[key] for obj in objects]
        if len(values) > len(set(values)):
            raise WrongRequestError("Property '%s' must be unique" % (key))

    @staticmethod
    def validate_dict(input_dict, schema, origin):
        res = []
        for schema_el in schema:
            key = schema_el["name"]
            if key in input_dict:
                try:
                    value = eval(schema_el["dtype"].value)(str(input_dict[key]))
                except ValueError:
                    raise WrongRequestError(
                        "error while retrieving parameter '%s' with value %s in %s: parameter should be a %s"
                        % (key, input_dict[key], origin, schema_el["dtype"].value)
                    )

                if "interval" in schema_el:
                    if (
                        not schema_el["interval"][0]
                        <= value
                        <= schema_el["interval"][1]
                    ):
                        raise WrongRequestError(
                            "value %s not allowed for parameter '%s' in %s. Allowed values are in range: %s"
                            % (
                                input_dict[key],
                                key,
                                origin,
                                ", ".join(
                                    [str(value) for value in schema_el["interval"]]
                                ),
                            )
                        )

                if (
                    "allowed_values" in schema_el
                    and value not in schema_el["allowed_values"]
                ):
                    raise WrongRequestError(
                        "value %s not allowed for parameter '%s' in %s. Allowed values are: %s"
                        % (
                            input_dict[key],
                            key,
                            origin,
                            ", ".join(schema_el["allowed_values"]),
                        )
                    )
            elif schema_el["required"] is True:
                raise WrongRequestError(
                    "Parameter '%s' is required but was not found in %s" % (key, origin)
                )
            else:
                value = schema_el["default"]

            res.append(value)

        return res
