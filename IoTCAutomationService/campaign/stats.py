import math
from typing import List

from IoTCAutomationService.common.errors import (
    CampaignNotFoundError,
    ScheduleNotFoundError,
)
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.persistence.common import EntityIdNotFound
from IoTCAutomationService.persistence.log import IoTCServiceLogger
from IoTCAutomationService.common.enums import IoTCCampaignStatus


class IoTCCampaignStats:
    def __init__(self, persistence: IoTCCampaignRepository) -> None:
        self._persistence = persistence

    def get_all(
        self,
        only_scheduled: bool,
        statuses: List[IoTCCampaignStatus],
        page: int,
        results_per_page: int,
    ):
        try:
            total_elements, campaigns = self._persistence.get_all_campaigns_details(
                only_scheduled, statuses, page, results_per_page
            )
            results_in_page = len(campaigns)
            total_pages = math.ceil(total_elements / int(results_per_page))
            return campaigns, page, results_in_page, total_pages, total_elements
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Cannot retrieve campaign information. Error: {err}"
            )
            raise Exception(
                "Cannot retrieve campaign information. See logs for details"
            )

    def get_campaign_definition(self, id_campaign: int):
        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError(f"Campaign {id_campaign} not found")
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Cannot retrieve campaign {id_campaign} definition. Error: {err}"
            )
            raise Exception(
                f"Cannot retrieve campaign {id_campaign} definition. See logs for details"
            )

        try:
            definition = campaign["definition"]
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Cannot retrieve campaign {id_campaign} definition. 'definition' not found in the retrieved campaign information. Retrieved campaign is: {campaign}."
            )
            raise Exception(
                f"Cannot retrieve campaign {id_campaign} definition. See logs for details"
            )

        return definition

    def get_schedule(
        self, id_campaign: int, id_schedule: int, page: int, results_per_page: int
    ):
        """
        Args:
            - results_per_page: The number of targets for each page.
        """

        try:
            schedule = self._persistence.get_schedule_details(id_campaign, id_schedule)
            total_elements, targets = self._persistence.get_all_targets_details(
                id_campaign, id_schedule, page, results_per_page
            )
            for target in targets:
                id_target = int(target["id_target"])
                tasks = self.get_target_tasks(id_campaign, id_schedule, id_target)
                target.update({"tasks": tasks})

            run_stats = {
                "id_campaign": id_campaign,
                "id_schedule": id_schedule,
                "summary": schedule,
                "targets": targets,
            }
            results_in_page = len(targets)
            total_pages = math.ceil(total_elements / int(results_per_page))
            return run_stats, page, results_in_page, total_pages, total_elements
        except EntityIdNotFound as err:
            message = self._get_schedule_not_found_message(id_campaign, id_schedule)
            raise ScheduleNotFoundError(message)
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Cannot retrieve details for campaign {id_campaign}, schedule {id_schedule}. Error: {err}"
            )
            raise Exception(
                f"Cannot retrieve details for campaign {id_campaign}, schedule {id_schedule}. See logs for details"
            )

    def get_all_schedules(self, id_campaign, page: int, results_per_page: int):
        try:
            self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError(f"Campaign {id_campaign} not found")

        try:
            total_elements, summaries = self._persistence.get_all_schedules_details(
                id_campaign, page, results_per_page
            )
            results_in_page = len(summaries)
            total_pages = math.ceil(total_elements / int(results_per_page))
            return summaries, page, results_in_page, total_pages, total_elements
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Failed to retrieve schedules for campaign {id_campaign}. Error: {err}"
            )
            raise Exception(
                f"Cannot retrieve schedules for campaign {id_campaign}. See logs for details"
            )

    def _get_schedule_not_found_message(self, id_campaign: int, id_schedule: int):
        # If the campaign doesn't exist, tell it to the user
        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            return f"Campaign {id_campaign} not found"

        # If the campaign has never been scheduled, tell it to the user
        if campaign.get("last_schedule") is None:
            return f"No schedule found for campaign {id_campaign}"

        id_last_schedule = int(campaign["last_schedule"])

        # Otherwise, the provided id_schedule is not valid for this campaign. Tell to the user the last schedule id for this campaign
        return f"Schedule {id_schedule} of campaign {id_campaign} not found. Last schedule is {id_last_schedule}"

    def get_target_tasks(self, id_campaign: int, id_schedule: int, id_target):
        """Given a campaign schedule and a target, return the detail of all of its tasks (and commands)."""
        tasks = self._persistence.get_all_tasks_details(
            id_campaign, id_schedule, id_target
        )
        for task in tasks:
            id_task_registry = int(task["id_task_registry"])
            commands = self._persistence.get_all_commands_details(
                id_campaign,
                id_schedule,
                id_target,
                id_task_registry,
            )
            commands = [
                {
                    **command["raw_definition"],
                    **command["raw_result"],
                }
                for command in commands
            ]
            task.update({"commands": commands})

            # Remove properties that are "ugly" to be shown to the front-end
            task.pop("id_task_registry")
            task.pop("id_task_result")
        return tasks
