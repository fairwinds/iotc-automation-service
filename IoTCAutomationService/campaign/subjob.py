import time
import threading
from typing import Any, Dict, NamedTuple, List

from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.common.common import (
    POLLING_ON_JOB_PAUSED,
)
from IoTCAutomationService.common.enums import (
    IoTCCampaignSemaphoreValue,
    IoTCCampaignStatus,
    IoTCOnCampaignError,
    IoTCTaskStatus,
)
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.campaign.task import (
    IoTCTaskConfig,
    IoTCTaskFactory,
)
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class IoTCSubjobConfig(NamedTuple):
    id_campaign: int
    id_schedule: int
    id_subjob: int

    targets: List[int]
    """The list of target ids this subjob will operate on."""

    tasks: List[Dict]
    """Tasks to be executed for each target."""

    campaign_def: Dict[str, Any]

    target_type: str

    on_error: IoTCOnCampaignError

    persistence: IoTCCampaignRepository

    hypervisor_cache: IoTCHypervisorCache

    task_factory: IoTCTaskFactory


class IoTCSubjob(threading.Thread):
    """
    This class executes the campaign on a list of targets in a separate thread.
    """

    def __init__(self, config: IoTCSubjobConfig):
        self.id_campaign = config.id_campaign
        self.id_schedule = config.id_schedule
        self.id = config.id_subjob
        super().__init__(
            name=f"Campaign {self.id_campaign} - Schedule {self.id_schedule} - Subjob {self.id}"
        )
        self.campaign_def = config.campaign_def
        self._persistence = config.persistence
        self._config = config
        self._tasks = config.tasks
        self._hypervisor_cache = config.hypervisor_cache
        self._task_factory = config.task_factory
        self._target_type = config.target_type
        self._targets = config.targets
        self._log_prefix = f"Campaign {self.id_campaign}. Schedule {self.id_schedule}. Subjob {self.id}"

        self.n_targets_failed: int = 0
        """At the end of the subjob, contains the number of failed campaigns."""

        self.n_targets_succeeded: int = 0
        """At the end of the subjob, contains the number of succeeded campaigns."""

        IoTCServiceLogger.get().info(
            f"{self._log_prefix} initialization. It will operate on {len(self._targets)} targets."
        )

    def run(self):
        """
        For each target, execute the campaign.
        """
        for id_target in self._targets:
            try:
                # Persist target initial state
                self._persistence.set_status_started(
                    id_campaign=self.id_campaign,
                    id_schedule=self.id_schedule,
                    id_target=id_target,
                    start_time=round(time.time()),
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Target {id_target}. Failed to persist campaign start event on the target. Error: {err}"
                )
                self.n_targets_failed += 1
                continue  # go to the next target

            target_status = None
            try:
                target_status = self._execute_campaign(id_target)
                if target_status == IoTCCampaignStatus.EXITED_OK:
                    self.n_targets_succeeded += 1
                else:
                    self.n_targets_failed += 1
            except Exception as err:
                IoTCServiceLogger.get().error(
                    "Campaign %s. Schedule %s. Subjob %s. Error while executing campaign on target %s. Error: %s"
                    % (self.id_campaign, self.id_schedule, self.id, id_target, err)
                )

            if target_status is not None:
                try:
                    # Persist target final state
                    self._persistence.set_status_finished(
                        id_campaign=self.id_campaign,
                        id_schedule=self.id_schedule,
                        id_target=id_target,
                        status=target_status,
                        end_time=round(time.time()),
                    )
                except Exception as err:
                    IoTCServiceLogger.get().error(
                        f"{self._log_prefix}. Target {id_target}. Failed to persist campaign end event on the target. Error: {err}"
                    )

        IoTCServiceLogger.get().info(
            f"{self._log_prefix} exiting. #{self.n_targets_failed} Failed Targets. #{self.n_targets_succeeded} Succeeded Targets"
        )

    def _execute_campaign(self, id_target: int) -> IoTCCampaignStatus:
        """
        Start the campaign on a specific target.

        Returns:
            The execution result of the campaign on the target.
        """

        log_prefix = f"{self._log_prefix}. Target {id_target}"

        if self._hypervisor_cache.get_data(id_target) is None:
            IoTCServiceLogger.get().error(
                f"{log_prefix} does not exist in the IoTCatalyst domain. Campaign won't be executed on this target"
            )
            return IoTCCampaignStatus.EXITED_KO

        IoTCServiceLogger.get().info(
            f"{log_prefix}. Starting campaign execution on this target"
        )

        # Fetch the task, command result ids that will be used by the task objects to persist the campaign results.
        ids_task_result = self._persistence.get_task_result_registry(
            self.id_campaign, self.id_schedule, id_target
        )

        target_status = IoTCCampaignStatus.EXITED_OK

        for id_task, task_def in zip(ids_task_result.keys(), self._tasks):

            def try_get_semaphore() -> IoTCCampaignSemaphoreValue:
                """
                Try to return the schedule semaphore from the persistence layer.
                If an error occurs, return PAUSED.
                """
                try:
                    semaphore_status = self._persistence.get_semaphore_status(
                        self.id_campaign, self.id_schedule
                    )
                except Exception as err:
                    IoTCServiceLogger.get().error(
                        f"{log_prefix}. Failed to retrieve the schedule semaphore status. The schedule will be set to [{IoTCCampaignSemaphoreValue.PAUSE.value}] and another attempt will be made soon. Error: {err}"
                    )
                    semaphore_status = IoTCCampaignSemaphoreValue.PAUSE
                return semaphore_status

            semaphore_status = try_get_semaphore()
            while semaphore_status == IoTCCampaignSemaphoreValue.PAUSE:
                time.sleep(POLLING_ON_JOB_PAUSED)
                semaphore_status = try_get_semaphore()

            if semaphore_status == IoTCCampaignSemaphoreValue.OK:
                IoTCServiceLogger.get().info(
                    f"{log_prefix}. Task {id_task} [{task_def['name']}]. Launching"
                )
                on_error = self._config.on_error
                ids_commands = ids_task_result[id_task]
                task_config = IoTCTaskConfig(
                    task_def,
                    task_def["commands"],
                    ids_commands,
                    self.campaign_def,
                    self._config.on_error,
                    self.id_campaign,
                    self.id_schedule,
                    id_target,
                    id_task,
                    "",
                )
                task = self._task_factory.create(
                    task_def["commands"][0]["type"], task_config
                )
                task_result = task.execute()

                def is_last_task() -> bool:
                    """Determine whether the current task is the last task for this target."""
                    return task_def == self._tasks[-1]

                if task_result == IoTCTaskStatus.EXITED_KO:
                    if on_error == IoTCOnCampaignError.EXIT or (
                        on_error == IoTCOnCampaignError.PAUSE and is_last_task()
                    ):
                        IoTCServiceLogger.get().info(
                            f"{log_prefix}. Task {id_task} failure makes the campaign exit for this target"
                        )
                        target_status = IoTCCampaignStatus.EXITED_KO
                        break
                    elif on_error == IoTCOnCampaignError.PAUSE:
                        IoTCServiceLogger.get().info(
                            f"{log_prefix}. Task {id_task} failure will pause the whole campaign if further tasks/targets remain after this task"
                        )
                        try:
                            self._persistence.set_actionable_status(
                                id_campaign=self.id_campaign,
                                id_schedule=self.id_schedule,
                                time=round(time.time()),
                                status=IoTCCampaignStatus.PAUSED,
                            )
                        except Exception as err:
                            IoTCServiceLogger.get().error(
                                f"{log_prefix}. Failed to set the campaign semaphore to [{IoTCCampaignSemaphoreValue.PAUSE.value}]. Exiting from the campaign on this target to prevent unwanted workflows. Error: {err}"
                            )
                            target_status = IoTCCampaignStatus.EXITED_KO
                            break

            elif semaphore_status == IoTCCampaignSemaphoreValue.ABORT:
                IoTCServiceLogger.get().info(
                    f"{log_prefix}. Campaign has been aborted. Exiting"
                )
                target_status = IoTCCampaignStatus.ABORTED
                break

        return target_status
