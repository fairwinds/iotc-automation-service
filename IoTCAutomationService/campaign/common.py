from typing import Any, Protocol
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.executors.pool import ThreadPoolExecutor

from IoTCAutomationService.common.common import (
    REDIS_HOST,
    REDIS_PORT,
    SCHEDULER_WORKERS,
)


class IoTCCampaignScheduler(Protocol):
    def add_job(self, *args, **kwargs) -> Any:
        ...

    def remove_job(self, id_job: str) -> Any:
        ...


# TODO: remove this class. Initialize the job store inside configure_scheduler()
class IoTCCampaignSchedulerManager:
    instance = None

    @classmethod
    def create(cls):
        if cls.instance is None:
            redis_jobstore = RedisJobStore(host=REDIS_HOST, port=REDIS_PORT)
            jobstores = {"default": redis_jobstore}
            executors = {"default": ThreadPoolExecutor(SCHEDULER_WORKERS)}
            cls.instance = BackgroundScheduler(
                jobstores=jobstores,
                executors=executors,
                job_defaults={
                    "misfire_grace_time": None,
                    "coalesce": True,
                },
            )
            cls.instance.start()

    @classmethod
    def get(cls):
        return cls.instance


def configure_scheduler() -> IoTCCampaignScheduler:
    """Initialize the campaign scheduler and return it."""
    redis_jobstore = RedisJobStore(host=REDIS_HOST, port=REDIS_PORT)
    jobstores = {"default": redis_jobstore}
    executors = {"default": ThreadPoolExecutor(SCHEDULER_WORKERS)}
    scheduler = BackgroundScheduler(
        jobstores=jobstores,
        executors=executors,
        job_defaults={
            "misfire_grace_time": None,
            "coalesce": True,
        },
    )
    scheduler.start()
    return scheduler
