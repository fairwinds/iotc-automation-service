from datetime import datetime
import time
import threading
from typing import Any, Callable, List, Dict, Union
import math
from croniter import croniter
from apscheduler.triggers.cron import CronTrigger

from IoTCAutomationService.campaign.task import IoTCTaskFactory
from IoTCAutomationService.common.errors import (
    InvalidCampaignScheduleError,
    TooLowIntervalError,
    UnrecognizedEntityTypeError,
    WrongIntervalError,
)
from IoTCAutomationService.campaign.subjob import IoTCSubjob, IoTCSubjobConfig
from IoTCAutomationService.common.common import (
    MAX_PROCESSES_PER_CAMPAIGN,
)
from IoTCAutomationService.common.enums import (
    IoTCCampaignStatus,
    IoTCEntityCode,
    IoTCEntityType,
    IoTCOnCampaignError,
)
from IoTCAutomationService.campaign.common import IoTCCampaignScheduler
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.persistence.common import CommandDefinitionError
from IoTCAutomationService.common.types import IoTCTargetArchitecture, TaskIdRegistry
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.validators import IoTCValidator
from IoTCAutomationService.persistence.log import (
    IoTCServiceLogger,
)


class IoTCCampaignDispatcher:
    __persistence: Union[IoTCCampaignRepository, None] = None
    __task_factory: Union[IoTCTaskFactory, None] = None
    __hypervisor_cache: Union[IoTCHypervisorCache, None] = None
    """
    Only meant to be used into the function _get_persistence_for_campaign(). 
    DO NOT USE IT IN ANY OTHER WAY. 
    DELETE IT IF YOU REMOVE THAT FUNCTION.
    """

    def __init__(
        self,
        scheduler: IoTCCampaignScheduler,
        persistence: IoTCCampaignRepository,
        hypervisor_cache: IoTCHypervisorCache,
        task_factory: IoTCTaskFactory,
    ):
        self._scheduler = scheduler
        self._persistence = persistence
        IoTCCampaignDispatcher.__persistence = persistence
        IoTCCampaignDispatcher.__hypervisor_cache = hypervisor_cache
        IoTCCampaignDispatcher.__task_factory = task_factory

    def create_campaign(
        self,
        campaign_def: Dict[str, Any],
        targets: List[IoTCTargetArchitecture],
    ):
        """
        Create a campaign.

        Args:
        - campaign_def: Campaign JSON definition as dict.
        - targets: List of campaign targets.

        Returns:
        - A tuple in which:
            - The first element is the campaign ID
            - The second element is a user-friendly message about the creation result
        """

        IoTCValidator.validate_value(
            "on_error",
            campaign_def["execution"]["on_error"],
            [on_error.value for on_error in IoTCOnCampaignError],
        )

        target_type = self._extract_entity_type(
            campaign_def["target"]["idComponentType"]
        )
        targets_per_subjob = self._compute_num_of_targets_per_subjob(
            campaign_def,
            targets,
        )
        n_subjobs = self._compute_num_of_subjobs(campaign_def, targets)

        try:
            id_campaign, task_registry = self._persistence.add_campaign(
                campaign_def, targets, n_subjobs
            )
        except CommandDefinitionError as err:
            raise err
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Failed to persist campaign definition. Error: {err}. Campaign definition is: {campaign_def}"
            )
            raise Exception(
                "An error occurred while persisting the campaign information. See logs for details"
            )

        campaign_args = [
            id_campaign,
            campaign_def,
            target_type,
            targets,
            targets_per_subjob,
            n_subjobs,
            task_registry,
            IoTCCampaignDispatcher._get_persistence_for_campaign,
            IoTCCampaignDispatcher._get_hypervisor_cache_for_campaign,
            IoTCCampaignDispatcher._get_task_factory_for_campaign,
        ]
        campaign_name_for_scheduler = self._get_campaign_name_for_scheduler(
            id_campaign, campaign_def
        )
        log_message = f"Campaign {id_campaign} creation: "
        if "schedule" in campaign_def["execution"]:
            self._execute_on_cron(
                id_campaign,
                campaign_name_for_scheduler,
                campaign_args,
                campaign_def["execution"]["schedule"],
            )
            log_message += f"CRON ({campaign_def['execution']['schedule']}) "
        elif "every" in campaign_def["execution"]:
            every_interval: Dict = campaign_def["execution"]["every"]
            hours = int(every_interval["hours"]) if "hours" in every_interval else 0
            minutes = (
                int(every_interval["minutes"]) if "minutes" in every_interval else 0
            )
            self._execute_every_interval(
                id_campaign,
                campaign_name_for_scheduler,
                campaign_args,
                hours,
                minutes,
            )
            log_message += f"REPEATED ({campaign_def['execution']['every']}) "
        elif "in" in campaign_def["execution"]:
            in_interval: Dict = campaign_def["execution"]["in"]
            hours = int(in_interval["hours"]) if "hours" in in_interval else 0
            minutes = int(in_interval["minutes"]) if "minutes" in in_interval else 0
            self._execute_delayed(
                id_campaign,
                campaign_name_for_scheduler,
                campaign_args,
                hours,
                minutes,
            )
            log_message += f"DELAYED ({campaign_def['execution']['in']}) "
        else:
            self._execute_now(id_campaign, campaign_name_for_scheduler, campaign_args)
            log_message += f"SINGLE_RUN "
        log_message += f"- {n_subjobs} subjobs and {len(targets)} targets. {targets_per_subjob} targets per subjob. "
        IoTCServiceLogger.get().info(log_message)

        message = self._get_campaign_creation_message(campaign_def)
        return id_campaign, message

    def _compute_num_of_targets_per_subjob(
        self, campaign_def: Dict[str, Any], targets: List
    ) -> int:
        suggested_n_processes = self._cap_max_processes(campaign_def)
        targets_per_subjob = math.ceil(len(targets) / suggested_n_processes)
        return targets_per_subjob

    def _compute_num_of_subjobs(
        self, campaign_def: Dict[str, Any], targets: List
    ) -> int:
        targets_per_subjob = self._compute_num_of_targets_per_subjob(
            campaign_def, targets
        )
        return math.ceil(len(targets) / targets_per_subjob)

    def _get_campaign_creation_message(self, campaign_def: Dict[str, Any]) -> str:
        n_processes = campaign_def["execution"]["processes"]
        if n_processes > MAX_PROCESSES_PER_CAMPAIGN:
            return f"Number of processes {n_processes} is too high: {MAX_PROCESSES_PER_CAMPAIGN} processes will be suggested to the Automation Service"
        else:
            return ""

    def _cap_max_processes(self, campaign_def) -> int:
        if campaign_def["execution"]["processes"] > MAX_PROCESSES_PER_CAMPAIGN:
            suggested_n_processes = MAX_PROCESSES_PER_CAMPAIGN
        else:
            suggested_n_processes = campaign_def["execution"]["processes"]
        return suggested_n_processes

    def _extract_entity_type(self, id_entity):
        if str(id_entity) == str(IoTCEntityCode.HYPERVISOR.value):
            return IoTCEntityType.HYPERVISOR.value
        elif str(id_entity) == str(IoTCEntityCode.CONTAINER.value):
            return IoTCEntityType.CONTAINER.value
        else:
            raise UnrecognizedEntityTypeError(
                "Entity type %s not recognized" % id_entity
            )

    def _execute_on_cron(
        self, id_campaign: int, campaign_name: str, campaign_args: List, schedule: str
    ):
        if not croniter.is_valid(schedule):
            raise InvalidCampaignScheduleError("Invalid schedule %s" % schedule)
        trigger = CronTrigger.from_crontab(schedule)

        self._scheduler.add_job(
            IoTCCampaign,
            args=campaign_args,
            trigger=trigger,
            id=str(id_campaign),
            name=campaign_name,
        )

    def _execute_every_interval(
        self,
        id_campaign: int,
        campaign_name: str,
        campaign_args: List,
        hours: int,
        minutes: int,
    ):
        if hours < 0 or minutes < 0:
            raise WrongIntervalError(
                "Interval of %s hours and %s minutes is not valid: only positive numbers are allowed"
                % (hours, minutes)
            )

        if hours == 0 and minutes == 0:
            raise TooLowIntervalError(
                "When using 'every', interval of 0 hours and 0 minutes is not allowed: at least 1 minute interval is required"
            )

        self._scheduler.add_job(
            IoTCCampaign,
            "interval",
            args=campaign_args,
            hours=hours,
            minutes=minutes,
            id=str(id_campaign),
            name=campaign_name,
        )

    def _execute_delayed(
        self,
        id_campaign: int,
        campaign_name: str,
        campaign_args: List,
        hours: int,
        minutes: int,
    ):
        now = round(time.time())

        if hours < 0 or minutes < 0:
            raise WrongIntervalError(
                "Interval of %s hours and %s minutes is not valid: only positive numbers are allowed"
                % (hours, minutes)
            )

        execution_time = now + hours * 3600 + minutes * 60

        GRACE_PERIOD_SECONDS = 5
        start_date = datetime.fromtimestamp(now)
        end_date = datetime.fromtimestamp(execution_time + GRACE_PERIOD_SECONDS)

        self._scheduler.add_job(
            IoTCCampaign,
            "interval",
            start_date=start_date,
            end_date=end_date,
            args=campaign_args,
            hours=hours,
            minutes=minutes,
            id=str(id_campaign),
            name=campaign_name,
        )

    def _execute_now(self, id_campaign: int, campaign_name: str, campaign_args: List):
        self._scheduler.add_job(
            IoTCCampaign,
            "date",
            args=campaign_args,
            id=str(id_campaign),
            name=campaign_name,
        )

    def _get_campaign_name_for_scheduler(
        self, id_campaign: int, campaign_def: Dict[str, Any]
    ) -> str:
        """Return an informative string about the campaign. To be used for APScheduler logs.

        Args:
            id_campaign (int): The id of the campaign
            campaign_def (Dict[str, Any]): The campaign definition JSON as a dictionary.
        """
        campaign_name = campaign_def["name"]
        n_tasks = len(campaign_def["tasks"])
        n_processes = campaign_def["execution"]["processes"]
        on_error = campaign_def["execution"]["on_error"]
        suspendable = campaign_def["execution"]["suspendable"]
        abortable = campaign_def["execution"]["abortable"]
        return f"[Campaign {id_campaign} '{campaign_name}'][#{n_tasks} Tasks][#{n_processes} Processes][ON_ERROR {on_error}][SUSPENDABLE {suspendable}][ABORTABLE {abortable}]"

    @staticmethod
    def _get_persistence_for_campaign() -> IoTCCampaignRepository:
        """
        This is a function to be passed to the campaign __init__ function.
        Instead of passing the persistence object as an argument to the initializer, a "retrieval" function is passed.

        This is required because the persistence object may not be picklified (this is an issue since scheduled/delayed campaigns use APScheduler, which makes use of pickle).

        NOTE: The method is declared as static because pickle has issues to serialize local attributes/methods. See: https://github.com/sqlalchemy/sqlalchemy/discussions/7308
        """
        if IoTCCampaignDispatcher.__persistence is None:
            raise Exception(
                f"Error in campaign initialization. Campaign dispatcher has not been initialized with a persistence object of class {IoTCCampaignRepository.__name__} to be passed to the campaign"
            )
        return IoTCCampaignDispatcher.__persistence

    @staticmethod
    def _get_task_factory_for_campaign() -> IoTCTaskFactory:
        """See _get_persistence_for_campaign"""

        if IoTCCampaignDispatcher.__task_factory is None:
            raise Exception(
                f"Error in campaign initialization. Campaign dispatcher has not been initialized with a task factory object of class {IoTCTaskFactory.__name__} to be passed to the campaign"
            )
        return IoTCCampaignDispatcher.__task_factory

    @staticmethod
    def _get_hypervisor_cache_for_campaign() -> IoTCHypervisorCache:
        """See _get_persistence_for_campaign"""

        if IoTCCampaignDispatcher.__hypervisor_cache is None:
            raise Exception(
                f"Error in campaign initialization. Campaign dispatcher has not been initialized with a cache object of class {IoTCHypervisorCache.__name__} to be passed to the campaign"
            )
        return IoTCCampaignDispatcher.__hypervisor_cache


class IoTCCampaign:
    def __init__(
        self,
        id: int,
        campaign_def: Dict,
        target_type: str,
        targets: List[IoTCTargetArchitecture],
        n_targets_per_subjob: int,
        n_subjobs: int,
        task_registry: TaskIdRegistry,
        get_persistence: Callable[[], IoTCCampaignRepository],
        get_hypervisor_cache: Callable[[], IoTCHypervisorCache],
        get_task_factory: Callable[[], IoTCTaskFactory],
    ):
        try:
            self.id = id
            self.campaign_def = campaign_def
            self.on_error = IoTCOnCampaignError(campaign_def["execution"]["on_error"])
            self.id_schedule = None
            self._persistence = get_persistence()
            self._hypervisor_cache = get_hypervisor_cache()
            self._task_factory = get_task_factory()
            self._tasks = campaign_def["tasks"]
            self._target_type = target_type
            self._targets = targets
            self._targets_per_subjob = n_targets_per_subjob
            self._n_subjobs = n_subjobs
            self._subjobs: List[IoTCSubjob] = []
            self._subjob_to_targets = self._get_targets_per_subjob()
            self._target_to_subjob = self._get_subjob_by_target(self._subjob_to_targets)
            self.n_targets_succeeded: int = 0
            self.n_targets_failed: int = 0

            try:
                self.id_schedule = self._persistence.add_schedule(
                    self.campaign_def,
                    self._targets,
                    self._target_to_subjob,
                    self.id,
                    task_registry,
                    start_time=round(time.time()),
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"Campaign {self.id}. Failed to create a new schedule. Campaign will not be executed. Error: {err}"
                )
                raise err

            IoTCServiceLogger.get().info(
                "Campaign %s. Schedule %s. Initialization" % (self.id, self.id_schedule)
            )
            self._build_subjobs()
            self._spawn_subjobs()
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Fatal error during campaign {self.id} initialization. Error: {err}"
            )
            raise err

    def _get_subjob_by_target(
        self, subjob_to_targets: Dict[int, List[int]]
    ) -> Dict[int, int]:
        """
        Return a dict in which:
        - Key is the target id
        - Value is the id of the subjob that will execute the campaign on it

        Args:
        - Dict that associates every subjob id to the list of its targets.
        """
        result = {}
        for id_subjob, targets in subjob_to_targets.items():
            for id_target in targets:
                result.update({id_target: id_subjob})
        return result

    def _get_targets_per_subjob(self) -> Dict[int, List[int]]:
        """
        Determine subjob execution with its target list.
        Return a dict in which:
        - Key is the subjob id
        - Value is the list of its targets
        """
        result = {}
        ids_targets = [target.id_target for target in self._targets]
        for i in self._range(self._n_subjobs):
            subjob_targets = ids_targets[
                (i - 1) * self._targets_per_subjob : (i) * self._targets_per_subjob
            ]
            result.update({i: subjob_targets})
        return result

    def _build_subjobs(self):
        for id_subjob, subjob_targets in self._subjob_to_targets.items():
            subjob_config = IoTCSubjobConfig(
                id_campaign=self.id,
                id_schedule=self.id_schedule,
                id_subjob=id_subjob,
                targets=subjob_targets,
                tasks=self._tasks,
                campaign_def=self.campaign_def,
                target_type=self._target_type,
                on_error=self.on_error,
                persistence=self._persistence,
                hypervisor_cache=self._hypervisor_cache,
                task_factory=self._task_factory,
            )
            IoTCServiceLogger.get().info(
                f"Campaign {self.id}. Schedule {self.id_schedule}. Initializing subjob {id_subjob}"
            )
            subjob = IoTCSubjob(subjob_config)
            self._subjobs.append(subjob)

    def _spawn_subjobs(self):
        IoTCServiceLogger.get().info(
            f"Campaign {self.id}. Schedule {self.id_schedule}. Spawning {self._n_subjobs} subjobs"
        )

        for subjob in self._subjobs:
            subjob.start()

        for subjob in self._subjobs:
            subjob.join()
            self.n_targets_succeeded += subjob.n_targets_succeeded
            self.n_targets_failed += subjob.n_targets_failed
            try:
                self._persistence.set_subjob_finished(
                    self.id, self.id_schedule, subjob.id
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"Campaign {self.id}. Schedule {self.id_schedule}. Failed to increase the number of exited subjobs. Error: {err}"
                )

        IoTCServiceLogger.get().info(
            f"Campaign {self.id}. Schedule {self.id_schedule}. Every subjob exited"
        )

        try:
            schedule_status = self._get_schedule_status(
                self.n_targets_succeeded, self.n_targets_failed
            )
            IoTCServiceLogger.get().info(
                f"Campaign {self.id}. Schedule {self.id_schedule}. Exiting with status [{schedule_status}]"
            )

            # Update schedule status to finished.
            self._persistence.set_status_finished(
                id_campaign=self.id,
                id_schedule=self.id_schedule,
                end_time=round(time.time()),
                status=schedule_status,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"Campaign {self.id}. Schedule {self.id_schedule}. Failed to persist schedule end state. Error: {err}"
            )

    def _range(self, end):
        return range(1, end + 1)

    def _get_schedule_status(
        self, n_targets_succeeded: int, n_targets_failed: int
    ) -> IoTCCampaignStatus:
        """Given the execution result of all targets in the campaign, compute and return the final status of this schedule."""
        n_total_targets = n_targets_failed + n_targets_succeeded
        if (n_total_targets) != len(self._targets):
            raise Exception(
                f"Campaign {self.id}. Schedule {self.id_schedule}. Failed to determine the final status of this schedule: the subjobs returned {n_total_targets} target results but the campaign should have been executed on {len(self._targets)} targets. #{n_targets_failed} Targets Failed. #{n_targets_succeeded} Targets Succeeded"
            )

        status = IoTCCampaignStatus.EXITED_KO
        if (n_targets_failed == 0) or (
            n_targets_failed > 0 and self.on_error == IoTCOnCampaignError.SKIP
        ):
            status = IoTCCampaignStatus.EXITED_OK
        return status

    def __str__(self) -> str:
        return f"Campaign[id={self.id}, id_schedule={self.id_schedule}]"

    def __repr__(self) -> str:
        return self.__str__()
