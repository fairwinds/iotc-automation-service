from abc import ABC
import json
import time
from typing import Any, Callable, Dict, List, NamedTuple, Union
from requests import Response

from IoTCAutomationService.IoTCMessageFingerprint import IoTCMessageFingerprint
from IoTCAutomationService.IoTCMqttClient import IoTCMqttClient
from IoTCAutomationService.common.api_client import IoTCAPIClient
from IoTCAutomationService.common.common import (
    MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST,
    MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE,
    PREPARE_MQTT_MESSAGE,
)
from IoTCAutomationService.common.enums import (
    IoTCOnCampaignError,
    IoTCTaskStatus,
)
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class IoTCTaskConfig(NamedTuple):
    task: Dict[str, Any]
    """Task definition."""

    commands: List[Dict[str, Any]]
    """Command definitions."""

    ids_commands: List[int]
    """List of command result identifiers (assume that rows already exist into the database)."""

    campaign_def: Dict

    on_error: IoTCOnCampaignError

    id_campaign: int

    id_schedule: int

    id_target: int

    id_task: int

    response_topic_suffix: str
    """Used only for MQTT tasks. The topic suffix in which the executor should publish the campaign results"""


class IoTCTaskFactory:
    """Class used to create Automation Service tasks of different types."""

    def __init__(
        self,
        get_api_client: Callable[[], IoTCAPIClient],
        get_mqtt_client: Callable[[], IoTCMqttClient],
        get_persistence: Callable[[], IoTCCampaignRepository],
    ) -> None:
        """
        Initialize the factory.

        Why are get_xxx functions used instead of passing directly xxx?
        Because the xxx objects might not be available when the factory is created, but will be valid later when the actual tasks are created.

        Args:
            get_api_client: Returns an api client.
            get_mqtt_client: Returns an mqtt client.
            get_persistence: Returns a campaign persistence object.
        """
        self._get_api_client = get_api_client
        self._get_mqtt_client = get_mqtt_client
        self._get_persistence = get_persistence

    def create(self, type: str, config: IoTCTaskConfig):
        if type == "API":
            return IoTCApiTask(config, self._get_persistence, self._get_api_client)
        else:
            return IoTCMqttTask(config, self._get_persistence, self._get_mqtt_client)


class IoTCTask(ABC):
    def __init__(
        self, config: IoTCTaskConfig, persistence: IoTCCampaignRepository
    ) -> None:
        super().__init__()
        self.id = config.id_task
        self.id_campaign = config.id_campaign
        self.id_schedule = config.id_schedule
        self.id_target = config.id_target
        self._config = config
        self._persistence = persistence
        self._log_prefix = f"Campaign {self.id_campaign}. Schedule {self.id_schedule}. Target {self.id_target}. Task {self.id} ['{self._config.task['name']}']"
        self._commands = config.commands

    def _persist_initial_state(self, start_time: int):
        """Update task status to started"""
        try:
            self._persistence.set_status_started(
                id_campaign=self.id_campaign,
                id_schedule=self.id_schedule,
                id_target=self.id_target,
                id_task=self.id,
                start_time=start_time,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"{self._log_prefix}. Failed to persist task start event. Error: {err}"
            )

    def _persist_final_state(self, end_time: int, status: IoTCTaskStatus):
        try:
            self._persistence.set_status_finished(
                id_campaign=self.id_campaign,
                id_schedule=self.id_schedule,
                id_target=self.id_target,
                id_task=self.id,
                end_time=end_time,
                status=status,
            )
        except Exception as err:
            IoTCServiceLogger.get().error(
                f"{self._log_prefix}. Failed to persist task end event. Error: {err}"
            )

    def _persist_command(
        self, id_command: int, time: int, command_result: Union[Dict, None] = None
    ):
        """
        Update command status to started/finished.
        If command_result is None, status will be set to started. Otherwise will be set to finished.
        """
        if command_result is None:
            try:
                self._persistence.set_status_started(
                    id_campaign=self.id_campaign,
                    id_schedule=self.id_schedule,
                    id_target=self.id_target,
                    id_task=self.id,
                    id_command=id_command,
                    start_time=time,
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Command {id_command}. Failed to persist command start event. Error: {err}"
                )
        else:
            try:
                self._persistence.set_status_finished(
                    id_campaign=self.id_campaign,
                    id_schedule=self.id_schedule,
                    id_target=self.id_target,
                    id_task=self.id,
                    id_command=id_command,
                    end_time=time,
                    command_result=command_result,
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Command {id_command}. Failed to persist command end event. Error: {err}. Command result is: {command_result}"
                )

    def _log_finished(self, task_status: IoTCTaskStatus) -> None:
        IoTCServiceLogger.get().info(
            f"{self._log_prefix} finished. Result [{task_status}] ({len(self._commands)} commands)"
        )


class IoTCApiTask(IoTCTask):
    """
    An API task is a collection of commands executed (sequentially) directly by the Automation Service.
    """

    def __init__(
        self,
        config: IoTCTaskConfig,
        get_persistence: Callable[[], IoTCCampaignRepository],
        get_api_client: Callable[[], IoTCAPIClient],
    ) -> None:
        super().__init__(config, get_persistence())
        self._ids_commands = config.ids_commands
        self._client = get_api_client()
        self._on_error = config.on_error

    def execute(self) -> IoTCTaskStatus:
        self._persist_initial_state(round(time.time()))
        task_status = IoTCTaskStatus.EXITED_OK
        for id_command, command in zip(self._ids_commands, self._commands):
            self._persist_command(id_command, round(time.time()))
            try:
                api_result = self._client.call_api_method_raw(
                    method=command["command"], params=command["params"]
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    "Launch API Command [%s] failed due to error: %s"
                    % (command["name"], err)
                )
                task_status = IoTCTaskStatus.EXITED_KO
                break

            command_result = self._prepare_command_result(api_result)

            self._persist_command(
                id_command, command_result["execution_date"], command_result
            )

            if not self._command_succeeded(api_result):
                if self._on_error == IoTCOnCampaignError.SKIP:
                    IoTCServiceLogger.get().info(
                        f"{self._log_prefix}. Command [API] [{command['command']}] FAILURE (status code = {command_result['status_code']}). Proceeding campaign execution since on_error='{self._on_error.value}'"
                    )
                else:
                    IoTCServiceLogger.get().info(
                        f"{self._log_prefix}. Command [API] [{command['command']}] FAILURE (status code = {command_result['status_code']}). Exiting from the current task since on_error='{self._on_error.value}'"
                    )
                    task_status = IoTCTaskStatus.EXITED_KO
                    break

        self._log_finished(task_status)
        self._persist_final_state(round(time.time()), task_status)
        return task_status

    def _prepare_command_result(self, raw_response: Response):
        # Convert API command status code for easier front-end parsing.
        # If success, status code wiil is set to 0. Otherwise 1.
        status_code: int = 0
        if not self._command_succeeded(raw_response):
            status_code = 1

        return {
            "execution_date": round(time.time()),
            "status_code": status_code,
            "result": raw_response.json(),
        }

    def _command_succeeded(self, raw_response: Response) -> bool:
        return (
            (200 <= raw_response.status_code <= 399)
            and ("success" in raw_response.json())
            and (raw_response.json()["success"] == True)
        )


class IoTCMqttTask(IoTCTask):
    """
    An MQTT task contains commands that are not executed by the Automation Service.
    Instead, they are sent through MQTT to a target IoTCatalyst Agent.
    The Automation Service here is only responsible to send the task and to collect the commands result.
    """

    def __init__(
        self,
        config: IoTCTaskConfig,
        get_persistence: Callable[[], IoTCCampaignRepository],
        get_mqtt_client: Callable[[], IoTCMqttClient],
    ) -> None:
        super().__init__(config, get_persistence())
        self._ids_commands = config.ids_commands
        self._task_name = config.task["name"]
        campaign_def = config.campaign_def
        self._campaign_name = campaign_def["name"]
        self._task = config.task
        self._response_topic_suffix = config.response_topic_suffix
        self._mqtt_client = get_mqtt_client()
        self._message_fingerprint = IoTCMessageFingerprint(
            IoTCServiceLogger.get(), message=self._task
        )
        self._message_type = "0x94"
        self._can_exit = False
        self._result = IoTCTaskStatus.EXITED_KO

    def execute(self) -> IoTCTaskStatus:
        self._persist_initial_state(round(time.time()))

        topic_response = self._topic_response()
        self._mqtt_client.add_callback(
            topic_response,
            lambda mqtt_client, operation_result: self._handle_result(operation_result),
        )

        # Retry indefinitely if MQTT subscription failed.
        subscribe_result = -1  # anything but 0
        while (not self._mqtt_client.connected) or (subscribe_result != 0):
            subscribe_result, _ = self._mqtt_client.subscribe((topic_response, 2))
            time.sleep(1)
            if subscribe_result != 0:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Subscription failed. Trying again. Topic: [{topic_response}]"
                )
        try:
            try:
                self._message_fingerprint.save()
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Failed to persist mqtt message fingerprint in Redis. Error: {err}"
                )
                raise err

            try:
                # Persist all commands (within this task) to started.
                for id_command in self._ids_commands:
                    try:
                        self._persist_command(id_command, round(time.time()))
                    except Exception as err:
                        IoTCServiceLogger.get().error(
                            f"{self._log_prefix}. Command {id_command}. Failed to persist command start event. Error: {err}"
                        )

                message = PREPARE_MQTT_MESSAGE(self._message_body(), self._message_type)
                topic_request = self._topic_request()
                self._mqtt_client.send(
                    topic_request,
                    bytes(json.dumps(message), "utf8"),
                    qos=2,
                    retries=None,
                )
            except Exception as err:
                IoTCServiceLogger.get().error(
                    f"{self._log_prefix}. Failed publishing the task over MQTT. Error: {err}"
                )
                raise err
        except:
            self._can_exit = True

        while not self._can_exit:
            time.sleep(0.1)

        self._mqtt_client.unsubscribe(topic_response)
        return self._result

    def _message_body(self):
        return {
            "response_topic": self._config.response_topic_suffix,
            "campaign_name": self._campaign_name,
            "task": {
                "type": self._commands[0][
                    "type"
                ],  # Assuming homogeneous task (validated by jsonschema)
                **self._task,
            },
            "task_hash": self._message_fingerprint.compute_fingerprint(),
        }

    def _topic_request(self):
        return MQTT_TOPIC_CAMPAIGN_EXEC_REQUEST(
            self._mqtt_client.domain_name,
            self.id_target,
            self.id_campaign,
            self.id,
            self.id_schedule,
        )

    def _topic_response(self):
        return MQTT_TOPIC_CAMPAIGN_EXEC_RESPONSE(
            self._mqtt_client.domain_name,
            self.id_target,
            self.id_campaign,
            self.id,
            self.id_schedule,
            self._response_topic_suffix,
        )

    def _sanitize_command_results(self, commands: List[Dict]) -> List[Dict]:
        """
        Sanitize the task result received from a target. Return the sanitized commands.
        - If a command doesn't have the status code, set it to 0.
        - Set default result (stdout) to "".
        - Set default execution date to the local time.
        """

        for i, command_result in enumerate(commands):
            if "execution_date" not in command_result:
                IoTCServiceLogger.get().warning(
                    f"{self._log_prefix}. Command result with index {i}. 'execution_date' property not found. Local timestamp will be persisted instead. Command result is: {command_result}"
                )

        commands = [
            {
                "status_code": 0,
                "result": "",
                "execution_date": round(time.time()),
                **command,
            }
            for command in commands
        ]
        return commands

    def _handle_result(self, result):
        """
        Callback that is triggered when a task output is received from the broker.
        """

        error: bool = False

        task_status = IoTCTaskStatus.EXITED_KO
        if "commands" not in result:
            IoTCServiceLogger.get().error(
                f"{self._log_prefix}. While retrieving MQTT results: the MQTT payload does not contain 'commands' property. Task will exit. Received payload is: {result}"
            )
            self._can_exit = True
            error = True

        if not error:
            commands = self._sanitize_command_results(result["commands"])

            n_invocations = len(self._task["commands"])
            n_results = len(commands)
            if n_invocations != n_results:
                IoTCServiceLogger.get().warning(
                    f"{self._log_prefix}. While retrieving MQTT results: the number of sent commands ({n_invocations}) does not match the number of results ({n_results}). The size of the greater set will be cut to the size of the smaller one. Received payload is: {result}"
                )
                if n_results > n_invocations:
                    # cut the number of results to match the number of invocations
                    commands = commands[:n_invocations]
                else:
                    # cut the number of invocations to match the number of results
                    self._ids_commands = self._ids_commands[:n_results]

            for id_command, command_result in zip(self._ids_commands, commands):
                end_time = int(command_result["execution_date"])
                self._persist_command(id_command, end_time, command_result)

            all_commands_ok = all([command["status_code"] == 0 for command in commands])

            task_status = (
                IoTCTaskStatus.EXITED_OK
                if all_commands_ok
                else IoTCTaskStatus.EXITED_KO
            )

            self._log_finished(task_status)

            def log_failed_commands(all_commands):
                if not all_commands_ok:
                    for i, cmd in enumerate(all_commands):
                        if cmd["status_code"] != 0:
                            IoTCServiceLogger.get().info(
                                f"{self._log_prefix}. Command {i} failed. Status code = {cmd['status_code']}"
                            )

            log_failed_commands(commands)

        self._persist_final_state(round(time.time()), task_status)
        self._result = task_status
        self._can_exit = True
