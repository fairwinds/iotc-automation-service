import time
import apscheduler

from IoTCAutomationService.campaign.common import IoTCCampaignScheduler
from IoTCAutomationService.common.common import (
    CAMPAIGN_ABORTABLE_STATUSES,
    CAMPAIGN_PAUSABLE_STATUSES,
    CAMPAIGN_RESUMABLE_STATUSES,
)
from IoTCAutomationService.common.enums import IoTCCampaignSchedule, IoTCCampaignStatus
from IoTCAutomationService.common.errors import (
    CampaignNotFoundError,
    UnprocessableCampaignCommandError,
)
from IoTCAutomationService.persistence.common import EntityIdNotFound
from IoTCAutomationService.persistence.campaign import IoTCCampaignRepository
from IoTCAutomationService.persistence.log import IoTCServiceLogger


class IoTCCampaignController:
    def __init__(
        self, persistence: IoTCCampaignRepository, scheduler: IoTCCampaignScheduler
    ) -> None:
        self._persistence = persistence
        self._scheduler = scheduler

    def abort(self, id_campaign: int):
        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError(f"Campaign {id_campaign} not found")

        if not campaign["abortable"]:
            raise UnprocessableCampaignCommandError(
                "Campaign %s cannot be paused from outside since it was created with the key 'abortable' as 'false'"
                % id_campaign
            )

        if campaign.get("last_schedule") is None:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} execution cannot be aborted since it is not running. You might try to unschedule it instead"
            )

        id_last_schedule = int(campaign["last_schedule"])
        last_schedule = self._persistence.get_schedule_details(
            id_campaign, id_last_schedule
        )
        schedule_status = IoTCCampaignStatus(last_schedule["status"])

        if schedule_status in CAMPAIGN_ABORTABLE_STATUSES:
            self._persistence.set_actionable_status(
                id_campaign=id_campaign,
                id_schedule=id_last_schedule,
                time=round(time.time()),
                status=IoTCCampaignStatus.ABORTED,
            )
            IoTCServiceLogger.get().info(
                f"Campaign {id_campaign} has been aborted by an outside API request"
            )
            return (
                "Campaign %s will be aborted if further tasks remain after completion of ongoing tasks"
                % id_campaign
            )
        else:
            raise UnprocessableCampaignCommandError(
                "Campaign %s cannot be aborted since its status is '%s'"
                % (id_campaign, schedule_status.value)
            )

    def resume(self, id_campaign: int):
        """Given a campaign whose last schedule is paused, resume it."""

        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError("Campaign %s not found" % id_campaign)

        if campaign.get("last_schedule") is None:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} execution cannot be resumed since there is no schedule to resume"
            )

        id_last_schedule = int(campaign["last_schedule"])
        last_schedule = self._persistence.get_schedule_details(
            id_campaign, id_last_schedule
        )
        schedule_status = IoTCCampaignStatus(last_schedule["status"])
        if schedule_status in CAMPAIGN_RESUMABLE_STATUSES:
            self._persistence.set_actionable_status(
                id_campaign=id_campaign,
                id_schedule=id_last_schedule,
                time=round(time.time()),
                status=IoTCCampaignStatus.RUNNING,
            )
            IoTCServiceLogger.get().info(
                f"Campaign {id_campaign} has been resumed by an outside API request"
            )
            return f"Campaign {id_campaign} resumed"
        else:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} cannot be resumed since its status is '{schedule_status.value}'"
            )

    def pause(self, id_campaign: int):
        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError(f"Campaign {id_campaign} not found")

        is_suspendable = bool(campaign["suspendable"])
        if not is_suspendable:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} cannot be paused from outside since it was created with the key 'suspendable' as 'false'"
            )

        if campaign.get("last_schedule") is None:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} execution cannot be paused since there is no schedule to pause"
            )

        id_last_schedule = int(campaign["last_schedule"])
        last_schedule = self._persistence.get_schedule_details(
            id_campaign, id_last_schedule
        )
        schedule_status = IoTCCampaignStatus(last_schedule["status"])

        if schedule_status in CAMPAIGN_PAUSABLE_STATUSES:
            self._persistence.set_actionable_status(
                id_campaign=id_campaign,
                id_schedule=id_last_schedule,
                time=round(time.time()),
                status=IoTCCampaignStatus.PAUSED,
            )
            IoTCServiceLogger.get().info(
                f"Campaign {id_campaign} has been paused by an outside API request"
            )

            return f"Campaign {id_campaign} will be paused if further tasks remain after completion of ongoing tasks"
        else:
            raise UnprocessableCampaignCommandError(
                f"Campaign {id_campaign} cannot be paused since its status is '{schedule_status.value}'"
            )

    def remove_scheduled(self, id_campaign: int):
        try:
            campaign = self._persistence.get_campaign(id_campaign)
        except EntityIdNotFound:
            raise CampaignNotFoundError(f"Campaign {id_campaign} not found")

        scheduled = IoTCCampaignSchedule(campaign["scheduled"])
        if scheduled == IoTCCampaignSchedule.SCHEDULED:
            try:
                self._scheduler.remove_job(str(id_campaign))
            except apscheduler.jobstores.base.JobLookupError as err:
                IoTCServiceLogger.get().warning(
                    f"User tried to remove a schedule for campaign {id_campaign} which is set to scheduled but is not in campaign scheduler. Original error is: {err}"
                )
            self._persistence.set_campaign_unscheduled(id_campaign)
            IoTCServiceLogger.get().info(f"Campaign {id_campaign} has been unscheduled")
            return "Schedule for campaign %s removed" % id_campaign
        elif scheduled == IoTCCampaignSchedule.NOT_SCHEDULED:
            raise UnprocessableCampaignCommandError(
                f"Cannot remove schedule for campaign {id_campaign} since it is not scheduled"
            )
        elif scheduled == IoTCCampaignSchedule.REMOVED:
            raise UnprocessableCampaignCommandError(
                f"Cannot remove schedule for campaign {id_campaign} since it has already been removed"
            )
