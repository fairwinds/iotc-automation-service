from typing import Any, Dict, List

from IoTCAutomationService.common.api_client import IoTCAPIClient
from IoTCAutomationService.common.cache import IoTCHypervisorCache
from IoTCAutomationService.common.errors import (
    MissingTargetsError,
    TargetNotFoundError,
    UnrecognizedWildcardError,
)
from IoTCAutomationService.common.types import IoTCTargetArchitecture


class IoTCTargetCalculator:
    """Class used to retrieve, given a campaign definition, the information of the associated targets, such as ID and architecture."""

    def __init__(
        self, hypervisor_cache: IoTCHypervisorCache, api_client: IoTCAPIClient
    ) -> None:
        self._hypervisor_cache = hypervisor_cache
        self._api_client = api_client

    def extract_campaign_targets(self, campaign_def: Dict[str, Any]):
        """
        Given a campaign definition, extract the list of its targets, along with their architecture.
        """
        if "target" not in campaign_def:
            raise MissingTargetsError("'target' is a required field")

        if "idComponentType" not in campaign_def["target"]:
            raise MissingTargetsError(
                "'idComponentType' is a required property of the 'target' field"
            )

        if "tags" in campaign_def["target"]:
            ids = self._api_client.get_hypervisors_ids_by_tags(
                campaign_def["target"]["tags"]
            )
        elif "ids" in campaign_def["target"]:
            ids = list(set(campaign_def["target"]["ids"]))  # remove duplicate ids
        elif "wildcard" in campaign_def["target"]:
            if campaign_def["target"]["wildcard"] == "*":
                cache = self._hypervisor_cache
                ids = cache.get_all_ids()
            else:
                raise UnrecognizedWildcardError(
                    'Unrecognized wildcard "%s". Only "*" is a valid wildcard'
                    % campaign_def["target"]["wildcard"]
                )

        targets: List[IoTCTargetArchitecture] = []
        for id_target in ids:
            name = self._hypervisor_cache.get_name(id_target)
            architecture = self._hypervisor_cache.get_architecture(id_target)
            if (name is None) or (architecture is None):
                raise TargetNotFoundError(
                    f"Cannot find name or architecture for target {id_target} in the IoTCatalyst Domain"
                )
            targets.append(
                IoTCTargetArchitecture(
                    id_target=id_target, name=name, architecture=architecture
                )
            )

        return targets
