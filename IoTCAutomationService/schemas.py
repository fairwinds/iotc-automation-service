TASK_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "description": {"type": "string"},
        "conditions": {"type": "array", "items": {"$ref": "#/definitions/CONDITION"}},
        "commands": {
            "type": "array",
            "minItems": 1,
            "items": {"$ref": "#/definitions/COMMAND"},
        },
    },
    "required": ["name", "description", "conditions", "commands"],
    "allOf": [
        # If a task contains SHELL or METRIC commands, the 'on_error_policy' field is required.
        {
            "if": {
                "properties": {
                    "commands": {
                        "items": {"properties": {"type": {"enum": ["SHELL", "METRIC"]}}}
                    }
                }
            },
            "then": {
                "properties": {
                    "on_error_policy": {
                        "type": "string",
                        "enum": ["exit", "ignore", "retry"],
                    },
                },
                "required": ["on_error_policy"],
            },
        },
        # Commands inside a task must be homogeneous (ie. of the same type).
        {
            "oneOf": [
                {
                    "properties": {
                        "commands": {
                            "items": {"properties": {"type": {"const": "SHELL"}}}
                        }
                    }
                },
                {
                    "properties": {
                        "commands": {
                            "items": {"properties": {"type": {"const": "METRIC"}}}
                        }
                    }
                },
                {
                    "properties": {
                        "commands": {
                            "items": {"properties": {"type": {"const": "PRESET"}}}
                        }
                    }
                },
                {
                    "properties": {
                        "commands": {
                            "items": {"properties": {"type": {"const": "API"}}}
                        }
                    }
                },
            ]
        },
    ],
}

CONDITION_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "operator": {"type": "string", "enum": ["AND", "OR", "and", "or", ""]},
        "checks": {"type": "array", "items": {"$ref": "#/definitions/CONDITION_CHECK"}},
    },
    "required": ["operator", "checks"],
}

CONDITION_CHECK_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "operator": {"type": "string", "enum": ["AND", "OR", "and", "or", ""]},
        "check": {
            "type": "object",
            "properties": {
                "comparator": {
                    "type": "string",
                    "enum": [">", "=", "==", "<", ">=", "<=", "<>", "!="],
                },
                "arg1": {"type": "string"},
                "arg2": {"type": "string"},
            },
            "required": ["comparator", "arg1", "arg2"],
        },
    },
    "required": ["operator", "check"],
}

COMMAND_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "type": {"type": "string", "enum": ["SHELL", "METRIC", "PRESET", "API"]},
        "command": {"type": "string"},
        "timeout": {"type": "integer"},
        "retries": {"type": "integer", "minimum": 0},
    },
    "required": ["name", "type", "command", "timeout", "retries"],
    "allOf": [
        # If command is of type API, the 'params' field is required.
        {
            "if": {"properties": {"type": {"const": "API"}}},
            "then": {
                "properties": {
                    "params": {"type": "object"},
                },
                "required": ["params"],
            },
        },
        # If command is of type SHELL, the 'causes_reboot' field is required.
        {
            "if": {"properties": {"type": {"const": "SHELL"}}},
            "then": {
                "properties": {
                    "causes_reboot": {"type": "boolean"},
                },
                "required": ["causes_reboot"],
            },
        },
        # If command is of type METRIC, validate the 'refresh_interval', 'data_type', 'date_format' fields.
        {
            "if": {"properties": {"type": {"const": "METRIC"}}},
            "then": {
                "properties": {
                    "refresh_interval": {"type": "number"},
                    "data_type": {
                        "type": "string",
                        "enum": ["string", "numeric", "date"],
                    },
                },
                "required": ["refresh_interval", "data_type"],
                "if": {
                    "properties": {"data_type": {"const": "date"}},
                },
                "then": {
                    "properties": {
                        "date_format": {"type": "string"},
                    },
                    "required": ["date_format"],
                },
            },
        },
    ],
}

TARGET_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "idComponentType": {"type": "integer"},
        "tags": {"type": "string"},
        "ids": {
            "type": "array",
            "items": {"type": "integer"},
        },
        "wildcard": {"type": "string"},
    },
    "required": ["idComponentType"],
    "oneOf": [
        {"required": ["ids"]},
        {"required": ["tags"]},
        {"required": ["wildcard"]},
    ],
}

EXECUTION_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "schedule": {"type": "string"},
        "every": {
            "type": "object",
            "anyOf": [{"required": ["hours"]}, {"required": ["minutes"]}],
            "properties": {
                "hours": {"type": "number"},
                "minutes": {"type": "number"},
            },
        },
        "in": {
            "type": "object",
            "anyOf": [{"required": ["hours"]}, {"required": ["minutes"]}],
            "properties": {
                "hours": {"type": "number"},
                "minutes": {"type": "number"},
            },
        },
        "processes": {"type": "number"},
        "on_error": {"type": "string", "enum": ["exit", "pause", "skip"]},
        "suspendable": {"type": "boolean"},
        "abortable": {"type": "boolean"},
    },
    "required": ["processes", "on_error", "suspendable", "abortable"],
}

CAMPAIGN_JSON_SCHEMA = {
    "definitions": {
        "TARGET": TARGET_JSON_SCHEMA,
        "EXECUTION": EXECUTION_JSON_SCHEMA,
        "TASK": TASK_JSON_SCHEMA,
        "COMMAND": COMMAND_JSON_SCHEMA,
        "CONDITION": CONDITION_JSON_SCHEMA,
        "CONDITION_CHECK": CONDITION_CHECK_JSON_SCHEMA,
    },
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "description": {"type": "string"},
        "target": {"$ref": "#/definitions/TARGET"},
        "execution": {"$ref": "#/definitions/EXECUTION"},
        "tasks": {
            "type": "array",
            "minItems": 1,
            "items": {"$ref": "#/definitions/TASK"},
        },
    },
    "required": ["name", "description", "target", "execution", "tasks"],
}

LIBRARY_COMMAND_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string",
            "pattern": "^\S+$",
        },  # command name cannot be empty, and cannot contain spaces
        "command": {"type": "string"},
        "interpreter": {"type": "string"},
        "type": {"type": "string", "enum": ["SHELL", "METRIC"]},
        "architectures": {"type": "array", "minItems": 1, "items": {"type": "string"}},
    },
    "required": ["name", "command", "interpreter", "type", "architectures"],
}
