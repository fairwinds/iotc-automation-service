import logging
import random
import redis
from hashlib import sha512
from typing import Union, Dict, Any
import json

from IoTCAutomationService.common.common import REDIS_HOST, REDIS_PORT


class IoTCMessageFingerprint:
    """Utility class to compute, store and delete MQTT payload hashes/fingerprints.

    Use case:
    1. During campaign execution, to prevent MITM attacks, the Automation Service generates and sends
    the fingerprint to the IoTCatalyst Agents, along with campaign data. The Automation Service also
    stores the fingerprint.
    2. For each received message, IoTCatalyst Agents send the corresponding fingerprint to the Automation Service.
    It then checks if the fingerprint corresponds to any previous generated fingerprint.
    """

    def __init__(
        self,
        logger: logging.Logger,
        message: Union[Dict, str, None] = None,
        fingerprint: Union[str, None] = None,
    ) -> None:
        """
        Object usage depends on which argument is None:
        - If message is None, object will be initialized with the fingerprint.
        - If fingerprint is None, object will be initialized with the message.

        Args:
            message: The message you want to calculate the fingerprint on. Expected to be a JSON, either as a dict or a str.
            fingerprint: The fingerprint you want to retrieve from the persistence layer.
        """

        self._message: Union[str, None] = None
        self._fingerprint = None
        self._host = REDIS_HOST
        self._port = int(REDIS_PORT)

        if message is None and fingerprint is None:
            raise ValueError(
                f"Cannot initialize a message fingerprint with both 'message' and 'fingerprint' arguments with None value"
            )
        if message is not None and fingerprint is not None:
            raise ValueError(
                f"Cannot initialize a message fingerprint with both 'message' and 'fingerprint' arguments with a value. One, and only one of them must be None. Received message: {message}. Received fingerprint: {fingerprint}"
            )

        if message is not None:
            if type(message) == str:
                self._message = message
            elif type(message) == dict:
                self._message = json.dumps(self._message, sort_keys=True)
            else:
                raise ValueError(
                    f"Message fingerprint has invalid type '{type(message)}'. Allowed types are [Dict, str]. Message value: {message}"
                )

        if fingerprint is not None:
            self._fingerprint = fingerprint

        self._logger = logger

    def compute_fingerprint(self) -> str:
        """Returns the message fingerprint as string.

        Note: Can be used only if object has been initialized with a message.

        Returns:
            str: The message fingerprint.
        """
        if self._message is None:
            raise ValueError(
                f"Cannot call 'compute_fingerprint' on message fingerprint initialized with a fingerprint. Fingerprint is: {self._fingerprint}"
            )

        if self._fingerprint is None:
            # Minimize likelihood that 2 equal messages have the same hash.
            payload: str = self._message + str(random.getrandbits(32))

            self._fingerprint = sha512(payload.encode("utf8")).hexdigest()

        return self._fingerprint

    def save(self) -> None:
        """Store the message fingerprint inside Redis storage.
        The redis key name will be equal to the fingerprint.

        Note: Can be used only if object has been initialized with a message.
        """
        if self._message is None:
            raise ValueError(
                f"Cannot call 'save' on message fingerprint initialized with a fingerprint. Fingerprint is: {self._fingerprint}"
            )

        self._fingerprint = self.compute_fingerprint()
        redis_client = redis.Redis(host=self._host, port=self._port)
        redis_client.set(
            self._fingerprint, ""
        )  # The value is unused, and therefore set to an empty string.
        if redis_client.get(self._fingerprint) is None:
            raise RuntimeError(
                f"Could not save message fingerprint '{self._fingerprint}' on Redis. Redis is at {self._host}:{self._port}"
            )
        self._logger.info(
            f"Message fingerprint {self._fingerprint} successfully saved on Redis"
        )

    def remove(self) -> None:
        """Remove the message fingerprint from the persistence layer.

        Note: Can be used only if object has been initialized with a fingerprint.
        """
        if self._fingerprint is None:
            raise ValueError(
                f"Cannot call 'remove' on message fingerprint initialized with a message. Message is: {self._message}"
            )

        redis_client = redis.Redis(host=self._host, port=self._port)
        redis_client.delete(self._fingerprint)
        self._logger.info(
            f"Received request to remove message fingerprint {self._fingerprint} from Redis"
        )

    def find(self) -> Any:
        """Tries to retrieve the fingerprint from the persistence layer.
        Returns the retrieved value associated with the key.

        Note: Can be used only if object has been initialized with a fingerprint."""
        if self._fingerprint is None:
            raise ValueError(
                f"Cannot call 'find' on message fingerprint initialized with a message. Message is: {self._message}"
            )

        redis_client = redis.Redis(host=self._host, port=self._port)
        get_result = redis_client.get(self._fingerprint)
        if get_result is None:
            raise KeyError(
                f"Cannot find key '{self._fingerprint}' on Redis. Redis is at {self._host}:{self._port}"
            )
        return get_result
