#!/usr/bin/env bash

function usage_message() {
    local message="$1"
    echo
    echo "####################################################################################"
    echo
    echo "$message"
    echo
    echo "Usage: $0 <REMOVE DB VOLUME> <REMOVE REDIS VOLUME> <REMOVE IMAGE> <CONTAINER_NAME>"
    echo 
    echo "REMOVE DB VOLUME: either 'yes' or 'no'"
    echo "REMOVE REDIS VOLUME: either 'yes' or 'no'"
    echo "REMOVE IMAGE: either 'yes' or 'no'"
    echo "CONTAINER_NAME: Used to remove the Automation Service container, and its associated volumes"
    echo
    echo "Example: $0 no yes yes iotcatalyst_automation_service"
    echo
    echo "####################################################################################"
    echo
}

REMOVE_DB_VOLUME="$1"
REMOVE_REDIS_VOLUME="$2"
REMOVE_IMAGE="$3"
CONTAINER_NAME="$4"

if [ "$#" -ne 4 ]; then
    usage_message "ERROR: Wrong number of parameters ($# provided)"
    exit 1
fi

if [ "$REMOVE_DB_VOLUME" != "yes" ] && [ "$REMOVE_DB_VOLUME" != "no" ]; then
    usage_message "ERROR: Wrong value '$REMOVE_DB_VOLUME' for REMOVE_DB_VOLUME parameter"
    exit 1
fi

if [ "$REMOVE_REDIS_VOLUME" != "yes" ] && [ "$REMOVE_REDIS_VOLUME" != "no" ]; then
    usage_message "ERROR: Wrong value '$REMOVE_REDIS_VOLUME' for REMOVE_REDIS_VOLUME parameter"
    exit 1
fi

if [ "$REMOVE_IMAGE" != "yes" ] && [ "$REMOVE_IMAGE" != "no" ]; then
    usage_message "ERROR: Wrong value '$REMOVE_IMAGE' for REMOVE_IMAGE parameter"
    exit 1
fi

set -e
echo "Removing container ${CONTAINER_NAME} ..."
docker rm -f "$CONTAINER_NAME"

if [ "$REMOVE_DB_VOLUME" == "yes" ]; then
    DB_VOLUME="$CONTAINER_NAME"_mysql_storage
    echo "Removing DB volume $DB_VOLUME ..."
    docker volume rm "$DB_VOLUME"
fi

if [ "$REMOVE_REDIS_VOLUME" == "yes" ]; then
    REDIS_VOLUME="$CONTAINER_NAME"_redis_storage
    echo "Removing Redis volume $REDIS_VOLUME ..."
    docker volume rm "$REDIS_VOLUME"
fi

if [ "$REMOVE_IMAGE" == "yes" ]; then
    echo "Removing production image..."
    set -o xtrace
    docker images -a | grep "iotcatalyst_automation_service_linux_x86_64" | awk '{print $3}' | xargs -r docker rmi
fi
