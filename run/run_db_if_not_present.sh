#!/usr/bin/env bash

if [ "$#" -ne 10 ]; then
  echo 
  echo "####################################################################################"
  echo
  echo "Wrong number of parameters ($# provided)"
  echo 
  echo "Usage: $0 <MYSQL_USER> <MYSQL_PASSWORD> <MYSQL_ROOT_PASSWORD> <MYSQL_HOST_PORT_TCP> <MYSQL_HOST_PORT_PROTOBUF> <MAX_LOG_DIM> <MAX_LOG_FILES> <IMAGE_TAG> <AS_CONTAINER_NAME> <DB_SCRIPTS_PATH>"
  echo 
  echo "Example: $0 asuser alfabeto alfabeto 3309 33090 10m 3 latest iotcatalyst_automation_service ./IoTCAutomationServiceDB"
  echo
  echo "Description: If the Automation Service container is not found, create the image and start the container with the provided arguments."
  echo 
  echo "arguments:"
  echo "MYSQL_HOST_PORT_TCP         Number - The host port used to expose MySQL TCP port"
  echo "MYSQL_HOST_PORT_PROTOBUF    Number - The host port used to expose MySQL Protobuf port"
  echo "MAX_LOG_DIM                 String - The maximum size of the log before it is rolled. A positive integer plus a modifier representing the unit of measure (k, m, or g)"
  echo "MAX_LOG_FILES               Number - The maximum number of log files that can be present"
  echo "IMAGE_TAG                   String - The tag of the DB Docker image"
  echo "AS_CONTAINER_NAME           String - The name of the Automation Service container. Used to associate DB storage with the container"
  echo "DB_SCRIPTS_PATH             String - Path of directory that contains script used to create DB image and run DB container"
  echo
  echo "####################################################################################"
  echo
  exit 1
else
    MYSQL_USER="$1"
    MYSQL_PASSWORD="$2"
    MYSQL_ROOT_PASSWORD="$3"
    MYSQL_PORT="$4"
    MYSQL_PORT_PROTOBUF="$5"
    MAX_LOG_DIM="$6"
    MAX_LOG_FILES="$7"
    DB_IMAGE_TAG="$8"
    AS_CONTAINER_NAME="$9"
    DB_SCRIPTS_PATH="${10}"

    if [ $(docker ps -a -qf name='iotcatalyst_automation_service_db' | wc -l) -gt 0 ]; then
        echo "INFO: Automation Service DB container already running."
    else
        echo "INFO: Automation Service DB container is NOT running."

        set -e
        
        # Ensure DB scripts exist.
        ls "$DB_SCRIPTS_PATH/create_image.sh" > /dev/null
        ls "$DB_SCRIPTS_PATH/run_iotcatalyst_automation_service_db.sh" > /dev/null
        
        echo "INFO: Creating the DB image ..."
        chmod +x "$DB_SCRIPTS_PATH"/*.sh
        cd "$DB_SCRIPTS_PATH" && ./create_image.sh "$DB_IMAGE_TAG" && cd ..
        echo "INFO: Creating the DB container ..."
        "$DB_SCRIPTS_PATH"/run_iotcatalyst_automation_service_db.sh "$MYSQL_USER" "$MYSQL_PASSWORD" "$MYSQL_ROOT_PASSWORD" "$MYSQL_PORT" "$MYSQL_PORT_PROTOBUF" "$MAX_LOG_DIM" "$MAX_LOG_FILES" "$DB_IMAGE_TAG" "$AS_CONTAINER_NAME"
    fi
fi