#!/usr/bin/env bash

usage_message() {
  local message=$1
  echo
  echo "####################################################################################"
  echo
  echo "$message"
  echo
  echo "Usage: $0 [-d|h] <AUTOMATION_SERVICE_PORT> <REDIS_PORT> <CACHE_EXPIRATION> <MAX_LOG_DIM> <MAX_LOG_FILES> <SECRET> <IMAGE_TAG> <CONTAINER_NAME> <MYSQL_HOST> <MYSQL_PORT> <MYSQL_USER> <MYSQL_PASSWORD>"
  echo
  echo "Example 1: $0 9011 6379 24 10m 3 mySecret latest myContainer 127.0.0.1 3309 asuser alfabeto"
  echo "Example 2: $0 -d $(pwd)/IoTCAutomationService 9011 6379 24 10m 3 mySecret latest myContainer 127.0.0.1 3309 asuser alfabeto"
  echo
  echo "options:"
  echo "-d <SOURCES_PATH>       Run container in development mode. Create a bind mount between host SOURCES_PATH and container's sources path."
  echo "-h                      Show this help message"
  echo
  echo "arguments:"
  echo "AUTOMATION_SERVICE_PORT Number - The Automation Service Web Server TCP Port" 
  echo "REDIS_PORT              Number - The Automation Service Redis Server TCP Port" 
  echo "CACHE_EXPIRATION        Number - IoT Catalyst data cache expiration expressed in hours" 
  echo "MAX_LOG_DIM             String - The maximum size of the log before it is rolled. A positive integer plus a modifier representing the unit of measure (k, m, or g)"
  echo "MAX_LOG_FILES           Number - The maximum number of log files that can be present"
  echo "SECRET                  String - Secret key used to initialize the Automation Service"
  echo "IMAGE_TAG               String - The tag of the Automation Service Docker image"
  echo "CONTAINER_NAME          String - Used to name the Automation Service container, and to identify its associated volumes" 
  echo
  echo "####################################################################################"
  echo
}

DEVMODE="no"

while getopts ":hd:" option; do
  case $option in
    h)
      usage_message ""
      exit 0;;
    d)
      DEVMODE="yes"
      SOURCES_PATH="$OPTARG"
      shift 2;;
    \? )
      usage_message "Invalid option: $OPTARG"
      exit 1;;
  esac
done

if [ "$#" -ne 12 ]; then
  usage_message "Wrong number of parameters ($# provided)"
  exit 1
fi

AUTOMATION_SERVICE_PORT="$1"
REDIS_PORT="$2" 
CACHE_EXPIRATION="$3" 
MAX_LOG_DIM="$4"
MAX_LOG_FILES="$5" 
SECRET="$6"
DOCKER_IMAGE_TAG="$7"
CONTAINER_NAME="$8" 
MYSQL_HOST="$9" 
MYSQL_PORT="${10}"
MYSQL_USER="${11}" 
MYSQL_PASSWORD="${12}"

echo "WARNING: Running container in network host mode"
DOCKER_RUN_OPTIONS_NETWORK="--network host"

if [ "$DEVMODE" = "yes" ]; then
  DEBUG_PORT="5678"
  PROGRAM_OPTIONS="$DEBUG_PORT"
  DOCKER_RUN_OPTIONS_DEV="--mount type=bind,source=${SOURCES_PATH},target=/home/catalyst/IoTCatalystOS/IoTCAutomationService"
  # DOCKER_IMAGE="fwdigital/iotcatalyst_automation_service_dev_linux_x86_64"
  echo "WARNING: Development mode is active!"
  echo "WARNING: Exposing remote debug port $DEBUG_PORT"
  echo "WARNING: bind mount will be created with host directory $SOURCES_PATH"
  echo
else
  PROGRAM_OPTIONS=""
  DOCKER_RUN_OPTIONS_DEV=""
fi


echo ""
echo "Using AUTOMATION SERVICE PORT = $AUTOMATION_SERVICE_PORT"
echo "Using REDIS PORT = $REDIS_PORT"
echo "Using CACHE EXPIRATION = $CACHE_EXPIRATION"
echo "Using MAX LOG DIMENSION = $MAX_LOG_DIM"
echo "Using MAX LOG FILES = $MAX_LOG_FILES"
echo "Using SECRET = $SECRET"
echo "Using CONTAINER_NAME = $CONTAINER_NAME"

REDIS_VOLUME="$CONTAINER_NAME"_redis_storage

echo ""
echo "Creating container named $CONTAINER_NAME"
echo "Redis volume is $REDIS_VOLUME"

DOCKER_RUN_OPTIONS_ENV="-e AUTOMATION_SERVICE_PORT=${AUTOMATION_SERVICE_PORT} -e REDIS_PORT=${REDIS_PORT} -e CACHE_EX=${CACHE_EXPIRATION} -e MYSQL_HOST=${MYSQL_HOST} -e MYSQL_PORT=${MYSQL_PORT} -e MYSQL_USER=${MYSQL_USER} -e MYSQL_PASSWORD=${MYSQL_PASSWORD}"
DOCKER_RUN_OPTIONS_PERSISTENCE="--mount source=${REDIS_VOLUME},target=/var/lib/redis"
DOCKER_IMAGE="fwdigital/iotcatalyst_automation_service_linux_x86_64"

set -e 
echo ""
set -o xtrace
docker run --name "$CONTAINER_NAME" \
$DOCKER_RUN_OPTIONS_NETWORK \
--restart unless-stopped \
--log-driver json-file --log-opt max-size="$MAX_LOG_DIM" --log-opt max-file="$MAX_LOG_FILES" \
$DOCKER_RUN_OPTIONS_DEV \
$DOCKER_RUN_OPTIONS_ENV \
$DOCKER_RUN_OPTIONS_PERSISTENCE \
-d "$DOCKER_IMAGE":"$DOCKER_IMAGE_TAG" "$REDIS_PORT" "$SECRET" $PROGRAM_OPTIONS
