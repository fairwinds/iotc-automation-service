#!/usr/bin/env bash

set -e
chmod +x ./create_image.sh
./create_image.sh dev

docker build -t fwdigital/iotcatalyst_automation_service_dev_linux_x86_64:latest ./dev