stages:
  - typecheck
  - build
  - unit-test
  - integration-test
  - stop
  - run


# TODO: this should be uncommented, but there is no Python on Studio Dev machine...
# typecheck-studio-dev:
#   stage: typecheck
#   only:
#     - dev
#     - mysql
#     - dev-sqlalchemy
#   tags:
#     - Studio_DEV
#   variables:
#     GIT_CLEAN_FLAGS: -ffdx
#     PYTHON_PATH: <path/to/python3.8>
#   script:
#     - $PYTHON_PATH -m venv .venv
#     - .venv/bin/python3.8 -m pip install --upgrade pip
#     - .venv/bin/python3.8 -m pip install mypy
#     - .venv/bin/python3.8 -m mypy --install-types --non-interactive --explicit-package-bases --ignore-missing-imports -p IoTCAutomationService

typecheck-lab-dell:
  stage: typecheck
  only:
    - master
    - dev
  tags:
    - x86_64
  variables:
    GIT_CLEAN_FLAGS: -ffdx
    PYTHON_PATH: /home/devel/.pyenv/versions/3.8.1/envs/automation_service3.8/bin/python3.8
  script:
    - chmod +x ./typecheck.sh
    - ./typecheck.sh yes $PYTHON_PATH IoTCAutomationService


# Run unit tests inside a docker container.
.unit-test: &config-unit-test
  stage: unit-test
  only:
    - dev
  tags:
    - Studio_DEV
    - x86_64
  variables:
    GIT_CLEAN_FLAGS: -ffdx
    CONTAINER_NAME: iotcatalyst_automation_service_unit_tests
    IMAGE_NAME: fwdigital/iotcatalyst_automation_service_unit_tests_linux_x86_64
  script:
    - docker build -t $IMAGE_NAME ./tests
    - (docker ps -a | grep $CONTAINER_NAME > /dev/null) && (docker rm $CONTAINER_NAME) && (echo "Previous container $CONTAINER_NAME removed successfully")
    - docker run --name $CONTAINER_NAME $IMAGE_NAME

unit-test-lab-dell:
  <<: *config-unit-test
  tags:
    - x86_64

unit-test-studio:
  <<: *config-unit-test
  tags:
    - Studio_DEV


# Run integration tests. The Automation Service DB container is set up before running tests. It is removed after running tests.
.integration-test: &config-integration-test
  stage: integration-test
  only:
    - dev
  variables:
    GIT_CLEAN_FLAGS: -ffdx
    DB_IMAGE_NAME: fwdigital/iotcatalyst_automation_service_db:integration_test
    DB_CONTAINER_NAME: iotcatalyst_automation_service_db_integration_test
    DB_PORT: 64533 # dummy port. will be used just for the time the tests are running
  script:
    - echo "Using DB_IMAGE_NAME = $DB_IMAGE_NAME"
    - echo "Using DB_CONTAINER_NAME = $DB_CONTAINER_NAME"
    - echo "Using DB_PORT = $DB_PORT"
    - docker build -t "$DB_IMAGE_NAME" -f ./IoTCAutomationServiceDB/Dockerfile ./IoTCAutomationServiceDB
    - (docker ps -a | grep $DB_CONTAINER_NAME > /dev/null) && (docker rm -f $DB_CONTAINER_NAME) && (echo "Previous container $DB_CONTAINER_NAME removed successfully")
    - docker run -d --name "$DB_CONTAINER_NAME" -p "$DB_PORT":3306 -e MYSQL_USER="$MYSQL_USER" -e MYSQL_PASSWORD="$MYSQL_PASSWORD" -e MYSQL_ROOT_PASSWORD="$MYSQL_ROOT_PASSWORD" "$DB_IMAGE_NAME"
    - echo "Waiting for the test DB to initialize..."
    - sleep 60
    - $PYTHON_PATH -m venv tests/.venv
    - tests/.venv/bin/python -m pip install --upgrade pip
    - tests/.venv/bin/python -m pip install -r requirements.txt -r tests/requirements.txt
    - MYSQL_PORT="$DB_PORT" MYSQL_USER="$MYSQL_USER" MYSQL_PASSWORD="$MYSQL_PASSWORD" tests/.venv/bin/python -m unittest discover -s ./tests/test_integration --verbose
    - docker rm -f "$DB_CONTAINER_NAME"

integration-test-lab-dell:
  <<: *config-integration-test
  tags:
    - x86_64
  before_script:
    # cannot create a "variables" section because it would override the one from the yaml anchor
    - export PYTHON_PATH="/home/devel/.pyenv/versions/automation_service3.8/bin/python"

# integration-test-studio:
#   <<: *config-integration-test
#   tags:
#     - Studio_DEV
#   before_script:
#     - export PYTHON_PATH="???" # TODO: you need to install Python on Studio Dev machine in order to run this.


.script-stop: &script-stop
  - chmod +x run/*.sh
  - run/stop_iotcatalyst_automation_service_linux_x86_64.sh $REMOVE_VOLUME_DB $REMOVE_VOLUME_REDIS $REMOVE_IMAGE $AS_CONTAINER_NAME

stop-studio-dev:
  stage: stop
  only:
    - dev
  tags:
    - Studio_DEV
  variables:
    GIT_CLEAN_FLAGS: -ffdx
    AS_CONTAINER_NAME: $AS_CONTAINER_DEV
  script:
    *script-stop

stop-studio-prod:
    stage: stop
    only:
      - master
    tags:
      - Studio_DEV
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_PROD
    script:
      *script-stop

stop-lab-dell-dev:
    stage: stop
    only:
      - dev
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_DEV
    script:
      *script-stop

stop-lab-dell-prod:
    stage: stop
    only:
      - master
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_PROD
    script:
      *script-stop


.script-build: &script-build
  - chmod +x *.sh
  - ./create_image.sh $AS_IMAGE_TAG

build-studio-dev:
    stage: build
    only:
      - dev
    tags:
      - Studio_DEV
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_IMAGE_TAG: dev
    script:
      *script-build

build-studio-prod:
    stage: build
    only:
      - master
    tags:
      - Studio_DEV
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_IMAGE_TAG: latest
    script:
      *script-build

build-lab-dell-dev:
    stage: build
    only:
      - dev
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_IMAGE_TAG: dev
    script:
      *script-build

build-lab-dell-prod:
    stage: build
    only:
      - master
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_IMAGE_TAG: latest
    script:
      *script-build


.script-run: &script-run
  - chmod +x run/*.sh
  - run/run_db_if_not_present.sh $MYSQL_USER $MYSQL_PASSWORD $MYSQL_ROOT_PASSWORD $_MYSQL_PORT $_MYSQL_PORT_PROTOBUF 10m 3 $DB_IMAGE_TAG $AS_CONTAINER_NAME IoTCAutomationServiceDB
  - run/run_iotcatalyst_automation_service_linux_x86_64.sh $DEBUG_OPTION $AS_PORT $_REDIS_PORT 24 10m 3 $AS_SECRET $AS_IMAGE_TAG $AS_CONTAINER_NAME $MYSQL_HOST $_MYSQL_PORT $MYSQL_USER $MYSQL_PASSWORD

# Allow optional bind mount + debugging based on variable defined on Gitlab
.script-optional-debug-and-bindmount: &script-optional-debug-and-bindmount
  - >
    if [ "$AS_DEBUG" == "yes" ]; then
      export DEBUG_OPTION="-d $_AS_SOURCES_PATH"
    else
      export DEBUG_OPTION=""
    fi

run-studio-dev:
    stage: run
    only:
      - dev
    tags:
      - Studio_DEV
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_DEV
      AS_IMAGE_TAG: dev
      DB_IMAGE_TAG: dev
      _MYSQL_PORT: $MYSQL_PORT
      _MYSQL_PORT_PROTOBUF: $MYSQL_PORT_PROTOBUF
      _REDIS_PORT: $REDIS_PORT
      _AS_SOURCES_PATH: $AS_SOURCES_PATH_STUDIO_DEV
    before_script:
      *script-optional-debug-and-bindmount
    script:
      *script-run

run-studio-prod:
    stage: run
    only:
      - master
    tags:
      - Studio_DEV
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_PROD
      AS_IMAGE_TAG: latest
      DB_IMAGE_TAG: latest
      _MYSQL_PORT: $MYSQL_PORT
      _MYSQL_PORT_PROTOBUF: $MYSQL_PORT_PROTOBUF
      _REDIS_PORT: $REDIS_PORT
    script:
      *script-run

run-lab-dell-dev:
    stage: run
    only:
      - dev
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_DEV
      AS_IMAGE_TAG: dev
      DB_IMAGE_TAG: dev
      _MYSQL_PORT: 3306
      _MYSQL_PORT_PROTOBUF: 33060
      _REDIS_PORT: 6390
      _AS_SOURCES_PATH: $AS_SOURCES_PATH_LAB_DELL_DEV
    before_script:
      *script-optional-debug-and-bindmount
    script:
      *script-run

run-lab-dell-prod:
    stage: run
    only:
      - master
    tags:
      - x86_64
    variables:
      GIT_CLEAN_FLAGS: -ffdx
      AS_CONTAINER_NAME: $AS_CONTAINER_PROD
      AS_IMAGE_TAG: latest
      DB_IMAGE_TAG: latest
      _MYSQL_PORT: 3306
      _MYSQL_PORT_PROTOBUF: 33060
      _REDIS_PORT: 6390
      DEBUG_OPTION: ''
    script:
      *script-run