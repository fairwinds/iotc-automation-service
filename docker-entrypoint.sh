#!/usr/bin/env bash

if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
    echo 
    echo "####################################################################################"
    echo
    echo "Wrong number of parameters"
    echo 
    echo "Usage: $0 <REDIS PORT> <SECRET> [DEBUG PORT]"
    echo
    echo "####################################################################################"
    echo
    exit 1
else
    REDIS_PORT="$1"
    SECRET="$2"
    DEBUG_PORT="$3"
    
    sed -i "s/6379/$REDIS_PORT/g" /etc/redis/redis.conf;
    /usr/bin/redis-server /etc/redis/redis.conf > /dev/null;

    sleep 5;
    pid=$(pgrep redis);
    if [[ ! "$pid" =~ ^[0-9]+$ ]]; then
        echo "Redis server encountered an error on start, maybe port $REDIS_PORT is already in use?"
        exit 1
    fi

    ./start.sh $REDIS_PORT $SECRET .venv/bin/python $AUTOMATION_SERVICE_PORT $CACHE_EX $DEBUG_PORT
fi
