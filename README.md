# IoTCatalyst Automation Service

- [IoTCatalyst Automation Service](#iotcatalyst-automation-service)
  - [Running inside Docker](#running-inside-docker)
    - [Preliminary steps](#preliminary-steps)
    - [Production](#production)
    - [Development](#development)
    - [Removing previous container/image and cleaning volumes](#removing-previous-containerimage-and-cleaning-volumes)
  - [Running outside Docker](#running-outside-docker)
    - [Important notes](#important-notes)
    - [Steps](#steps)
  - [Running tests](#running-tests)
  - [View logs](#view-logs)
  - [Documentation](#documentation)
  - [Known issues](#known-issues)

## Running inside Docker

### Preliminary steps
#### Create the database container
1. `cd IoTCAutomationServiceDB`
2. Launch `create_image.sh`
3. Launch `run_iotcatalyst_automation_service_db.sh`

### Build and Run - Production
#### Create the Automation Service container
1. Launch `create_image.sh`
2. Launch `run/run_iotcatalyst_automation_service_linux_x86_64.sh`

### Build and Run - Development
#### Create the Automation Service container with debug enabled and source code bind mounts
1. Launch `dev_create_image.sh`
2. Launch `run/run_iotcatalyst_automation_service_linux_x86_64.sh` with the `-d` flag **before** the positional parameters.

**NOTES**: 
- Debug configuration for VSCode is provided inside `launch.json` (yes, you can debug the code even if it's inside the container).
- Code autoreload is currently not supported. You must restart the container every time you make a change in the code.

### Removing previous container/image and cleaning volumes
* Launch `run/stop_iotcatalyst_automation_service_linux_x86_64.sh`

## Running outside Docker
### Important notes
Running outside a container is strongly discouraged and should be done only for development purposes.
### Steps 
1. Download the same Python version as specified in the Dockerfile
2. Using a virtualenv is **highly** recommended
3. Install `requirements.txt` and `dev/dev_requirements.txt`
4. Launch `start.sh`

## Running tests
1. Install `tests/requirements.txt`
2. Launch `python -m unittest tests/<test_filename>`

## View logs
* Logs are available in `/var/log/automation-service/automation-service.log`

## Documentation
* [Postman endpoints collection](/docs/endpoints-collection.json)
* [API endpoints pdf guide](/docs/help.pdf)
* [Automation Service Documentation](/docs/Automation%20Service%20Docs.pdf)

  * [Click here if you have troubles visualizing diagrams](/docs/Automation%20Service%20Docs.md)

## Known issues
* Due to the `--network=host` flag, running the Docker containers only works in Linux systems.